package com.alvarodiez.util;

import javax.swing.JOptionPane;

/**
 * Esta es una clase en la que tengo métodos estáticos para crear una ventana con un mensaje.
 * Cada método se refiere a un tipo distinto de mensaje.
 */
public class Util {

    /**
     * Este método me muestra un mensaje de error con el texto recibido
     * @param mensaje Texto del mensaje de error
     */
    public static void showErrorAlert(String mensaje) {
        JOptionPane.showMessageDialog(null, mensaje, "Error", JOptionPane.ERROR_MESSAGE);
    }

    /**
     * Este método me muestra un mensaje de aviso con el texto recibido
     * @param mensaje Texto del mensaje de aviso
     */
    public static void showWarningAlert(String mensaje) {
        JOptionPane.showMessageDialog(null, mensaje, "Aviso", JOptionPane.WARNING_MESSAGE);
    }

}
