package com.alvarodiez.principal;

import com.alvarodiez.gui.*;

import java.awt.*;
import java.io.IOException;
import java.sql.SQLException;

public class Principal {
    public static void main(String[] args) throws SQLException, IOException, FontFormatException {
        LoginDialog login = new LoginDialog();
        Modelo modelo = new Modelo();
        Controlador controlador = new Controlador(modelo, login);
    }

}
