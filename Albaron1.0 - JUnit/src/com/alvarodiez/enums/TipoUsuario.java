package com.alvarodiez.enums;

/**
 * Esta clase enum enumera las constantes con las que se rellena
 * el JComoboBox del tipo de usuario.
 * Representan los diferentes tipso de perfiles de usuario que existen en la aplicación.
 */
public enum TipoUsuario{
    CLIENTE("Cliente"),
    CONSULTOR("Consultor"),
    ADMINISTRADOR("Administrador");

    private String valor;

    TipoUsuario(String valor) { this.valor=valor; }
    public String getValor() {
        return valor;
    }
}
