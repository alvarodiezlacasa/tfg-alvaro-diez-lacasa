package com.alvarodiez.enums;

/**
 * Esta clase enum enumera las constantes con las que se rellena
 * el JComoboBox tipoPrenda de la vista.
 * Representan las sedes de las tiendas que existen.
 */
public enum UsosProducto {
    BASICOS("Basicos"),
    ESTRUCTURAS("Estructuras"),
    FACHADAS("Fachadas"),
    REVESTIMIENTOS("Revestimientos"),
    CUBIERTAS("Cubiertas"),
    PAVIMENTACION("Pavimentación"),
    AISLANTES("Aislantes"),
    INSTALACIONES("Instalaciones"),
    MATERIALESAYUDA("Materiales de ayuda"),
    URBANIZACION("Urbanización");

    private String valor;

    UsosProducto(String valor) { this.valor=valor; }
    public String getValor() {
        return valor;
    }
}
