package com.alvarodiez.gui;

import com.alvarodiez.enums.UsosProducto;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

public class ModifProductDialog extends JFrame{
    JPanel panel1;

    //JTextField
    JTextField txtCodigoProductoModif;
    JTextField txtNombreProductoModif;
    JTextField txtPrecioProductoModif;
    JTextField txtCantidadProductoModif;
    JTextField txtMedidaProductoModif;
    JTextField txtTipoProductoModif;

    //JComboBox
    JComboBox cbUsosProductoModif;
    JComboBox cbCatProductoModif;

    //JButton
    JButton btnModifProd;
    JButton btnAtrasProdModif;

    /**
     * Constructor de la clase ModifProductDialog dónde
     * llamamos al método startJFrame() para configurar la ventana
     */
    public ModifProductDialog() {
        startJFrame();
    }

    /**
     * Método en el que establecemos la configuración de la ventana
     * como por ejemplo el tamaño, la posición, el icono...
     * Además, llamamos a los métodos para configurar los combobox.
     */
    private void startJFrame(){
        this.setTitle("Albaron");
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.pack();
        this.setVisible(false);
        this.setSize(new Dimension(this.getWidth(), this.getHeight()));
        this.setLocationRelativeTo(null);
        Image ico=Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("icono.png"));
        this.setIconImage(ico);
        setEnumComboBox();
    }

    /**
     * Setea el combobox en el que cargamos los objetos de una clase enumerada
     */
    private void setEnumComboBox() {
        for (UsosProducto constant : UsosProducto.values()) {
            cbUsosProductoModif.addItem(constant.getValor());
        }
        cbUsosProductoModif.setSelectedIndex(-1);
    }

}
