package com.alvarodiez.gui;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Vector;

/**
 * Clase contenedora de todos los métodos utilizados en el controlador
 */
public class Ajustes {

    /**
     * Método para cambiar la fuente de texto de las tablas
     * @param vistaAdmin la ventana dónde va a funcionar el método
     */
    public void tablaAjustesAdmin1(VistaAdmin vistaAdmin) {
        //Llamamos al objeto que queramos modificar y mediante el método .setFont
        //cambiamos la fuente introduciendo el nombre de la fuente, el estilo y el tamaño
        vistaAdmin.btnTipoTabla.setText("Fuente 2");
        vistaAdmin.tableUsuarioAdmin.setFont(new Font("MV Boli", 1, 13));
        vistaAdmin.tableProductosAdmin.setFont(new Font("MV Boli", 1, 13));
        vistaAdmin.tableCategoriasAdmin.setFont(new Font("MV Boli", 1, 13));
        vistaAdmin.tableListaAdmin.setFont(new Font("MV Boli", 1, 13));
        vistaAdmin.lblFuenteAjustes.setFont(new Font("MV Boli", 1, 14));
    }
    /**
     * Método para cambiar la fuente de texto de las tablas en la vista del administrador
     * @param vistaAdmin la ventana dónde va a funcionar el método
     */
    public void tablaAjustesAdmin2(VistaAdmin vistaAdmin) {
        vistaAdmin.btnTipoTabla.setText("Fuente 1");
        vistaAdmin.tableUsuarioAdmin.setFont(new Font("Droid Sans Mono Dotted", 1, 12));
        vistaAdmin.tableProductosAdmin.setFont(new Font("Droid Sans Mono Dotted", 1, 12));
        vistaAdmin.tableCategoriasAdmin.setFont(new Font("Droid Sans Mono Dotted", 1, 12));
        vistaAdmin.tableListaAdmin.setFont(new Font("Droid Sans Mono Dotted", 1, 12));
        vistaAdmin.lblFuenteAjustes.setFont(new Font("Droid Sans Mono Dotted", 1, 14));

    }
    /**
     * Método para cambiar la fuente de texto de las tablas
     * @param vistaConsultor la ventana dónde va a funcionar el método
     */
    public void tablaAjustesConsultor1(VistaConsultor vistaConsultor) {
        //Llamamos al objeto que queramos modificar y mediante el método .setFont
        //cambiamos la fuente introduciendo el nombre de la fuente, el estilo y el tamaño
        vistaConsultor.btnTipoTabla.setText("Fuente 2");
        vistaConsultor.tablaUsuarios.setFont(new Font("MV Boli", 1, 13));
        vistaConsultor.tablaProductos.setFont(new Font("MV Boli", 1, 13));
        vistaConsultor.tablaCategorias.setFont(new Font("MV Boli", 1, 13));
        vistaConsultor.lblFuenteAjustes.setFont(new Font("MV Boli", 1, 14));
    }
    /**
     * Método para cambiar la fuente de texto de las tablas en la vista del consultor
     * @param vistaConsultor la ventana dónde va a funcionar el método
     */
    public void tablaAjustesConsultor2(VistaConsultor vistaConsultor) {
        vistaConsultor.btnTipoTabla.setText("Fuente 1");
        vistaConsultor.tablaUsuarios.setFont(new Font("Droid Sans Mono Dotted", 1, 12));
        vistaConsultor.tablaProductos.setFont(new Font("Droid Sans Mono Dotted", 1, 12));
        vistaConsultor.tablaCategorias.setFont(new Font("Droid Sans Mono Dotted", 1, 12));
        vistaConsultor.lblFuenteAjustes.setFont(new Font("Droid Sans Mono Dotted", 1, 14));

    }
    /**
     * Método para cambiar la fuente de texto de las tablas en la vista del cliente
     * @param vistaCliente
     */
    public void tablaAjustesCliente1(VistaCliente vistaCliente, AjustesCliente ajustesCliente) {
        vistaCliente.tablaPedidos       .setFont(new Font("Comic Sans MS", 1 , 14));
        vistaCliente.tablaInfo          .setFont(new Font("Comic Sans MS", 1 , 14));
        ajustesCliente.lblEjemploFuente .setFont(new Font("Comic Sans MS", 1 , 14));
    }
    /**
     * Método para cambiar la fuente de texto de las tablas en la vista del cliente
     * @param vistaCliente
     */
    public void tablaAjustesCliente2(VistaCliente vistaCliente, AjustesCliente ajustesCliente) {
        vistaCliente.tablaPedidos       .setFont(new Font("Corbel", 1 , 14));
        vistaCliente.tablaInfo          .setFont(new Font("Corbel", 1 , 14));
        ajustesCliente.lblEjemploFuente .setFont(new Font("Corbel", 1 , 14));
    }
    /**
     * Método para cambiar la fuente de texto de las tablas en la vista del cliente
     * @param vistaCliente
     */
    public void tablaAjustesCliente3(VistaCliente vistaCliente, AjustesCliente ajustesCliente) {
        vistaCliente.tablaPedidos       .setFont(new Font("Cooper Black", 1 , 12));
        vistaCliente.tablaInfo          .setFont(new Font("Cooper Black", 1 , 12));
        ajustesCliente.lblEjemploFuente .setFont(new Font("Cooper Black", 1 , 12));
    }
    /**
     * Método para cambiar la fuente de texto de las tablas en la vista del cliente
     * @param vistaCliente
     */
    public void tablaAjustesCliente4(VistaCliente vistaCliente, AjustesCliente ajustesCliente) {
        vistaCliente.tablaPedidos       .setFont(new Font("Lucida Calligraphy", 1 , 12));
        vistaCliente.tablaInfo          .setFont(new Font("Lucida Calligraphy", 1 , 12));
        ajustesCliente.lblEjemploFuente .setFont(new Font("Lucida Calligraphy", 1 , 12));
    }

    /**
     * Método para cambiar una imagen que se encuentra dentro de un JLabel
     * @param vistaAdmin la ventana dónde va a funcionar el método
     */
    public void imagenIcono1Admin(VistaAdmin vistaAdmin) {
        //Creamos una variable de tipo Image para guardar la ruta de la imagen
        //que queremos poner en el JLabel
        Image image;
        //Llamamos a la clase toolkit para manipular la imagen e introducimos la ruta de la imagen
        image = Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("Icono1.png"));
        //Creamos una variable Icon donde guardaremos los parámetros
        //para darle tamaño y formato a nuestra foto
        Icon appIcon = new ImageIcon(image.getScaledInstance(vistaAdmin.lblIconoAjustes.getWidth(),
                vistaAdmin.lblIconoAjustes.getHeight(), Image.SCALE_SMOOTH));
        Icon appIcon1 = new ImageIcon(image.getScaledInstance(vistaAdmin.lblIconoProducto.getWidth(),
                vistaAdmin.lblIconoProducto.getHeight(), Image.SCALE_SMOOTH));
        Icon appIcon2 = new ImageIcon(image.getScaledInstance(vistaAdmin.lblIconoUsuario.getWidth(),
                vistaAdmin.lblIconoUsuario.getHeight(), Image.SCALE_SMOOTH));
        Icon appIcon3 = new ImageIcon(image.getScaledInstance(vistaAdmin.lblIconoCategoria.getWidth(),
                vistaAdmin.lblIconoCategoria.getHeight(), Image.SCALE_SMOOTH));

        //Asignamos el Icon que hemos creado a una etiqueta
        vistaAdmin.lblIconoAjustes.setIcon(appIcon);
        vistaAdmin.lblIconoProducto.setIcon(appIcon1);
        vistaAdmin.lblIconoUsuario.setIcon(appIcon2);
        vistaAdmin.lblIconoCategoria.setIcon(appIcon3);
    }
    /**
     * Método para cambiar una imagen que se encuentra dentro de un JLabel
     * @param vistaConsultor la ventana dónde va a funcionar el método
     */
    public void imagenIcono1Consultor(VistaConsultor vistaConsultor) {
        //Creamos una variable de tipo Image para guardar la ruta de la imagen
        //que queremos poner en el JLabel
        Image image;
        //Llamamos a la clase toolkit para manipular la imagen e introducimos la ruta de la imagen
        image = Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("Icono1.png"));
        //Creamos una variable Icon donde guardaremos los parámetros
        //para darle tamaño y formato a nuestra foto
        Icon appIcon = new ImageIcon(image.getScaledInstance(vistaConsultor.lblIconoAjustes.getWidth(),
                vistaConsultor.lblIconoAjustes.getHeight(), Image.SCALE_SMOOTH));
        Icon appIcon1 = new ImageIcon(image.getScaledInstance(vistaConsultor.lblIconoProducto.getWidth(),
                vistaConsultor.lblIconoProducto.getHeight(), Image.SCALE_SMOOTH));
        Icon appIcon2 = new ImageIcon(image.getScaledInstance(vistaConsultor.lblIconoUsuario.getWidth(),
                vistaConsultor.lblIconoUsuario.getHeight(), Image.SCALE_SMOOTH));
        Icon appIcon3 = new ImageIcon(image.getScaledInstance(vistaConsultor.lblIconoCategoria.getWidth(),
                vistaConsultor.lblIconoCategoria.getHeight(), Image.SCALE_SMOOTH));


        vistaConsultor.lblIconoAjustes.setIcon(appIcon);
        vistaConsultor.lblIconoProducto.setIcon(appIcon1);
        vistaConsultor.lblIconoUsuario.setIcon(appIcon2);
        vistaConsultor.lblIconoCategoria.setIcon(appIcon3);
    }
    /**
     * Método para cambiar una imagen que se encuentra dentro de un JLabel
     * @param vistaAdmin la ventana dónde va a funcionar el método
     */
    public void imagenIcono2Admin(VistaAdmin vistaAdmin) {
        //Creamos una variable de tipo Image para guardar la ruta de la imagen
        //que queremos poner en el JLabel
        Image image;
        //Llamamos a la clase toolkit para manipular la imagen e introducimos la ruta de la imagen
        image = Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("Icono2.png"));
        //Creamos una variable Icon donde guardaremos los parámetros
        //para darle tamaño y formato a nuestra foto
        Icon appIcon = new ImageIcon(image.getScaledInstance(vistaAdmin.lblIconoAjustes.getWidth(),
                vistaAdmin.lblIconoAjustes.getHeight(), Image.SCALE_SMOOTH));
        Icon appIcon1 = new ImageIcon(image.getScaledInstance(vistaAdmin.lblIconoProducto.getWidth(),
                vistaAdmin.lblIconoProducto.getHeight(), Image.SCALE_SMOOTH));
        Icon appIcon2 = new ImageIcon(image.getScaledInstance(vistaAdmin.lblIconoUsuario.getWidth(),
                vistaAdmin.lblIconoUsuario.getHeight(), Image.SCALE_SMOOTH));
        Icon appIcon3 = new ImageIcon(image.getScaledInstance(vistaAdmin.lblIconoCategoria.getWidth(),
                vistaAdmin.lblIconoCategoria.getHeight(), Image.SCALE_SMOOTH));

        //Asignamos el Icon que hemos creado a una etiqueta
        vistaAdmin.lblIconoAjustes.setIcon(appIcon);
        vistaAdmin.lblIconoProducto.setIcon(appIcon1);
        vistaAdmin.lblIconoUsuario.setIcon(appIcon2);
        vistaAdmin.lblIconoCategoria.setIcon(appIcon3);
    }
    /**
     * Método para cambiar una imagen que se encuentra dentro de un JLabel
     * @param vistaConsultor la ventana dónde va a funcionar el método
     */
    public void imagenIcono2Consultor(VistaConsultor vistaConsultor) {
        //Creamos una variable de tipo Image para guardar la ruta de la imagen
        //que queremos poner en el JLabel
        Image image;
        //Llamamos a la clase toolkit para manipular la imagen e introducimos la ruta de la imagen
        image = Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("Icono2.png"));
        //Creamos una variable Icon donde guardaremos los parámetros
        //para darle tamaño y formato a nuestra foto
        Icon appIcon = new ImageIcon(image.getScaledInstance(vistaConsultor.lblIconoAjustes.getWidth(),
                vistaConsultor.lblIconoAjustes.getHeight(), Image.SCALE_SMOOTH));
        Icon appIcon1 = new ImageIcon(image.getScaledInstance(vistaConsultor.lblIconoProducto.getWidth(),
                vistaConsultor.lblIconoProducto.getHeight(), Image.SCALE_SMOOTH));
        Icon appIcon2 = new ImageIcon(image.getScaledInstance(vistaConsultor.lblIconoUsuario.getWidth(),
                vistaConsultor.lblIconoUsuario.getHeight(), Image.SCALE_SMOOTH));
        Icon appIcon3 = new ImageIcon(image.getScaledInstance(vistaConsultor.lblIconoCategoria.getWidth(),
                vistaConsultor.lblIconoCategoria.getHeight(), Image.SCALE_SMOOTH));


        vistaConsultor.lblIconoAjustes.setIcon(appIcon);
        vistaConsultor.lblIconoProducto.setIcon(appIcon1);
        vistaConsultor.lblIconoUsuario.setIcon(appIcon2);
        vistaConsultor.lblIconoCategoria.setIcon(appIcon3);
    }
    /**
     * Método para cambiar una imagen que se encuentra dentro de un JLabel
     * @param vistaAdmin la ventana dónde va a funcionar el método
     */
    public void imagenIcono3Admin(VistaAdmin vistaAdmin) {
        //Creamos una variable de tipo Image para guardar la ruta de la imagen
        //que queremos poner en el JLabel
        Image image;
        //Llamamos a la clase toolkit para manipular la imagen e introducimos la ruta de la imagen
        image = Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("Icono3.jpeg"));
        //Creamos una variable Icon donde guardaremos los parámetros
        //para darle tamaño y formato a nuestra foto
        Icon appIcon = new ImageIcon(image.getScaledInstance(vistaAdmin.lblIconoAjustes.getWidth(),
                vistaAdmin.lblIconoAjustes.getHeight(), Image.SCALE_SMOOTH));
        Icon appIcon1 = new ImageIcon(image.getScaledInstance(vistaAdmin.lblIconoProducto.getWidth(),
                vistaAdmin.lblIconoProducto.getHeight(), Image.SCALE_SMOOTH));
        Icon appIcon2 = new ImageIcon(image.getScaledInstance(vistaAdmin.lblIconoUsuario.getWidth(),
                vistaAdmin.lblIconoUsuario.getHeight(), Image.SCALE_SMOOTH));
        Icon appIcon3 = new ImageIcon(image.getScaledInstance(vistaAdmin.lblIconoCategoria.getWidth(),
                vistaAdmin.lblIconoCategoria.getHeight(), Image.SCALE_SMOOTH));

        //Asignamos el Icon que hemos creado a una etiqueta
        vistaAdmin.lblIconoAjustes.setIcon(appIcon);
        vistaAdmin.lblIconoProducto.setIcon(appIcon1);
        vistaAdmin.lblIconoUsuario.setIcon(appIcon2);
        vistaAdmin.lblIconoCategoria.setIcon(appIcon3);
    }
    /**
     * Método para cambiar una imagen que se encuentra dentro de un JLabel
     * @param vistaConsultor la ventana dónde va a funcionar el método
     */
    public void imagenIcono3Consultor(VistaConsultor vistaConsultor) {
        //Creamos una variable de tipo Image para guardar la ruta de la imagen
        //que queremos poner en el JLabel
        Image image;
        //Llamamos a la clase toolkit para manipular la imagen e introducimos la ruta de la imagen
        image = Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("Icono3.jpeg"));
        //Creamos una variable Icon donde guardaremos los parámetros
        //para darle tamaño y formato a nuestra foto
        Icon appIcon = new ImageIcon(image.getScaledInstance(vistaConsultor.lblIconoAjustes.getWidth(),
                vistaConsultor.lblIconoAjustes.getHeight(), Image.SCALE_SMOOTH));
        Icon appIcon1 = new ImageIcon(image.getScaledInstance(vistaConsultor.lblIconoProducto.getWidth(),
                vistaConsultor.lblIconoProducto.getHeight(), Image.SCALE_SMOOTH));
        Icon appIcon2 = new ImageIcon(image.getScaledInstance(vistaConsultor.lblIconoUsuario.getWidth(),
                vistaConsultor.lblIconoUsuario.getHeight(), Image.SCALE_SMOOTH));
        Icon appIcon3 = new ImageIcon(image.getScaledInstance(vistaConsultor.lblIconoCategoria.getWidth(),
                vistaConsultor.lblIconoCategoria.getHeight(), Image.SCALE_SMOOTH));


        vistaConsultor.lblIconoAjustes.setIcon(appIcon);
        vistaConsultor.lblIconoProducto.setIcon(appIcon1);
        vistaConsultor.lblIconoUsuario.setIcon(appIcon2);
        vistaConsultor.lblIconoCategoria.setIcon(appIcon3);
    }

    /**
     * Método mediante el cuál vamos a cambiar el color de los JPanel, JButtons y JLabels
     * @param vistaAdmin la ventana dónde va a funcionar el método
     * @param productDialog la ventana dónde va a funcionar el método
     * @param modifProductDialog la ventana dónde va a funcionar el método
     * @param categoryDialog la ventana dónde va a funcionar el método
     * @param modifUserDialog la ventana dónde va a funcionar el método
     * @param modifCategoryDialog la ventana dónde va a funcionar el método
     */
    public void modoOscuro(VistaAdmin vistaAdmin, ProductDialog productDialog,
                           ModifProductDialog modifProductDialog, CategoryDialog categoryDialog,
                           ModifUserDialog modifUserDialog, ModifCategoryDialog modifCategoryDialog) {
        //Llamamos a los objetos que queremos modificar y mediante
        //el método .setBackground o .setForeground cambiamos
        //los colores del fondo o de la letra, respectivamente

        //Paneles
        vistaAdmin.tabbedPane1    .setBackground(new Color(57, 58, 56));
        vistaAdmin.panel1         .setBackground(new Color(142, 143, 140));
        vistaAdmin.panelLista     .setBackground(new Color(111, 112, 110));
        vistaAdmin.panelProductos .setBackground(new Color(111, 112, 110));
        vistaAdmin.panelUsuarios  .setBackground(new Color(111, 112, 110));
        vistaAdmin.panelCategorias.setBackground(new Color(111, 112, 110));
        vistaAdmin.panelAjustes   .setBackground(new Color(111, 112, 110));
        productDialog.panel1      .setBackground(new Color(111, 112, 110));
        categoryDialog.panel1     .setBackground(new Color(111, 112, 110));
        modifProductDialog.panel1 .setBackground(new Color(111, 112, 110));
        modifUserDialog.panel1    .setBackground(new Color(111, 112, 110));
        modifCategoryDialog.panel1.setBackground(new Color(111, 112, 110));

        //Ventana Modificar
        modifProductDialog.btnModifProd      .setBackground(new Color(60,63,65));
        modifProductDialog.btnAtrasProdModif .setBackground(new Color(60,63,65));
        modifUserDialog.btnHoyUser           .setBackground(new Color(60,63,65));
        modifUserDialog.btnModificarUser     .setBackground(new Color(60,63,65));
        modifUserDialog.btnAtrasModifUser    .setBackground(new Color(60,63,65));
        modifCategoryDialog.btnAtrasCategoria.setBackground(new Color(60,63,65));
        modifCategoryDialog.btnModifCategoria.setBackground(new Color(60,63,65));

        modifProductDialog.btnModifProd      .setForeground(new Color(250,254,245));
        modifProductDialog.btnAtrasProdModif .setForeground(new Color(250,254,245));
        modifUserDialog.btnHoyUser           .setForeground(new Color(250,254,245));
        modifUserDialog.btnModificarUser     .setForeground(new Color(250,254,245));
        modifUserDialog.btnAtrasModifUser    .setForeground(new Color(250,254,245));
        modifCategoryDialog.btnAtrasCategoria.setForeground(new Color(250,254,245));
        modifCategoryDialog.btnModifCategoria.setForeground(new Color(250,254,245));

        //Ventana Insertar
        categoryDialog.btnAtrasCategoria.setBackground(new Color(60,63,65));
        categoryDialog.btnCrearCategoria.setBackground(new Color(60,63,65));
        productDialog.btnAtrasProducto  .setBackground(new Color(60,63,65));
        productDialog.btnCrearProducto  .setBackground(new Color(60,63,65));

        categoryDialog.btnAtrasCategoria.setForeground(new Color(250,254,245));
        categoryDialog.btnCrearCategoria.setForeground(new Color(250,254,245));
        productDialog.btnAtrasProducto  .setForeground(new Color(250,254,245));
        productDialog.btnCrearProducto  .setForeground(new Color(250,254,245));

        //Ventana listado
        vistaAdmin.btnMostrarSinFiltro    .setBackground(new Color(60,63,65));
        vistaAdmin.btnPrecioAltoListaAdmin.setBackground(new Color(60,63,65));
        vistaAdmin.btnNombreListaAdmin    .setBackground(new Color(60,63,65));
        vistaAdmin.btnCategoriaListaAdmin .setBackground(new Color(60,63,65));
        vistaAdmin.btnUsosListaAdmin      .setBackground(new Color(60,63,65));
        vistaAdmin.btnTipoListaAdmin      .setBackground(new Color(60,63,65));
        vistaAdmin.btnPrecioBajoListaAdmin.setBackground(new Color(60,63,65));
        vistaAdmin.btnAOrdenarListaAZ     .setBackground(new Color(60,63,65));
        vistaAdmin.btnAOrdenarListaZA     .setBackground(new Color(60,63,65));
        vistaAdmin.txtNombreListaAdmin    .setBackground(new Color(60,63,65));
        vistaAdmin.txtTipoListaAdmin      .setBackground(new Color(60,63,65));
        vistaAdmin.cbCategoriaListaAdmin  .setBackground(new Color(60,63,65));
        vistaAdmin.cbUsosListaAdmin       .setBackground(new Color(60,63,65));
        vistaAdmin.tableListaAdmin        .setBackground(new Color(60,63,65));
        vistaAdmin.scrollPaneLista        .setBackground(new Color(60,63,65));

        vistaAdmin.btnMostrarSinFiltro    .setForeground(new Color(250,254,245));
        vistaAdmin.btnPrecioAltoListaAdmin.setForeground(new Color(250,254,245));
        vistaAdmin.btnNombreListaAdmin    .setForeground(new Color(250,254,245));
        vistaAdmin.btnCategoriaListaAdmin .setForeground(new Color(250,254,245));
        vistaAdmin.btnUsosListaAdmin      .setForeground(new Color(250,254,245));
        vistaAdmin.btnTipoListaAdmin      .setForeground(new Color(250,254,245));
        vistaAdmin.btnPrecioBajoListaAdmin.setForeground(new Color(250,254,245));
        vistaAdmin.btnAOrdenarListaAZ     .setForeground(new Color(250,254,245));
        vistaAdmin.btnAOrdenarListaZA     .setForeground(new Color(250,254,245));
        vistaAdmin.txtNombreListaAdmin    .setForeground(new Color(250,254,245));
        vistaAdmin.txtTipoListaAdmin      .setForeground(new Color(250,254,245));
        vistaAdmin.cbCategoriaListaAdmin  .setForeground(new Color(250,254,245));
        vistaAdmin.cbUsosListaAdmin       .setForeground(new Color(250,254,245));
        vistaAdmin.tableListaAdmin        .setForeground(new Color(250,254,245));
        vistaAdmin.scrollPaneLista        .setForeground(new Color(250,254,245));

        //Ventana productos
        vistaAdmin.btnBuscarProductosAdmin   .setBackground(new Color(60,63,65));
        vistaAdmin.btnInsertarProductoAdmin  .setBackground(new Color(60,63,65));
        vistaAdmin.btnEliminarProductoAdmin  .setBackground(new Color(60,63,65));
        vistaAdmin.btnModificarProductosAdmin.setBackground(new Color(60,63,65));
        vistaAdmin.txtBuscarProductosAdmin   .setBackground(new Color(60,63,65));
        vistaAdmin.tableProductosAdmin       .setBackground(new Color(60,63,65));
        vistaAdmin.lblProdAdmin              .setBackground(new Color(60,63,65));
        vistaAdmin.lblProducto               .setBackground(new Color(60,63,65));
        vistaAdmin.lblIconoProducto          .setBackground(new Color(60,63,65));
        vistaAdmin.btnMostrarTodoProd        .setBackground(new Color(60,63,65));

        vistaAdmin.btnBuscarProductosAdmin   .setForeground(new Color(250,254,245));
        vistaAdmin.btnInsertarProductoAdmin  .setForeground(new Color(250,254,245));
        vistaAdmin.btnEliminarProductoAdmin  .setForeground(new Color(250,254,245));
        vistaAdmin.btnModificarProductosAdmin.setForeground(new Color(250,254,245));
        vistaAdmin.txtBuscarProductosAdmin   .setForeground(new Color(250,254,245));
        vistaAdmin.tableProductosAdmin       .setForeground(new Color(250,254,245));
        vistaAdmin.lblProdAdmin              .setForeground(new Color(250,254,245));
        vistaAdmin.lblProducto               .setForeground(new Color(250,254,245));
        vistaAdmin.lblIconoProducto          .setForeground(new Color(250,254,245));
        vistaAdmin.btnMostrarTodoProd        .setForeground(new Color(250,254,245));

        //Ventana usuarios
        vistaAdmin.btnEliminarUserAdmin .setBackground(new Color(60,63,65));
        vistaAdmin.btnModificarUserAdmin.setBackground(new Color(60,63,65));
        vistaAdmin.btnBuscarDniAdmin    .setBackground(new Color(60,63,65));
        vistaAdmin.txtBuscarUserAdmin   .setBackground(new Color(60,63,65));
        vistaAdmin.lblUserAdmin         .setBackground(new Color(60,63,65));
        vistaAdmin.lblUsuario           .setBackground(new Color(60,63,65));
        vistaAdmin.lblIconoUsuario      .setBackground(new Color(60,63,65));
        vistaAdmin.tableUsuarioAdmin    .setBackground(new Color(60,63,65));
        vistaAdmin.btnMostrarTodoUser   .setBackground(new Color(60,63,65));

        vistaAdmin.btnEliminarUserAdmin .setForeground(new Color(250,254,245));
        vistaAdmin.btnModificarUserAdmin.setForeground(new Color(250,254,245));
        vistaAdmin.btnBuscarDniAdmin    .setForeground(new Color(250,254,245));
        vistaAdmin.txtBuscarUserAdmin   .setForeground(new Color(250,254,245));
        vistaAdmin.lblUserAdmin         .setForeground(new Color(250,254,245));
        vistaAdmin.lblUsuario           .setForeground(new Color(250,254,245));
        vistaAdmin.lblIconoUsuario      .setForeground(new Color(250,254,245));
        vistaAdmin.tableUsuarioAdmin    .setForeground(new Color(250,254,245));
        vistaAdmin.btnMostrarTodoUser   .setForeground(new Color(250,254,245));

        //Ventana categorias
        vistaAdmin.btnBuscarCategoriaAdmin   .setBackground(new Color(60,63,65));
        vistaAdmin.btnEliminarCategoriaAdmin .setBackground(new Color(60,63,65));
        vistaAdmin.btnModificarCategoriaAdmin.setBackground(new Color(60,63,65));
        vistaAdmin.btnInsertarCategoriaAdmin .setBackground(new Color(60,63,65));
        vistaAdmin.cbBuscarCategoria         .setBackground(new Color(60,63,65));
        vistaAdmin.lblCatAdmin               .setBackground(new Color(60,63,65));
        vistaAdmin.lblCategoria              .setBackground(new Color(60,63,65));
        vistaAdmin.tableCategoriasAdmin      .setBackground(new Color(60,63,65));
        vistaAdmin.lblIconoCategoria         .setBackground(new Color(60,63,65));
        vistaAdmin.btnMostrarTodoCat         .setBackground(new Color(60,63,65));

        vistaAdmin.btnBuscarCategoriaAdmin   .setForeground(new Color(250,254,245));
        vistaAdmin.btnEliminarCategoriaAdmin .setForeground(new Color(250,254,245));
        vistaAdmin.btnModificarCategoriaAdmin.setForeground(new Color(250,254,245));
        vistaAdmin.btnInsertarCategoriaAdmin .setForeground(new Color(250,254,245));
        vistaAdmin.cbBuscarCategoria         .setForeground(new Color(250,254,245));
        vistaAdmin.lblCatAdmin               .setForeground(new Color(250,254,245));
        vistaAdmin.lblCategoria              .setForeground(new Color(250,254,245));
        vistaAdmin.tableCategoriasAdmin      .setForeground(new Color(250,254,245));
        vistaAdmin.lblIconoCategoria         .setForeground(new Color(250,254,245));
        vistaAdmin.btnMostrarTodoCat         .setForeground(new Color(250,254,245));

        //Ventana ajustes
        vistaAdmin.btnModoOscuro  .setBackground(new Color(60,63,65));
        vistaAdmin.btnTipoTabla   .setBackground(new Color(60,63,65));
        vistaAdmin.lblIconoAjustes.setBackground(new Color(60,63,65));
        vistaAdmin.lblAjustes     .setBackground(new Color(60,63,65));
        vistaAdmin.btnIcono1      .setBackground(new Color(111,112,110));
        vistaAdmin.btnIcono2      .setBackground(new Color(111,112,110));
        vistaAdmin.btnIcono3      .setBackground(new Color(111,112,110));

        vistaAdmin.btnModoOscuro  .setForeground(new Color(250,254,245));
        vistaAdmin.btnTipoTabla   .setForeground(new Color(250,254,245));
        vistaAdmin.lblIconoAjustes.setForeground(new Color(250,254,245));
        vistaAdmin.lblAjustes     .setForeground(new Color(250,254,245));
        vistaAdmin.btnIcono1      .setForeground(new Color(250,254,245));
        vistaAdmin.btnIcono2      .setForeground(new Color(250,254,245));
        vistaAdmin.btnIcono3      .setForeground(new Color(250,254,245));

        //Títulos
        vistaAdmin.tabbedPane1  .setForeground(new Color(250, 254, 245));
        vistaAdmin.lblAjustes   .setForeground(new Color(182, 183, 179));
        vistaAdmin.lblFiltrarPor.setForeground(new Color(182, 183, 179));
        vistaAdmin.lblLista     .setForeground(new Color(182, 183, 179));
        vistaAdmin.lblProducto  .setForeground(new Color(182, 183, 179));
        vistaAdmin.lblUsuario   .setForeground(new Color(182, 183, 179));
        vistaAdmin.lblCategoria .setForeground(new Color(182, 183, 179));


    }
    /**
     * Método mediante el cuál vamos a cambiar el color de los JPanel, JButtons y JLabels
     * @param vistaAdmin la ventana dónde va a funcionar el método
     * @param productDialog la ventana dónde va a funcionar el método
     * @param modifProductDialog la ventana dónde va a funcionar el método
     * @param categoryDialog la ventana dónde va a funcionar el método
     * @param modifUserDialog la ventana dónde va a funcionar el método
     * @param modifCategoryDialog la ventana dónde va a funcionar el método
     */
    public void modoClaro(VistaAdmin vistaAdmin, ProductDialog productDialog,
                          ModifProductDialog modifProductDialog, CategoryDialog categoryDialog,
                          ModifUserDialog modifUserDialog, ModifCategoryDialog modifCategoryDialog) {
        //Paneles
        vistaAdmin.tabbedPane1    .setBackground(new Color(169, 215, 209));
        vistaAdmin.panel1         .setBackground(new Color(193, 221, 243));
        vistaAdmin.panelLista     .setBackground(new Color(188, 234, 239));
        vistaAdmin.panelProductos .setBackground(new Color(188, 234, 239));
        vistaAdmin.panelUsuarios  .setBackground(new Color(188, 234, 239));
        vistaAdmin.panelCategorias.setBackground(new Color(188, 234, 239));
        vistaAdmin.panelAjustes   .setBackground(new Color(188, 234, 239));
        productDialog.panel1      .setBackground(new Color(188, 234, 239));
        modifProductDialog.panel1 .setBackground(new Color(188, 234, 239));
        modifUserDialog.panel1    .setBackground(new Color(188, 234, 239));
        modifCategoryDialog.panel1.setBackground(new Color(188, 234, 239));
        categoryDialog.panel1     .setBackground(new Color(188, 234, 239));
        modifUserDialog.panel1    .setBackground(new Color(188, 234, 239));

        //Ventana Modificar
        modifProductDialog.btnModifProd      .setBackground(new Color(169,215,209));
        modifProductDialog.btnAtrasProdModif .setBackground(new Color(169,215,209));
        modifUserDialog.btnHoyUser           .setBackground(new Color(169,215,209));
        modifUserDialog.btnModificarUser     .setBackground(new Color(169,215,209));
        modifUserDialog.btnAtrasModifUser    .setBackground(new Color(169,215,209));
        modifCategoryDialog.btnAtrasCategoria.setBackground(new Color(169,215,209));
        modifCategoryDialog.btnModifCategoria.setBackground(new Color(169,215,209));

        modifProductDialog.btnModifProd      .setForeground(new Color(57,58,56));
        modifProductDialog.btnAtrasProdModif .setForeground(new Color(57,58,56));
        modifUserDialog.btnHoyUser           .setForeground(new Color(57,58,56));
        modifUserDialog.btnModificarUser     .setForeground(new Color(57,58,56));
        modifUserDialog.btnAtrasModifUser    .setForeground(new Color(57,58,56));
        modifCategoryDialog.btnAtrasCategoria.setForeground(new Color(57,58,56));
        modifCategoryDialog.btnModifCategoria.setForeground(new Color(57,58,56));

        //Ventana Insertar
        categoryDialog.btnAtrasCategoria.setBackground(new Color(169,215,209));
        categoryDialog.btnCrearCategoria.setBackground(new Color(169,215,209));
        productDialog.btnAtrasProducto  .setBackground(new Color(169,215,209));
        productDialog.btnCrearProducto  .setBackground(new Color(169,215,209));

        categoryDialog.btnAtrasCategoria.setForeground(new Color(57,58,56));
        categoryDialog.btnCrearCategoria.setForeground(new Color(57,58,56));
        productDialog.btnAtrasProducto  .setForeground(new Color(57,58,56));
        productDialog.btnCrearProducto  .setForeground(new Color(57,58,56));

        //Ventana listado
        vistaAdmin.btnMostrarSinFiltro    .setBackground(new Color(169,215,209));
        vistaAdmin.btnPrecioAltoListaAdmin.setBackground(new Color(169,215,209));
        vistaAdmin.btnNombreListaAdmin    .setBackground(new Color(169,215,209));
        vistaAdmin.btnCategoriaListaAdmin .setBackground(new Color(169,215,209));
        vistaAdmin.btnUsosListaAdmin      .setBackground(new Color(169,215,209));
        vistaAdmin.btnTipoListaAdmin      .setBackground(new Color(169,215,209));
        vistaAdmin.btnPrecioBajoListaAdmin.setBackground(new Color(169,215,209));
        vistaAdmin.btnAOrdenarListaAZ     .setBackground(new Color(169,215,209));
        vistaAdmin.btnAOrdenarListaZA     .setBackground(new Color(169,215,209));
        vistaAdmin.txtNombreListaAdmin    .setBackground(new Color(169,215,209));
        vistaAdmin.txtTipoListaAdmin      .setBackground(new Color(169,215,209));
        vistaAdmin.cbCategoriaListaAdmin  .setBackground(new Color(169,215,209));
        vistaAdmin.cbUsosListaAdmin       .setBackground(new Color(169,215,209));
        vistaAdmin.tableListaAdmin        .setBackground(new Color(169,215,209));
        vistaAdmin.scrollPaneLista        .setBackground(new Color(169,215,209));

        vistaAdmin.btnMostrarSinFiltro    .setForeground(new Color(57,58,56));
        vistaAdmin.btnPrecioAltoListaAdmin.setForeground(new Color(57,58,56));
        vistaAdmin.btnNombreListaAdmin    .setForeground(new Color(57,58,56));
        vistaAdmin.btnCategoriaListaAdmin .setForeground(new Color(57,58,56));
        vistaAdmin.btnUsosListaAdmin      .setForeground(new Color(57,58,56));
        vistaAdmin.btnTipoListaAdmin      .setForeground(new Color(57,58,56));
        vistaAdmin.btnPrecioBajoListaAdmin.setForeground(new Color(57,58,56));
        vistaAdmin.btnAOrdenarListaAZ     .setForeground(new Color(57,58,56));
        vistaAdmin.btnAOrdenarListaZA     .setForeground(new Color(57,58,56));
        vistaAdmin.txtNombreListaAdmin    .setForeground(new Color(57,58,56));
        vistaAdmin.txtTipoListaAdmin      .setForeground(new Color(57,58,56));
        vistaAdmin.cbCategoriaListaAdmin  .setForeground(new Color(57,58,56));
        vistaAdmin.cbUsosListaAdmin       .setForeground(new Color(57,58,56));
        vistaAdmin.tableListaAdmin        .setForeground(new Color(57,58,56));
        vistaAdmin.scrollPaneLista        .setForeground(new Color(57,58,56));

        //Ventana productos
        vistaAdmin.btnBuscarProductosAdmin   .setBackground(new Color(169,215,209));
        vistaAdmin.btnInsertarProductoAdmin  .setBackground(new Color(169,215,209));
        vistaAdmin.btnEliminarProductoAdmin  .setBackground(new Color(169,215,209));
        vistaAdmin.btnModificarProductosAdmin.setBackground(new Color(169,215,209));
        vistaAdmin.txtBuscarProductosAdmin   .setBackground(new Color(169,215,209));
        vistaAdmin.tableProductosAdmin       .setBackground(new Color(169,215,209));
        vistaAdmin.lblProdAdmin              .setBackground(new Color(169,215,209));
        vistaAdmin.lblProducto               .setBackground(new Color(169,215,209));
        vistaAdmin.lblIconoProducto          .setBackground(new Color(169,215,209));
        vistaAdmin.btnMostrarTodoProd        .setBackground(new Color(169,215,209));

        vistaAdmin.btnBuscarProductosAdmin   .setForeground(new Color(57,58,56));
        vistaAdmin.btnInsertarProductoAdmin  .setForeground(new Color(57,58,56));
        vistaAdmin.btnEliminarProductoAdmin  .setForeground(new Color(57,58,56));
        vistaAdmin.btnModificarProductosAdmin.setForeground(new Color(57,58,56));
        vistaAdmin.txtBuscarProductosAdmin   .setForeground(new Color(57,58,56));
        vistaAdmin.tableProductosAdmin       .setForeground(new Color(57,58,56));
        vistaAdmin.lblProdAdmin              .setForeground(new Color(57,58,56));
        vistaAdmin.lblProducto               .setForeground(new Color(57,58,56));
        vistaAdmin.lblIconoProducto          .setForeground(new Color(57,58,56));
        vistaAdmin.btnMostrarTodoProd          .setForeground(new Color(57,58,56));

        //Ventana usuarios
        vistaAdmin.btnEliminarUserAdmin .setBackground(new Color(169,215,209));
        vistaAdmin.btnModificarUserAdmin.setBackground(new Color(169,215,209));
        vistaAdmin.btnBuscarDniAdmin    .setBackground(new Color(169,215,209));
        vistaAdmin.txtBuscarUserAdmin   .setBackground(new Color(169,215,209));
        vistaAdmin.lblUserAdmin         .setBackground(new Color(169,215,209));
        vistaAdmin.lblUsuario           .setBackground(new Color(169,215,209));
        vistaAdmin.lblIconoUsuario      .setBackground(new Color(169,215,209));
        vistaAdmin.tableUsuarioAdmin    .setBackground(new Color(169,215,209));
        vistaAdmin.btnMostrarTodoUser   .setBackground(new Color(169,215,209));

        vistaAdmin.btnEliminarUserAdmin .setForeground(new Color(57,58,56));
        vistaAdmin.btnModificarUserAdmin.setForeground(new Color(57,58,56));
        vistaAdmin.btnBuscarDniAdmin    .setForeground(new Color(57,58,56));
        vistaAdmin.txtBuscarUserAdmin   .setForeground(new Color(57,58,56));
        vistaAdmin.lblUserAdmin         .setForeground(new Color(57,58,56));
        vistaAdmin.lblUsuario           .setForeground(new Color(57,58,56));
        vistaAdmin.lblIconoUsuario      .setForeground(new Color(57,58,56));
        vistaAdmin.tableUsuarioAdmin    .setForeground(new Color(57,58,56));
        vistaAdmin.btnMostrarTodoUser   .setForeground(new Color(57,58,56));

        //Ventana categorias
        vistaAdmin.btnBuscarCategoriaAdmin   .setBackground(new Color(169,215,209));
        vistaAdmin.btnEliminarCategoriaAdmin .setBackground(new Color(169,215,209));
        vistaAdmin.btnModificarCategoriaAdmin.setBackground(new Color(169,215,209));
        vistaAdmin.btnInsertarCategoriaAdmin .setBackground(new Color(169,215,209));
        vistaAdmin.cbBuscarCategoria         .setBackground(new Color(169,215,209));
        vistaAdmin.lblCatAdmin               .setBackground(new Color(169,215,209));
        vistaAdmin.lblCategoria              .setBackground(new Color(169,215,209));
        vistaAdmin.tableCategoriasAdmin      .setBackground(new Color(169,215,209));
        vistaAdmin.lblIconoCategoria         .setBackground(new Color(169,215,209));
        vistaAdmin.btnMostrarTodoCat         .setBackground(new Color(169,215,209));

        vistaAdmin.btnBuscarCategoriaAdmin   .setForeground(new Color(57,58,56));
        vistaAdmin.btnEliminarCategoriaAdmin .setForeground(new Color(57,58,56));
        vistaAdmin.btnModificarCategoriaAdmin.setForeground(new Color(57,58,56));
        vistaAdmin.btnInsertarCategoriaAdmin .setForeground(new Color(57,58,56));
        vistaAdmin.cbBuscarCategoria         .setForeground(new Color(57,58,56));
        vistaAdmin.lblCatAdmin               .setForeground(new Color(57,58,56));
        vistaAdmin.lblCategoria              .setForeground(new Color(57,58,56));
        vistaAdmin.tableCategoriasAdmin      .setForeground(new Color(57,58,56));
        vistaAdmin.lblIconoCategoria         .setForeground(new Color(57,58,56));
        vistaAdmin.btnMostrarTodoCat         .setForeground(new Color(57,58,56));

        //Ventana ajustes
        vistaAdmin.btnModoOscuro  .setBackground(new Color(234, 235, 226));
        vistaAdmin.btnTipoTabla   .setBackground(new Color(234, 235, 226));
        vistaAdmin.lblIconoAjustes.setBackground(new Color(169,215,209));
        vistaAdmin.lblAjustes     .setBackground(new Color(169,215,209));
        vistaAdmin.btnIcono1      .setBackground(new Color(188, 234, 239));
        vistaAdmin.btnIcono2      .setBackground(new Color(188, 234, 239));
        vistaAdmin.btnIcono3      .setBackground(new Color(188, 234, 239));

        vistaAdmin.btnModoOscuro  .setForeground(new Color(57,58,56));
        vistaAdmin.btnTipoTabla   .setForeground(new Color(57,58,56));
        vistaAdmin.lblIconoAjustes.setForeground(new Color(57,58,56));
        vistaAdmin.lblAjustes     .setForeground(new Color(57,58,56));
        vistaAdmin.btnIcono1      .setForeground(new Color(57,58,56));
        vistaAdmin.btnIcono2      .setForeground(new Color(57,58,56));
        vistaAdmin.btnIcono3      .setForeground(new Color(57,58,56));

        //Títulos
        vistaAdmin.tabbedPane1          .setForeground(new Color(57, 58, 56));
        vistaAdmin.lblAjustes           .setForeground(new Color(134, 108,153));
        vistaAdmin.lblFiltrarPor        .setForeground(new Color(134, 108,153));
        vistaAdmin.lblLista             .setForeground(new Color(134, 108,153));
        vistaAdmin.lblProducto          .setForeground(new Color(134, 108,153));
        vistaAdmin.lblUsuario           .setForeground(new Color(134, 108,153));
        vistaAdmin.lblCategoria         .setForeground(new Color(134, 108,153));

    }
    /**
     * Método mediante el cuál vamos a cambiar el color de los JPanel, JButtons y JLabels
     * @param ajustesCliente la ventana dónde va a funcionar el método
     * @param vistaCliente la ventana dónde va a  funcionar el método
     */
    public void modoOscuroCliente(AjustesCliente ajustesCliente, VistaCliente vistaCliente) {
        ajustesCliente.panel1         .setBackground(new Color(142,143,140));
        vistaCliente.panel1           .setBackground(new Color(142,143,140));

        ajustesCliente.btnFuenteCuatro.setBackground(new Color(60, 63, 65));
        ajustesCliente.btnFuenteTres  .setBackground(new Color(60, 63, 65));
        ajustesCliente.btnFuenteDos   .setBackground(new Color(60, 63, 65));
        ajustesCliente.btnFuenteUno   .setBackground(new Color(60, 63, 65));
        ajustesCliente.btnModoOscuro  .setBackground(new Color(60, 63, 65));

        ajustesCliente.btnFuenteCuatro .setForeground(new Color(250,254,245));
        ajustesCliente.btnFuenteTres   .setForeground(new Color(250,254,245));
        ajustesCliente.btnFuenteDos    .setForeground(new Color(250,254,245));
        ajustesCliente.btnFuenteUno    .setForeground(new Color(250,254,245));
        ajustesCliente.btnModoOscuro   .setForeground(new Color(250,254,245));
        ajustesCliente.lblCambiarFuente.setForeground(new Color(250,254,245));
        ajustesCliente.lblEjemploFuente.setForeground(new Color(250,254,245));
        ajustesCliente.lblModoOscuro   .setForeground(new Color(250,254,245));

        vistaCliente.btnEliminarInfo    .setBackground(new Color(60,63,65));
        vistaCliente.btnCrearInfo       .setBackground(new Color(60,63,65));
        vistaCliente.btnEliminarProducto.setBackground(new Color(60,63,65));
        vistaCliente.btnGenerarAlbaran  .setBackground(new Color(60,63,65));
        vistaCliente.btnInsertarProducto.setBackground(new Color(60,63,65));
        vistaCliente.btnVaciarPedido    .setBackground(new Color(60,63,65));
        vistaCliente.txtCPCliente       .setBackground(new Color(60,63,65));
        vistaCliente.txtPortalCliente   .setBackground(new Color(60,63,65));
        vistaCliente.txtCantidad        .setBackground(new Color(60,63,65));
        vistaCliente.txtCalleCliente    .setBackground(new Color(60,63,65));
        vistaCliente.txtCiudadCliente   .setBackground(new Color(60,63,65));
        vistaCliente.cbProductoCliente  .setBackground(new Color(60,63,65));
        vistaCliente.cbInfoCliente      .setBackground(new Color(60,63,65));
        vistaCliente.cbUsuarioCliente   .setBackground(new Color(60,63,65));
        vistaCliente.tablaPedidos       .setBackground(new Color(60,63,65));
        vistaCliente.tablaInfo          .setBackground(new Color(60,63,65));

        vistaCliente.btnEliminarInfo    .setForeground(new Color(250,254,245));
        vistaCliente.btnCrearInfo       .setForeground(new Color(250,254,245));
        vistaCliente.btnEliminarProducto.setForeground(new Color(250,254,245));
        vistaCliente.btnGenerarAlbaran  .setForeground(new Color(250,254,245));
        vistaCliente.btnInsertarProducto.setForeground(new Color(250,254,245));
        vistaCliente.btnVaciarPedido    .setForeground(new Color(250,254,245));
        vistaCliente.txtCPCliente       .setForeground(new Color(250,254,245));
        vistaCliente.txtPortalCliente   .setForeground(new Color(250,254,245));
        vistaCliente.txtCantidad        .setForeground(new Color(250,254,245));
        vistaCliente.txtCalleCliente    .setForeground(new Color(250,254,245));
        vistaCliente.txtCiudadCliente   .setForeground(new Color(250,254,245));
        vistaCliente.cbProductoCliente  .setForeground(new Color(250,254,245));
        vistaCliente.cbInfoCliente      .setForeground(new Color(250,254,245));
        vistaCliente.cbUsuarioCliente   .setForeground(new Color(250,254,245));
        vistaCliente.tablaPedidos       .setForeground(new Color(250,254,245));
        vistaCliente.tablaInfo          .setForeground(new Color(250,254,245));

        vistaCliente.lblCalleInfo       .setForeground(new Color(250,254,245));
        vistaCliente.lblCiudadInfo      .setForeground(new Color(250,254,245));
        vistaCliente.lblCPInfo          .setForeground(new Color(250,254,245));
        vistaCliente.lblFechaInfo       .setForeground(new Color(250,254,245));
        vistaCliente.lblPortalInfo      .setForeground(new Color(250,254,245));
        vistaCliente.lblUsuarioInfo     .setForeground(new Color(250,254,245));
        vistaCliente.lblCantidadDetalle .setForeground(new Color(250,254,245));
        vistaCliente.lblDetalle         .setForeground(new Color(250,254,245));
        vistaCliente.lblPedido          .setForeground(new Color(250,254,245));
        vistaCliente.lblInfoDetalle     .setForeground(new Color(250,254,245));
        vistaCliente.lblProductoDetalle .setForeground(new Color(250,254,245));
    }
    /**
     * Método mediante el cuál vamos a cambiar el color de los JPanel, JButtons y JLabels
     * @param ajustesCliente la ventana dónde va a funcionar el método
     * @param vistaCliente la ventana dónde va a  funcionar el método
     */
    public void modoClaroCliente(AjustesCliente ajustesCliente, VistaCliente vistaCliente) {
        ajustesCliente.panel1         .setBackground(new Color(188,234,239));
        vistaCliente.panel1           .setBackground(new Color(188,234,239));

        ajustesCliente.btnFuenteCuatro.setBackground(new Color(169, 215, 209));
        ajustesCliente.btnFuenteTres  .setBackground(new Color(169, 215, 209));
        ajustesCliente.btnFuenteDos   .setBackground(new Color(169, 215, 209));
        ajustesCliente.btnFuenteUno   .setBackground(new Color(169, 215, 209));
        ajustesCliente.btnModoOscuro  .setBackground(new Color(169, 215, 209));

        ajustesCliente.btnFuenteCuatro .setForeground(new Color(57,58,56));
        ajustesCliente.btnFuenteTres   .setForeground(new Color(57,58,56));
        ajustesCliente.btnFuenteDos    .setForeground(new Color(57,58,56));
        ajustesCliente.btnFuenteUno    .setForeground(new Color(57,58,56));
        ajustesCliente.btnModoOscuro   .setForeground(new Color(57,58,56));
        ajustesCliente.lblCambiarFuente.setForeground(new Color(57,58,56));
        ajustesCliente.lblEjemploFuente.setForeground(new Color(57,58,56));
        ajustesCliente.lblModoOscuro   .setForeground(new Color(57,58,56));

        vistaCliente.btnEliminarInfo    .setBackground(new Color(169,215,209));
        vistaCliente.btnCrearInfo       .setBackground(new Color(169,215,209));
        vistaCliente.btnEliminarProducto.setBackground(new Color(169,215,209));
        vistaCliente.btnGenerarAlbaran  .setBackground(new Color(169,215,209));
        vistaCliente.btnInsertarProducto.setBackground(new Color(169,215,209));
        vistaCliente.btnVaciarPedido    .setBackground(new Color(169,215,209));
        vistaCliente.txtCPCliente       .setBackground(new Color(169,215,209));
        vistaCliente.txtPortalCliente   .setBackground(new Color(169,215,209));
        vistaCliente.txtCantidad        .setBackground(new Color(169,215,209));
        vistaCliente.txtCalleCliente    .setBackground(new Color(169,215,209));
        vistaCliente.txtCiudadCliente   .setBackground(new Color(169,215,209));
        vistaCliente.cbProductoCliente  .setBackground(new Color(169,215,209));
        vistaCliente.cbInfoCliente      .setBackground(new Color(169,215,209));
        vistaCliente.cbUsuarioCliente   .setBackground(new Color(169,215,209));
        vistaCliente.tablaInfo          .setBackground(new Color(169,215,209));
        vistaCliente.tablaPedidos       .setBackground(new Color(169,215,209));

        vistaCliente.btnEliminarInfo    .setForeground(new Color(57,58,56));
        vistaCliente.btnCrearInfo       .setForeground(new Color(57,58,56));
        vistaCliente.btnEliminarProducto.setForeground(new Color(57,58,56));
        vistaCliente.btnGenerarAlbaran  .setForeground(new Color(57,58,56));
        vistaCliente.btnInsertarProducto.setForeground(new Color(57,58,56));
        vistaCliente.btnVaciarPedido    .setForeground(new Color(57,58,56));
        vistaCliente.txtCPCliente       .setForeground(new Color(57,58,56));
        vistaCliente.txtPortalCliente   .setForeground(new Color(57,58,56));
        vistaCliente.txtCantidad        .setForeground(new Color(57,58,56));
        vistaCliente.txtCalleCliente    .setForeground(new Color(57,58,56));
        vistaCliente.txtCiudadCliente   .setForeground(new Color(57,58,56));
        vistaCliente.cbProductoCliente  .setForeground(new Color(57,58,56));
        vistaCliente.cbInfoCliente      .setForeground(new Color(57,58,56));
        vistaCliente.cbUsuarioCliente   .setForeground(new Color(57,58,56));
        vistaCliente.tablaPedidos       .setForeground(new Color(57,58,56));
        vistaCliente.tablaInfo          .setForeground(new Color(57,58,56));

        vistaCliente.lblCalleInfo       .setForeground(new Color(57,58,56));
        vistaCliente.lblCiudadInfo      .setForeground(new Color(57,58,56));
        vistaCliente.lblCPInfo          .setForeground(new Color(57,58,56));
        vistaCliente.lblFechaInfo       .setForeground(new Color(57,58,56));
        vistaCliente.lblPortalInfo      .setForeground(new Color(57,58,56));
        vistaCliente.lblUsuarioInfo     .setForeground(new Color(57,58,56));
        vistaCliente.lblCantidadDetalle .setForeground(new Color(57,58,56));
        vistaCliente.lblDetalle         .setForeground(new Color(57,58,56));
        vistaCliente.lblPedido          .setForeground(new Color(57,58,56));
        vistaCliente.lblInfoDetalle     .setForeground(new Color(57,58,56));
        vistaCliente.lblProductoDetalle .setForeground(new Color(57,58,56));
    }

    /**
     * Método para vaciar los campos de texto de la ventana del login
     * @param loginDialog la ventana dónde va a funcionar el método
     */
    public void vaciarLogin(LoginDialog loginDialog) {
        loginDialog.txtDNILogin.setText(null);
        loginDialog.txtPsswdLogin.setText(null);
        loginDialog.cbTipoUsuario.setSelectedItem(null);
    }

    /**
     * Método booleano que sirve para comprobar si alguno de los campos está vacio
     * @param vistaCliente la ventana dónde va a funcionar el método
     * @return si algún campo está vacio
     */
    public boolean comprobarPedidoVacio(VistaCliente vistaCliente) {
        return  vistaCliente.cbProductoCliente.getSelectedIndex() == -1 ||
                vistaCliente.txtCantidad.getText().isEmpty() ||
                vistaCliente.cbInfoCliente.getSelectedIndex() == -1;
    }
    /**
     * Método booleano que sirve para comprobar si alguno de los campos está vacio
     * @param vistaCliente la ventana dónde va a funcionar el método
     * @return si algún campo está vacio
     */
    public boolean comprobarInfoVacia(VistaCliente vistaCliente) {
        return vistaCliente.txtCiudadCliente.getText().isEmpty() ||
               vistaCliente.txtCPCliente.getText().isEmpty() ||
               vistaCliente.txtCalleCliente.getText().isEmpty() ||
               vistaCliente.cbUsuarioCliente.getSelectedIndex() == -1 ||
               vistaCliente.txtPortalCliente.getText().isEmpty();
    }
    /**
     * Método booleano que sirve para comprobar si alguno de los campos está vacio
     * @param registerDialog la ventana dónde va a funcionar el método
     * @return si algún campo está vacio
     */
    public boolean comprobarUsuarioVacio(RegisterDialog registerDialog) {
        return  registerDialog.txtNombreRegister.getText().isEmpty() ||
                registerDialog.txtApellidoRegister.getText().isEmpty() ||
                registerDialog.cbTipoRegister.getSelectedIndex() == -1 ||
                registerDialog.dpCumpleRegister.getText().isEmpty() ||
                registerDialog.txtDniRegister.getText().isEmpty() ||
                registerDialog.txtPsswdRegister.getText().isEmpty() ||
                registerDialog.txtTelefonoRegister.getText().isEmpty() ||
                registerDialog.txtAltaRegister.getText().isEmpty();
    }
    /**
     * Método booleano que sirve para comprobar si alguno de los campos está vacio
     * @param categoryDialog la ventana dónde va a funcionar el método
     * @return si algún campo está vacio
     */
    public boolean comprobarCategoriaVacia(CategoryDialog categoryDialog) {
        return categoryDialog.txtCodigoCategoria.getText().isEmpty() ||
                categoryDialog.txtNombreCategoria.getText().isEmpty() ||
                categoryDialog.txtDescripcionCategoria.getText().isEmpty();
    }
    /**
     * Método booleano que sirve para comprobar si alguno de los campos está vacio
     * @param productDialog la ventana dónde va a funcionar el método
     * @return si algún campo está vacio
     */
    public boolean comprobarProductoVacio(ProductDialog productDialog) {
        return productDialog.txtCodigoProducto.getText().isEmpty() ||
                productDialog.txtNombreProducto.getText().isEmpty() ||
                productDialog.cbCategoriaProducto.getSelectedIndex() == -1 ||
                productDialog.txtPrecioProducto.getText().isEmpty() ||
                productDialog.txtCantidadProducto.getText().isEmpty() ||
                productDialog.txtMedidaProducto.getText().isEmpty() ||
                productDialog.txtTipoProducto.getText().isEmpty() ||
                productDialog.cbUsosProducto.getSelectedIndex() == -1;

    }
    /**
     * Método booleano que sirve para comprobar si alguno de los campos está vacio
     * @param modifProductDialog la ventana dónde va a funcionar el método
     * @return si algún campo está vacio
     */
    public boolean comprobarProductoModifVacio(ModifProductDialog modifProductDialog) {
        return modifProductDialog.txtCodigoProductoModif.getText().isEmpty() ||
                modifProductDialog.txtNombreProductoModif.getText().isEmpty() ||
                modifProductDialog.cbCatProductoModif.getSelectedIndex() == -1 ||
                modifProductDialog.txtPrecioProductoModif.getText().isEmpty() ||
                modifProductDialog.txtCantidadProductoModif.getText().isEmpty() ||
                modifProductDialog.txtMedidaProductoModif.getText().isEmpty() ||
                modifProductDialog.txtTipoProductoModif.getText().isEmpty() ||
                modifProductDialog.cbUsosProductoModif.getSelectedIndex() == -1;

    }
    /**
     * Método booleano que sirve para comprobar si alguno de los campos está vacio
     * @param modifUserDialog la ventana dónde va a funcionar el método
     * @return si algún campo está vacio
     */
    public boolean comprobarUsuarioModifVacio(ModifUserDialog modifUserDialog) {
        return  modifUserDialog.txtNombreUser.getText().isEmpty() ||
                modifUserDialog.txtApellidoUser.getText().isEmpty() ||
                modifUserDialog.cbTipoUser.getSelectedIndex() == -1 ||
                modifUserDialog.dpCumpleUser.getText().isEmpty() ||
                modifUserDialog.txtDniUser.getText().isEmpty() ||
                modifUserDialog.txtPsswdUser.getText().isEmpty() ||
                modifUserDialog.txtTelefonoUser.getText().isEmpty() ||
                modifUserDialog.txtAltaUser.getText().isEmpty();
    }
    /**
     * Método booleano que sirve para comprobar si alguno de los campos está vacio
     * @param modifCategoryDialog la ventana dónde va a funcionar el método
     * @return si algún campo está vacio
     */
    public boolean comprobarCategoriaModifVacia(ModifCategoryDialog modifCategoryDialog) {
        return  modifCategoryDialog.txtCodigoModif.getText().isEmpty() ||
                modifCategoryDialog.txtNombreModif.getText().isEmpty() ||
                modifCategoryDialog.txtDescripcionModif.getText().isEmpty();
    }

    /**
     * Método para vaciar los campos del formulario de creación de la información de un pedido
     * @param vistaCliente la ventana dónde va a funcionar el método
     */
    public void borrarCamposPedido(VistaCliente vistaCliente) {
        vistaCliente.cbProductoCliente.setSelectedIndex(-1);
        vistaCliente.txtCantidad.setText("");
    }
    /**
     * Método para vaciar los campos del formulario de creación de la información de un producto
     * @param vistaCliente la ventana dónde va a funcionar el método
     */
    public void borrarCamposInfo(VistaCliente vistaCliente) {
        vistaCliente.txtCiudadCliente.setText("");
        vistaCliente.txtCPCliente.setText("");
        vistaCliente.txtPortalCliente.setText("");
        vistaCliente.txtCalleCliente.setText("");
        vistaCliente.cbUsuarioCliente.setSelectedIndex(-1);
        vistaCliente.fechaCliente.setText("");
    }
    /**
     * Método para vaciar los campos del formulario de creación de un producto
     * @param productDialog la ventana dónde va a funcionar el método
     */
    public void borrarCamposProducto(ProductDialog productDialog) {
        productDialog.txtCodigoProducto.setText("");
        productDialog.txtNombreProducto.setText("");
        productDialog.cbCategoriaProducto.setSelectedIndex(-1);
        productDialog.txtPrecioProducto.setText("");
        productDialog.txtCantidadProducto.setText("");
        productDialog.txtMedidaProducto.setText("");
        productDialog.txtTipoProducto.setText("");
        productDialog.cbUsosProducto.setSelectedIndex(-1);
    }
    /**
     * Método para vaciar los campos del formulario de creación de un usuario
     * @param registerDialog la ventana dónde va a funcionar el método
     */
    public void borrarCamposUsuario(RegisterDialog registerDialog) {
        registerDialog.txtNombreRegister.setText("");
        registerDialog.txtApellidoRegister.setText("");
        registerDialog.cbTipoRegister.setSelectedIndex(-1);
        registerDialog.dpCumpleRegister.setText("");
        registerDialog.txtDniRegister.setText("");
        registerDialog.txtPsswdRegister.setText("");
        registerDialog.txtConfirmarRegister.setText("");
        registerDialog.txtTelefonoRegister.setText("");
        registerDialog.txtAltaRegister.setText("");

    }
    /**
     * Método para vaciar los campos del formulario de creación de una categoría
     * @param categoryDialog la ventana dónde va a funcionar el método
     */
    public void borrarCamposCategoria(CategoryDialog categoryDialog) {
        categoryDialog.txtCodigoCategoria.setText("");
        categoryDialog.txtNombreCategoria.setText("");
        categoryDialog.txtDescripcionCategoria.setText("");
    }
    /**
     * Método para vaciar los campos del formulario de modificación de un producto
     * @param modifProductDialog
     */
    public void borrarCamposProductoModif(ModifProductDialog modifProductDialog) {
        modifProductDialog.txtCodigoProductoModif.setText("");
        modifProductDialog.txtNombreProductoModif.setText("");
        modifProductDialog.cbCatProductoModif.setSelectedIndex(-1);
        modifProductDialog.txtPrecioProductoModif.setText("");
        modifProductDialog.txtCantidadProductoModif.setText("");
        modifProductDialog.txtMedidaProductoModif.setText("");
        modifProductDialog.txtTipoProductoModif.setText("");
        modifProductDialog.cbUsosProductoModif.setSelectedIndex(-1);
    }
    /**
     * Método para vaciar los campos del formulario de modificación de un usuario
     * @param modifUserDialog la ventana dónde va a funcionar el método
     */
    public void borrarCamposUsuarioModif(ModifUserDialog modifUserDialog) {
        modifUserDialog.txtNombreUser.setText("");
        modifUserDialog.txtApellidoUser.setText("");
        modifUserDialog.cbTipoUser.setSelectedIndex(-1);
        modifUserDialog.dpCumpleUser.setText("");
        modifUserDialog.txtDniUser.setText("");
        modifUserDialog.txtPsswdUser.setText("");
        modifUserDialog.txtTelefonoUser.setText("");
        modifUserDialog.txtAltaUser.setText("");

    }
    /**
     * Método para vaciar los campos del formulario de modificación de una categoria
     * @param modifCategoryDialog la ventana dónde va a funcionar el método
     */
    public void borrarCamposCategoriaModif(ModifCategoryDialog modifCategoryDialog) {
        modifCategoryDialog.txtCodigoModif.setText("");
        modifCategoryDialog.txtNombreModif.setText("");
        modifCategoryDialog.txtDescripcionModif.setText("");

    }

    /**
     * Método para construir la tabla Categoría
     * @param rs resultset que contiene los datos para cargar las filas de la tabla
     * @param vistaAdmin la ventana dónde va a funcionar el método
     * @return la tabla de las categorías con las filas cargadas
     * @throws SQLException
     */
    public DefaultTableModel constrDTMCategoria(ResultSet rs, VistaAdmin vistaAdmin) throws SQLException {

        ResultSetMetaData metaData = rs.getMetaData();

        //Nombre de las columnas
        Vector<String> columnNames = new Vector<>();
        //Contamos el número de columnas y lo guardamos en un int
        int columnCount = metaData.getColumnCount();
        //Bucle para añadir el nombre de las columnas
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }
        //Datos de la tabla
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        //Asignamos las columnas y los datos a la tabla
        vistaAdmin.dtmCategoriaAdmin.setDataVector(data, columnNames);
        //Devuelve la tabla rellenada
        return vistaAdmin.dtmCategoriaAdmin;
    }
    /**
     * Método para construir la tabla Producto
     * @param rs resultset que contiene los datos para cargar las filas de la tabla
     * @param vistaAdmin la ventana dónde va a funcionar el método
     * @return la tabla de los productos con las filas cargadas
     * @throws SQLException
     */
    public DefaultTableModel constrDTMProducto(ResultSet rs, VistaAdmin vistaAdmin) throws SQLException {

        ResultSetMetaData metaData = rs.getMetaData();

        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        vistaAdmin.dtmProductoAdmin.setDataVector(data, columnNames);
        return vistaAdmin.dtmProductoAdmin;
    }
    /**
     * Método para construir la tabla Usuario
     * @param rs resultset que contiene los datos para cargar las filas de la tabla
     * @param vistaAdmin la ventana dónde va a funcionar el método
     * @return la tabla de usuarios con las filas cargadas
     * @throws SQLException
     */
    public DefaultTableModel constrDTMUsuario(ResultSet rs, VistaAdmin vistaAdmin) throws SQLException {

        ResultSetMetaData metaData = rs.getMetaData();

        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        vistaAdmin.dtmUsuarioAdmin.setDataVector(data, columnNames);
        return vistaAdmin.dtmUsuarioAdmin;
    }
    /**
     * Método para construir la tabla de productos en el listado
     * @param rs resultset que contiene los datos para cargar las filas de la tabla
     * @param vistaAdmin la ventana dónde va a funcionar el método
     * @return la tabla del listado con las filas cargadas
     * @throws SQLException
     */
    public DefaultTableModel constrDTMLista(ResultSet rs, VistaAdmin vistaAdmin) throws SQLException {

        ResultSetMetaData metaData = rs.getMetaData();

        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        vistaAdmin.dtmListaAdmin.setDataVector(data, columnNames);
        return vistaAdmin.dtmListaAdmin;
    }
    /**
     * Método para construir la tabla Categoría
     * @param rs resultset que contiene los datos para cargar las filas de la tabla
     * @param vistaConsultor la ventana dónde va a funcionar el método
     * @return la tabla de categorías con las filas cargadas
     * @throws SQLException
     */
    public DefaultTableModel constrDTMCategoriaConsultor(ResultSet rs, VistaConsultor vistaConsultor) throws SQLException {

        ResultSetMetaData metaData = rs.getMetaData();

        //Nombre de las columnas
        Vector<String> columnNames = new Vector<>();
        //Contamos el número de columnas y lo guardamos en un int
        int columnCount = metaData.getColumnCount();
        //Bucle para añadir el nombre de las columnas
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }
        //Datos de la tabla
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        //Asignamos las columnas y los datos a la tabla
        vistaConsultor.dtmListaCategorias.setDataVector(data, columnNames);
        //Devuelve la tabla rellenada
        return vistaConsultor.dtmListaCategorias;
    }
    /**
     * Método para construir la tabla Producto
     * @param rs resultset que contiene los datos para cargar las filas de la tabla
     * @param vistaConsultor la ventana dónde va a funcionar el método
     * @return la tabla de productos con las filas cargadas
     * @throws SQLException
     */
    public DefaultTableModel constrDTMProductoConsultor(ResultSet rs, VistaConsultor vistaConsultor) throws SQLException {

        ResultSetMetaData metaData = rs.getMetaData();

        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        vistaConsultor.dtmListaProductos.setDataVector(data, columnNames);
        return vistaConsultor.dtmListaProductos;
    }
    /**
     * Método para construir la tabla Usuario
     * @param rs resultset que contiene los datos para cargar las filas de la tabla
     * @param vistaConsultor la ventana dónde va a funcionar el método
     * @return la tabla de usuarios con las filas cargadas
     * @throws SQLException
     */
    public DefaultTableModel constrDTMUsuarioConsultor(ResultSet rs, VistaConsultor vistaConsultor) throws SQLException {

        ResultSetMetaData metaData = rs.getMetaData();

        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        vistaConsultor.dtmListaUsuarios.setDataVector(data, columnNames);
        return vistaConsultor.dtmListaUsuarios;
    }
    /**
     * Método para construir la tabla Pedido
     * @param rs resultset que contiene los datos para cargar las filas de la tabla
     * @param vistaCliente la ventana dónde va a funcionar el método
     * @return la tabla de usuarios con las filas cargadas
     * @throws SQLException
     */
    public DefaultTableModel constrDTMPedidoCliente(ResultSet rs, VistaCliente vistaCliente) throws SQLException {

        ResultSetMetaData metaData = rs.getMetaData();

        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        vistaCliente.dtmPedidoCliente.setDataVector(data, columnNames);
        return vistaCliente.dtmPedidoCliente;
    }
    /**
     * Método para construir la tabla de productos oara ek cliente
     * @param rs resultset que contiene los datos para cargar las filas de la tabla
     * @param vistaCliente la ventana dónde va a funcionar el método
     * @return la tabla de los productos con las filas cargadas
     * @throws SQLException
     */
    public DefaultTableModel constrDTMInfoCliente(ResultSet rs, VistaCliente vistaCliente) throws SQLException {

        ResultSetMetaData metaData = rs.getMetaData();

        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        vistaCliente.dtmInfoCliente.setDataVector(data, columnNames);
        return vistaCliente.dtmInfoCliente;
    }

    /**
     * Método para actualizar la información del pedido cuando insertamos un
     * nuevo registro
     * @param vistaCliente la ventana dónde va a funcionar el método
     * @param modelo clase de la que llamamos al método para construir la tabla de usuarios
     */
    public void refrescarPedido(VistaCliente vistaCliente, Modelo modelo) {
        try {
            vistaCliente.tablaPedidos.setModel(constrDTMPedidoCliente(modelo.consultarPedido(), vistaCliente));
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
    /**
     * Método para actualizar la información del pedido cuando insertamos un
     * nuevo registro
     * @param vistaCliente la ventana dónde va a funcionar el método
     * @param modelo clase de la que llamamos al método para construir la tabla de usuarios
     */
    public void refrescarInfo(VistaCliente vistaCliente, Modelo modelo) {
        try {
            vistaCliente.tablaInfo.setModel(constrDTMInfoCliente(modelo.consultarInfoPedido(), vistaCliente));
            vistaCliente.cbInfoCliente.removeAllItems();
            for(int i = 0; i < vistaCliente.dtmInfoCliente.getRowCount(); i++) {
                vistaCliente.cbInfoCliente.addItem(vistaCliente.dtmInfoCliente.getValueAt(i, 0)+" - "+
                        vistaCliente.dtmInfoCliente.getValueAt(i, 1)+", "+vistaCliente.dtmInfoCliente.getValueAt(i, 2));
            }
            vistaCliente.cbInfoCliente.setSelectedIndex(-1);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
    /**
     * Método para actualizar la tabla Categoria y los combobox cuando insertamos un
     * nuevo registro, lo eliminamos o lo modificamos
     * @param vistaAdmin la ventana dónde va a funcionar el método
     * @param vistaConsultor la ventana dónde va a funcionar el método
     * @param productDialog la ventana dónde va a funcionar el método
     * @param modifProductDialog la ventana dónde va a funcionar el método
     * @param modelo clase de la que llamamos al método para construir la tabla de categorías
     */
    public void refrescarCategoria(VistaAdmin vistaAdmin, VistaConsultor vistaConsultor, ProductDialog productDialog,
                                    ModifProductDialog modifProductDialog, Modelo modelo) {
        try {
            //Construimos la tabla con todos los datos
            vistaAdmin.tableCategoriasAdmin.setModel(constrDTMCategoria(modelo.consultarCategorias(), vistaAdmin));
            //Limpiamos el combobox con todos los registros que tenía cargados
            productDialog.cbCategoriaProducto.removeAllItems();
            //Bucle para contar el número de registros que tiene la tabla para cargar
            //los registros en el combobox
            for(int i = 0; i < vistaAdmin.dtmCategoriaAdmin.getRowCount(); i++) {
                productDialog.cbCategoriaProducto.addItem(vistaAdmin.dtmCategoriaAdmin.getValueAt(i, 0)+" - "+
                        vistaAdmin.dtmCategoriaAdmin.getValueAt(i, 2));
            }
            productDialog.cbCategoriaProducto.setSelectedIndex(-1);


            modifProductDialog.cbCatProductoModif.removeAllItems();
            for(int i = 0; i < vistaAdmin.dtmCategoriaAdmin.getRowCount(); i++) {
                modifProductDialog.cbCatProductoModif.addItem(vistaAdmin.dtmCategoriaAdmin.getValueAt(i, 0)+" - "+
                        vistaAdmin.dtmCategoriaAdmin.getValueAt(i, 2));
            }
            modifProductDialog.cbCatProductoModif.setSelectedIndex(-1);


            vistaAdmin.cbCategoriaListaAdmin.removeAllItems();
            for(int i = 0; i < vistaAdmin.dtmCategoriaAdmin.getRowCount(); i++) {
                vistaAdmin.cbCategoriaListaAdmin.addItem(vistaAdmin.dtmCategoriaAdmin.getValueAt(i, 0)+" - "+
                        vistaAdmin.dtmCategoriaAdmin.getValueAt(i, 2));
            }
            vistaAdmin.cbCategoriaListaAdmin.setSelectedIndex(-1);

            vistaAdmin.cbBuscarCategoria.removeAllItems();
            for(int i = 0; i < vistaAdmin.dtmCategoriaAdmin.getRowCount(); i++) {
                vistaAdmin.cbBuscarCategoria.addItem(vistaAdmin.dtmCategoriaAdmin.getValueAt(i, 0)+" - "+
                        vistaAdmin.dtmCategoriaAdmin.getValueAt(i, 2));
            }
            vistaAdmin.cbBuscarCategoria.setSelectedIndex(-1);

            vistaConsultor.tablaCategorias.setModel(constrDTMCategoriaConsultor(modelo.consultarCategorias(), vistaConsultor));
            vistaConsultor.cbCategoriaProducto.removeAllItems();
            for(int i = 0; i < vistaConsultor.dtmListaCategorias.getRowCount(); i++) {
                vistaConsultor.cbCategoriaProducto.addItem(vistaConsultor.dtmListaCategorias.getValueAt(i, 0)+" - "+
                        vistaConsultor.dtmListaCategorias.getValueAt(i, 2));
            }
            vistaConsultor.cbCategoriaProducto.setSelectedIndex(-1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    /**
     * Método para actualizar la tabla Producto cuando insertamos un
     * nuevo registro, lo eliminamos o lo modificamos
     * @param vistaAdmin la ventana dónde va a funcionar el método
     * @param vistaConsultor la ventana dónde va a funcionar el método
     * @param vistaCliente la ventana dónde va a funcionar el método
     * @param modelo clase de la que llamamos al método para construir la tabla de productos
     */
    public void refrescarProducto(VistaAdmin vistaAdmin, VistaConsultor vistaConsultor,
                                  VistaCliente vistaCliente, Modelo modelo) {
        try {
            vistaAdmin.tableProductosAdmin.setModel(constrDTMProducto(modelo.consultarProductos(), vistaAdmin));
            vistaConsultor.tablaProductos.setModel(constrDTMProductoConsultor(modelo.consultarProductos(), vistaConsultor));

            vistaCliente.cbProductoCliente.removeAllItems();
            for(int i = 0; i < vistaAdmin.dtmProductoAdmin.getRowCount(); i++) {
                vistaCliente.cbProductoCliente.addItem(vistaAdmin.dtmProductoAdmin.getValueAt(i, 0)+" - "+
                        vistaAdmin.dtmProductoAdmin.getValueAt(i, 2));
            }
            vistaCliente.cbProductoCliente.setSelectedIndex(-1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    /**
     * Método para actualizar la tabla Usuario cuando insertamos un
     * nuevo registro, lo eliminamos o lo modificamos
     * @param vistaAdmin la ventana dónde va a funcionar el método
     * @param vistaConsultor la ventana dónde va a funcionar el método
     * @param vistaCliente la ventana dónde va a funcionar el método
     * @param modelo clase de la que llamamos al método para construir la tabla de usuarios
     */
    public void refrescarUsuario(VistaAdmin vistaAdmin, VistaConsultor vistaConsultor,
                                 VistaCliente vistaCliente, Modelo modelo) {
        try {
            vistaAdmin.tableUsuarioAdmin.setModel(constrDTMUsuario(modelo.consultarUsuarios(), vistaAdmin));
            vistaConsultor.tablaUsuarios.setModel(constrDTMUsuarioConsultor(modelo.consultarUsuarios(), vistaConsultor));

            vistaCliente.cbUsuarioCliente.removeAllItems();
            for(int i = 0; i < vistaAdmin.dtmUsuarioAdmin.getRowCount(); i++) {
                vistaCliente.cbUsuarioCliente.addItem(vistaAdmin.dtmUsuarioAdmin.getValueAt(i, 0)+" - "+
                        vistaAdmin.dtmUsuarioAdmin.getValueAt(i, 5));
            }
            vistaCliente.cbUsuarioCliente.setSelectedIndex(-1);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    /**
     * Método para actualizar la tabla Lista cuando insertamos un
     * nuevo registro, lo eliminamos o lo modificamos
     * @param vistaAdmin la ventana dónde va a funcionar el método
     * @param modelo clase de la que llamamos al método para construir la tabla del listado
     */
    public void refrescarLista(VistaAdmin vistaAdmin, Modelo modelo) {
        try {
            vistaAdmin.tableListaAdmin.setModel(constrDTMLista(modelo.consultarProductos(), vistaAdmin));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Método mediante el cuál guardamos la posición de las columnas de la tabla Producto
     * para mostrar los datos en su posición en la pestaña de la lista.
     * @param resultSet resultset que nos devuelve las filas cargadas de la tabla del listado
     * @param vistaAdmin la ventana dónde va a funcionar el método
     * @throws SQLException
     */
    public void cargarFilasListadoAdmin(ResultSet resultSet, VistaAdmin vistaAdmin) throws SQLException {
        Object[] fila=new Object[9];
        vistaAdmin.dtmListaAdmin.setRowCount(0);

        while (resultSet.next()){
            fila[0]=resultSet.getObject(1);
            fila[1]=resultSet.getObject(2);
            fila[2]=resultSet.getObject(3);
            fila[3]=resultSet.getObject(4);
            fila[4]=resultSet.getObject(5);
            fila[5]=resultSet.getObject(6);
            fila[6]=resultSet.getObject(7);
            fila[7]=resultSet.getObject(8);
            fila[8]=resultSet.getObject(9);

            vistaAdmin.dtmListaAdmin.addRow(fila);
        }
    }
    /**
     * Método mediante el cuál guardamos la posición de las columnas de la tabla Producto
     * para mostrar los datos en su posición.
     * @param resultSet resultset que nos devuelve las filas cargadas de la tabla de los productos
     * @param vistaAdmin la ventana dónde va a funcionar el método
     * @throws SQLException
     */
    public void cargarFilasProductsAdmin(ResultSet resultSet, VistaAdmin vistaAdmin) throws SQLException {
        Object[] fila=new Object[9];
        vistaAdmin.dtmProductoAdmin.setRowCount(0);

        while (resultSet.next()){
            fila[0]=resultSet.getObject(1);
            fila[1]=resultSet.getObject(2);
            fila[2]=resultSet.getObject(3);
            fila[3]=resultSet.getObject(4);
            fila[4]=resultSet.getObject(5);
            fila[5]=resultSet.getObject(6);
            fila[6]=resultSet.getObject(7);
            fila[7]=resultSet.getObject(8);
            fila[8]=resultSet.getObject(9);

            vistaAdmin.dtmProductoAdmin.addRow(fila);
        }
        if(resultSet.last()) {
            vistaAdmin.lblProdAdmin.setVisible(true);
            vistaAdmin.lblProdAdmin.setText(resultSet.getRow()+" registros cargados");
        }
    }
    /**
     * Método mediante el cuál guardamos la posición de las columnas de la tabla Usuario
     * para mostrar los datos en su posición.
     * @param resultSet resultset que nos devuelve las filas cargadas de la tabla de los usuarios
     * @param vistaAdmin la ventana dónde va a funcionar el método
     * @throws SQLException
     */
    public void cargarFilasUsuariosAdmin(ResultSet resultSet, VistaAdmin vistaAdmin) throws SQLException {
        Object[] fila=new Object[9];
        vistaAdmin.dtmUsuarioAdmin.setRowCount(0);

        while (resultSet.next()){
            fila[0]=resultSet.getObject(1);
            fila[1]=resultSet.getObject(2);
            fila[2]=resultSet.getObject(3);
            fila[3]=resultSet.getObject(4);
            fila[4]=resultSet.getObject(5);
            fila[5]=resultSet.getObject(6);
            fila[6]=resultSet.getObject(7);
            fila[7]=resultSet.getObject(8);
            fila[8]=resultSet.getObject(9);

            vistaAdmin.dtmUsuarioAdmin.addRow(fila);
        }
        if(resultSet.last()) {
            vistaAdmin.lblUserAdmin.setVisible(true);
            vistaAdmin.lblUserAdmin.setText(resultSet.getRow()+" registros cargados");
        }
    }
    /**
     * Método mediante el cuál guardamos la posición de las columnas de la tabla Categoría
     * para mostrar los datos en su posición.
     * @param resultSet resultset que nos devuelve las filas cargadas de la tabla de las categorías
     * @param vistaAdmin la ventana dónde va a funcionar el método
     * @throws SQLException
     */
    public void cargarFilasCategoriaAdmin(ResultSet resultSet, VistaAdmin vistaAdmin) throws SQLException {
        Object[] fila=new Object[4];
        vistaAdmin.dtmProductoAdmin.setRowCount(0);

        while (resultSet.next()){
            fila[0]=resultSet.getObject(1);
            fila[1]=resultSet.getObject(2);
            fila[2]=resultSet.getObject(3);
            fila[3]=resultSet.getObject(4);

            vistaAdmin.dtmCategoriaAdmin.addRow(fila);
        }
        if(resultSet.last()) {
            vistaAdmin.lblCatAdmin.setVisible(true);
            vistaAdmin.lblCatAdmin.setText(resultSet.getRow()+" registros cargados");
        }
    }
    /**
     * Método mediante el cuál guardamos la posición de las columnas de la tabla Producto
     * para mostrar los datos en su posición.
     * @param resultSet resultset que nos devuelve las filas cargadas de la tabla de los productos
     * @param vistaConsultor la ventana dónde va a funcionar el método
     * @throws SQLException
     */
    public void cargarFilasProductsConsultor(ResultSet resultSet, VistaConsultor vistaConsultor) throws SQLException {
        Object[] fila=new Object[9];
        vistaConsultor.dtmListaProductos.setRowCount(0);

        while (resultSet.next()){
            fila[0]=resultSet.getObject(1);
            fila[1]=resultSet.getObject(2);
            fila[2]=resultSet.getObject(3);
            fila[3]=resultSet.getObject(4);
            fila[4]=resultSet.getObject(5);
            fila[5]=resultSet.getObject(6);
            fila[6]=resultSet.getObject(7);
            fila[7]=resultSet.getObject(8);
            fila[8]=resultSet.getObject(9);

            vistaConsultor.dtmListaProductos.addRow(fila);
        }
        if(resultSet.last()) {
            vistaConsultor.lblAccionProductoC.setVisible(true);
            vistaConsultor.lblAccionProductoC.setText(resultSet.getRow()+" registros cargados");
        }
    }
    /**
     * Método mediante el cuál guardamos la posición de las columnas de la tabla Usuario
     * para mostrar los datos en su posición.
     * @param resultSet resultset que nos devuelve las filas cargadas de la tabla de los usuarios
     * @param vistaConsultor la ventana dónde va a funcionar el método
     * @throws SQLException
     */
    public void cargarFilasUsuariosConsultor(ResultSet resultSet, VistaConsultor vistaConsultor) throws SQLException {
        Object[] fila=new Object[9];
        vistaConsultor.dtmListaUsuarios.setRowCount(0);

        while (resultSet.next()){
            fila[0]=resultSet.getObject(1);
            fila[1]=resultSet.getObject(2);
            fila[2]=resultSet.getObject(3);
            fila[3]=resultSet.getObject(4);
            fila[4]=resultSet.getObject(5);
            fila[5]=resultSet.getObject(6);
            fila[6]=resultSet.getObject(7);
            fila[7]=resultSet.getObject(8);
            fila[8]=resultSet.getObject(9);

            vistaConsultor.dtmListaUsuarios.addRow(fila);
        }
        if(resultSet.last()) {
            vistaConsultor.lblAccionUsuarioC.setVisible(true);
            vistaConsultor.lblAccionUsuarioC.setText(resultSet.getRow()+" registros cargados");
        }
    }
    /**
     * Método mediante el cuál guardamos la posición de las columnas de la tabla Categoría
     * para mostrar los datos en su posición.
     * @param resultSet resultset que nos devuelve las filas cargadas de la tabla de las categorías
     * @param vistaConsultor la ventana dónde va a funcionar el método
     * @throws SQLException
     */
    public void cargarFilasCategoriaConsultor(ResultSet resultSet, VistaConsultor vistaConsultor) throws SQLException {
        Object[] fila=new Object[4];
        vistaConsultor.dtmListaCategorias.setRowCount(0);

        while (resultSet.next()){
            fila[0]=resultSet.getObject(1);
            fila[1]=resultSet.getObject(2);
            fila[2]=resultSet.getObject(3);
            fila[3]=resultSet.getObject(4);

            vistaConsultor.dtmListaCategorias.addRow(fila);
        }
        if(resultSet.last()) {
            vistaConsultor.lblAccionCategoriaC.setVisible(true);
            vistaConsultor.lblAccionCategoriaC.setText(resultSet.getRow()+" registros cargados");
        }
    }

    /**
     * Método para cargar las columnas
     * @param rs resultset para mostrar las columnas de la tabla
     * @param columnCount el número de columnas que tiene la tabla
     * @param data la información de la tabla
     * @throws SQLException
     */
    private void setDataVector(ResultSet rs, int columnCount, Vector<Vector<Object>> data) throws SQLException {
        while (rs.next()) {
            Vector<Object> vector = new Vector<>();
            for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
                vector.add(rs.getObject(columnIndex));
            }
            data.add(vector);
        }
    }

}
