package com.alvarodiez.gui;

import com.alvarodiez.enums.TipoUsuario;
import com.alvarodiez.enums.UsosProducto;
import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;
import java.awt.*;

/**
 * Clase de la vista para modificar un usuario
 */
public class ModifUserDialog extends JFrame{
    JPanel panel1;

    //JTextField
    JTextField txtNombreUser;
    JTextField txtApellidoUser;
    JTextField txtDniUser;
    JTextField txtPsswdUser;
    JTextField txtTelefonoUser;

    //JComboBox
    JComboBox cbTipoUser;

    //JButton
    JButton btnModificarUser;
    JButton btnAtrasModifUser;
    JButton btnHoyUser;

    //JLabel
    JLabel txtAltaUser;

    //DatePicker
    DatePicker dpCumpleUser;

    /**
     * Constructor de la clase ModifUserDialog dónde
     * llamamos al método startJFrame() para configurar la ventana
     */
    public ModifUserDialog() {
        startJFrame();
    }

    /**
     * Método en el que establecemos la configuración de la ventana
     * como por ejemplo el tamaño, la posición, el icono...
     * Además, llamamos a los métodos para configurar los combobox.
     */
    private void startJFrame(){
        this.setTitle("Albaron");
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.pack();
        this.setVisible(false);
        this.setSize(new Dimension(this.getWidth(), this.getHeight()));
        this.setLocationRelativeTo(null);
        Image ico=Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("icono.png"));
        this.setIconImage(ico);
        setEnumComboBox();
    }

    /**
     * Setea el combobox en el que cargamos los objetos de una clase enumerada
     */
    private void setEnumComboBox() {
        for (TipoUsuario constant : TipoUsuario.values()) {
            cbTipoUser.addItem(constant.getValor());
        }
        cbTipoUser.setSelectedIndex(-1);
    }

}
