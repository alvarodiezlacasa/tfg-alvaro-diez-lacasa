package com.alvarodiez.gui;

import com.alvarodiez.enums.TipoUsuario;


import javax.swing.*;
import java.awt.*;
import java.sql.SQLException;

/**
 * Clase de la vista del login la cual es la primera ventana que se abre en nuestro programa.
 * Es la ventana para iniciar sesión o registrarse
 */
public class LoginDialog extends JFrame{
    private JPanel panel1;

    //JTextField
    JTextField txtDNILogin;
    JPasswordField txtPsswdLogin;

    //JButton
    JButton btnValidarLogin;
    JButton btnRegistrarLogin;
    JButton btnVaciarLogin;

    //Combobox
    JComboBox cbTipoUsuario;

    //JMenuBar
    JMenuItem itemRegistrar;
    JMenuItem itemSalir;
    JMenuItem itemAcercaDe;

    //JLabel
    JLabel lblLogin;

    /**
     * Constructor de la clase LoginDialog dónde
     * llamamos al método startJFrame() para configurar la ventana
     */
    public LoginDialog() { startJFrame(); }

    /**
     * Método en el que establecemos la configuración de la ventana
     * como por ejemplo el tamaño, la posición, el icono...
     * Además, llamamos a los métodos para configurar los combobox y la barra de menú
     */
    private void startJFrame(){
        this.setTitle("Albaron");
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(true);
        this.setSize(500, 285);
        this.setLocationRelativeTo(null);
        Image ico=Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("icono.png"));
        this.setIconImage(ico);

        setMenu();
        setEnumComboBox();


    }

    /**
     * Setea el combobox en el que cargamos los objetos de una clase enumerada
     */
    private void setEnumComboBox() {
        for (TipoUsuario constant : TipoUsuario.values()) {
            cbTipoUsuario.addItem(constant.getValor());
        }
        cbTipoUsuario.setSelectedIndex(-1);
    }

    /**
     * Setea una barra de menus
     */
    private void setMenu(){
        JMenuBar mbBar = new JMenuBar();
        JMenu menu = new JMenu("Ajustes");
        itemRegistrar = new JMenuItem("Registrarme");
        itemRegistrar.setActionCommand("Registrarme");
        itemAcercaDe = new JMenuItem("Acerca de");
        itemAcercaDe.setActionCommand("Acerca de");
        itemSalir = new JMenuItem("Salir");
        itemSalir.setActionCommand("Salir");
        menu.add(itemAcercaDe);
        menu.add(itemRegistrar);
        menu.add(itemSalir);
        mbBar.add(menu);
        mbBar.add(Box.createHorizontalGlue());
        this.setJMenuBar(mbBar);
    }

}
