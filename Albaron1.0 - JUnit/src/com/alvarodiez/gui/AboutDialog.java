package com.alvarodiez.gui;

import javax.swing.*;
import java.awt.*;

/**
 * Clase de la vista donde se muestra la información de la aplicación
 */
public class AboutDialog extends JFrame{
    private JPanel panel1;

    /**
     * Constructor de la clase VistaAdmin dónde
     * llamamos al método startJFrame() para configurar la ventana
     */
    public AboutDialog() {
        startJFrame();
    }

    /**
     * Método en el que establecemos la configuración de la ventana
     * como por ejemplo el tamaño, la posición, el icono...
     */
    private void startJFrame() {
        setTitle("Albaron");
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.pack();
        this.setVisible(false);
        this.setEnabled(true);
        this.setSize(new Dimension(this.getWidth(), this.getHeight()));
        this.setLocationRelativeTo(null);
        Image ico=Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("icono.png"));
        this.setIconImage(ico);

    }

}
