package com.alvarodiez.gui;

import com.alvarodiez.enums.TipoUsuario;
import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;
import java.awt.*;
import java.sql.SQLException;

/**
 * Clase de la vista para registrar un nuevo usuario
 */
public class RegisterDialog extends JFrame{
    private JPanel panel1;

    //JTextField
    JTextField txtNombreRegister;
    JTextField txtApellidoRegister;
    JTextField txtTelefonoRegister;
    JTextField txtDniRegister;

    //JComboBox
    JComboBox cbTipoRegister;

    //JPasswordField
    JPasswordField txtPsswdRegister;
    JPasswordField txtConfirmarRegister;

    //JButton
    JButton btnAltaRegister;
    JButton btnCrearUsuario;
    JButton btnAtrasRegister;

    //DatePicker
    DatePicker dpCumpleRegister;

    //JLabel
    JLabel txtAltaRegister;

    /**
     * Constructor de la clase RegisterDialog dónde
     * llamamos al método startJFrame() para configurar la ventana
     */
    public RegisterDialog() {
        startJFrame();
    }

    /**
     * Método en el que establecemos la configuración de la ventana
     * como por ejemplo el tamaño, la posición, el icono...
     * Además, llamamos a los métodos para configurar los combobox.
     */
    private void startJFrame(){
        this.setTitle("Alvaro Diez Lacasa");
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.pack();
        this.setVisible(false);
        this.setSize(this.getWidth(), this.getHeight());
        this.setLocationRelativeTo(null);
        Image ico=Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("icono.png"));
        this.setIconImage(ico);

        setEnumComboBox();
    }

    /**
     * Setea el combobox en el que cargamos los objetos de una clase enumerada
     */
    private void setEnumComboBox() {
        for (TipoUsuario constant : TipoUsuario.values()) {
            cbTipoRegister.addItem(constant.getValor());
        }
        cbTipoRegister.setSelectedIndex(-1);
    }


}
