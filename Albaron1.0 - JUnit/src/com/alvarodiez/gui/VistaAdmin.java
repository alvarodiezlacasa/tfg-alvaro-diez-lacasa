package com.alvarodiez.gui;

import com.alvarodiez.enums.UsosProducto;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

/**
 * Clase de la vista del administrador
 */
public class VistaAdmin extends JFrame{
    JTabbedPane tabbedPane1;
    JPanel panel1;
    JPanel panelLista;
    JPanel panelProductos;
    JPanel panelUsuarios;
    JPanel panelAjustes;
    JPanel panelCategorias;

    //Productos
    JButton btnBuscarProductosAdmin;
    JButton btnInsertarProductoAdmin;
    JButton btnEliminarProductoAdmin;
    JButton btnModificarProductosAdmin;
    JButton btnMostrarTodoProd;
    JTextField txtBuscarProductosAdmin;
    JTable tableProductosAdmin;
    JLabel lblProdAdmin;
    JLabel lblProducto;
    JLabel lblIconoProducto;

    //Lista
    JButton btnMostrarSinFiltro;
    JButton btnPrecioAltoListaAdmin;
    JButton btnNombreListaAdmin;
    JButton btnCategoriaListaAdmin;
    JButton btnUsosListaAdmin;
    JButton btnTipoListaAdmin;
    JButton btnPrecioBajoListaAdmin;
    JButton btnAOrdenarListaAZ;
    JButton btnAOrdenarListaZA;
    JTextField txtNombreListaAdmin;
    JTextField txtTipoListaAdmin;
    JComboBox cbCategoriaListaAdmin;
    JComboBox cbUsosListaAdmin;
    JScrollPane scrollPaneLista;
    JTable tableListaAdmin;
    JLabel lblFiltrarPor;
    JLabel lblLista;

    //Usuarios
    JButton btnEliminarUserAdmin;
    JButton btnModificarUserAdmin;
    JButton btnBuscarDniAdmin;
    JButton btnMostrarTodoUser;
    JTextField txtBuscarUserAdmin;
    JLabel lblUserAdmin;
    JLabel lblUsuario;
    JLabel lblIconoUsuario;
    JTable tableUsuarioAdmin;

    //Categorías
    JButton btnBuscarCategoriaAdmin;
    JButton btnEliminarCategoriaAdmin;
    JButton btnModificarCategoriaAdmin;
    JButton btnInsertarCategoriaAdmin;
    JButton btnMostrarTodoCat;
    JComboBox cbBuscarCategoria;
    JLabel lblCatAdmin;
    JLabel lblCategoria;
    JLabel lblIconoCategoria;
    JTable tableCategoriasAdmin;

    //Ajustes
    JToggleButton btnModoOscuro;
    JToggleButton btnTipoTabla;
    JLabel lblIconoAjustes;
    JLabel lblAjustes;
    JLabel lblFuenteAjustes;
    JRadioButton btnIcono1;
    JRadioButton btnIcono2;
    JRadioButton btnIcono3;

    //JMenuBar
    JMenuItem itemAcercaDe;
    JMenuItem itemVolver;

    //DTM's
    DefaultTableModel dtmUsuarioAdmin;
    DefaultTableModel dtmListaAdmin;
    DefaultTableModel dtmProductoAdmin;
    DefaultTableModel dtmCategoriaAdmin;


    /**
     * Constructor de la clase VistaAdmin dónde
     * llamamos al método startJFrame() para configurar la ventana
     */
    public VistaAdmin() {
        startJFrame();
    }

    /**
     * Método en el que establecemos la configuración de la ventana
     * como por ejemplo el tamaño, la posición, el icono...
     * Además, llamamos a los métodos para configurar los combobox,
     * las tablas y la barra de menú
     */
    private void startJFrame() {
        setTitle("Albaron");
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(false);
        this.setEnabled(true);
        this.setSize(new Dimension(this.getWidth(), this.getHeight()+20));
        this.setLocationRelativeTo(null);
        Image ico=Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("icono.png"));
        this.setIconImage(ico);
        setDTM();
        setMenu();
        setEnumComboBox();
    }

    /**
     * Método para aplicar un formato a las tablas
     */
    private void setDTM() {
        this.dtmUsuarioAdmin = new DefaultTableModel();
        this.tableUsuarioAdmin.setModel(dtmUsuarioAdmin);

        this.dtmProductoAdmin = new DefaultTableModel();
        this.tableProductosAdmin.setModel(dtmProductoAdmin);

        this.dtmListaAdmin = new DefaultTableModel();
        this.tableListaAdmin.setModel(dtmListaAdmin);

        this.dtmCategoriaAdmin = new DefaultTableModel();
        this.tableCategoriasAdmin.setModel(dtmCategoriaAdmin);
    }

    /**
     * Setea una barra de menus
     */
    private void setMenu(){
        //Declaramos la barra de menu
        JMenuBar barra = new JMenuBar();
        //Declaramos un JMenu
        JMenu menu = new JMenu("Opciones");
        //Creamos un componente para la barra de menú
        itemAcercaDe = new JMenuItem("Acerca de");
        itemVolver = new JMenuItem("Volver");
        //Asignamos un comando para poder darle función
        itemAcercaDe.setActionCommand("Acerca de");
        itemVolver.setActionCommand("VolverLogin");
        //Añadimos los JMenuItems al menú
        menu.add(itemVolver);
        menu.add(itemAcercaDe);
        //Añadimos el menú a la barra
        barra.add(menu);
        barra.add(Box.createHorizontalGlue());
        this.setJMenuBar(barra);
    }

    /**
     * Setea el combobox en el que cargamos los objetos de una clase enumerada
     */
    private void setEnumComboBox() {
        //Recorremos todos los valores de la enumeración
        for (UsosProducto constant : UsosProducto.values()) {
            //Añadimos los valores al combobox
            cbUsosListaAdmin.addItem(constant.getValor());
        }
        cbUsosListaAdmin.setSelectedIndex(-1);
    }

}
