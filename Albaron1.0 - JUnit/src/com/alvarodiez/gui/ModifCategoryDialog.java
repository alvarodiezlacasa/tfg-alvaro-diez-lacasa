package com.alvarodiez.gui;

import com.alvarodiez.enums.UsosProducto;

import javax.swing.*;
import java.awt.*;

/**
 * Clase de la vista para modificar una categoría
 */
public class ModifCategoryDialog extends JFrame{
    JPanel panel1;

    //JTextField
    JTextField txtCodigoModif;
    JTextField txtNombreModif;

    //JTextPane
    JTextPane txtDescripcionModif;

    //JButton
    JButton btnModifCategoria;
    JButton btnAtrasCategoria;

    /**
     * Constructor de la clase ModifCategoryDialog dónde
     * llamamos al método startJFrame() para configurar la ventana
     */
    public ModifCategoryDialog() {
        startJFrame();
    }

    /**
     * Método en el que establecemos la configuración de la ventana
     * como por ejemplo el tamaño, la posición, el icono...
     */
    private void startJFrame(){
        this.setTitle("Albaron");
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.pack();
        this.setVisible(false);
        this.setSize(new Dimension(this.getWidth(), this.getHeight()));
        this.setLocationRelativeTo(null);
        Image ico=Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("icono.png"));
        this.setIconImage(ico);
    }
}
