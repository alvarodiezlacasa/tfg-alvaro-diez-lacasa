package com.alvarodiez.gui;

import javax.swing.*;
import java.awt.*;

/**
 * Clase de la vista para insertar una nueva categoría en la base de datos
 */
public class CategoryDialog extends JFrame {
    JPanel panel1;

    //JTextField
    JTextField txtCodigoCategoria;
    JTextField txtNombreCategoria;

    //JTextArea
    JTextArea txtDescripcionCategoria;

    //JButton
    JButton btnCrearCategoria;
    JButton btnAtrasCategoria;

    private VistaAdmin vistaAdmin;

    /**
     * Constructor de la clase CategoryDialog dónde
     * llamamos al método startJFrame() para configurar la ventana
     */
    public CategoryDialog(){
        startJFrame();
    }

    /**
     * Método en el que establecemos la configuración de la ventana
     * como por ejemplo el tamaño, la posición, el icono...
     */
    private void startJFrame(){

        this.setTitle("Alvaro Diez Lacasa");
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.pack();
        this.setVisible(false);
        this.setSize(425, 290);
        this.setLocationRelativeTo(null);
        Image ico=Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("icono.png"));
        this.setIconImage(ico);

        vistaAdmin=new VistaAdmin();

    }
}
