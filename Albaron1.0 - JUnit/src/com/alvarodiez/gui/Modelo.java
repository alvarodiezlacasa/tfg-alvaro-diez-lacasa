package com.alvarodiez.gui;

import javax.swing.*;
import java.sql.*;

public class Modelo {


    private Connection conexion;


    /**
     * Método mediante el cual creamos una conexión con nuestra base de datos
     *
     * @throws SQLException
     */
    public void conectar() throws SQLException {
        conexion = null;
        conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/bbddtfg", "root", "");
    }

    //----------------------------Métodos Existe----------------------------

    /**
     * Método para controlar el login. Si no tienes una cuenta en la base de datos no podremos acceder al programa.
     * Pasamos como parámetro todas las variables necesarias para comprobar
     * si existe ese usuario
     *
     * @param dni número único para identificar a cada usuario
     * @param psswd conjunto de carácteres privado para acceder a la aplicación
     * @param tipoUser tipo de usuario
     * @return false si no hay coincidencias
     */
    public boolean existeUsuario(String dni, String psswd, String tipoUser) {

        //Instanciamos un objeto para almacenar la consulta SQL
        PreparedStatement sentencia = null;
        try {
            //Guardamos la consulta en un String
            String consulta = "SELECT * FROM usuario where dniUsuario= ? and psswdUsuario= ? and tipoUsuario= ?";
            //Preparamos la consulta
            sentencia = conexion.prepareStatement(consulta);
            //Asignamos los String en las diferentes posiciones de la consulta
            sentencia.setString(1, dni);
            sentencia.setString(2, psswd);
            sentencia.setString(3, tipoUser);
            //Ejecutamos la consulta y guardamos el resultado en un ResultSet
            ResultSet resultado = sentencia.executeQuery();

            if (resultado.next()) {
                return true;
            }
            return false;
        } catch (SQLException a) {
            a.printStackTrace();
        } finally {
            if (sentencia != null) {
                try {
                    sentencia.close();
                } catch (SQLException a) {
                    a.printStackTrace();
                }
            }
        }
        return false;
    }

    /**
     * Método que llama a una función que comprueba la existencia de un DNI
     *
     * @param dni conjunto de valores único de identificación de los usuarios
     * @return true si existe el DNI
     */
    public boolean dniYaExiste(String dni) {

        //Instanciamos un objeto para almacenar la consulta SQL
        PreparedStatement function = null;
        boolean dniExiste = false;
        try {
            //Guardamos la consulta en un String
            String consultaDni = "SELECT existeDni(?)";

            function = conexion.prepareStatement(consultaDni);
            function.setString(1, dni);
            ResultSet rs = function.executeQuery();
            rs.next();

            //Booleano que devuelve false si no existe el DNI
            dniExiste = rs.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return dniExiste;
    }

    /**
     * Método que llama a una función que comprueba la existencia de un DNI
     *
     * @param codigoProducto conjunto de valores único para identificar un producto
     * @return true si existe el DNI
     */
    public boolean productoYaExiste(String codigoProducto) {

        //Instanciamos un objeto para almacenar la consulta SQL
        PreparedStatement function = null;
        boolean codigoExiste = false;
        try {
            //Guardamos la consulta en un String
            String consultaProducto = "SELECT existeProducto(?)";

            function = conexion.prepareStatement(consultaProducto);
            function.setString(1, codigoProducto);
            ResultSet rs = function.executeQuery();
            rs.next();

            //Booleano que devuelve false si no existe el DNI
            codigoExiste = rs.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return codigoExiste;
    }

    /**
     * Método que llama a una función que comprueba la existencia de un DNI
     *
     * @param codigoCategoria conjunto de valores único para identificar una categoría
     * @return true si existe el DNI
     */
    public boolean categoriaYaExiste(String codigoCategoria) {

        //Instanciamos un objeto para almacenar la consulta SQL
        PreparedStatement function = null;
        boolean categoriaExiste = false;
        try {
            //Guardamos la consulta en un String
            String consultaCategoria = "SELECT existeCategoria(?)";

            function = conexion.prepareStatement(consultaCategoria);
            function.setString(1, codigoCategoria);
            ResultSet rs = function.executeQuery();
            rs.next();

            //Booleano que devuelve false si no existe el DNI
            categoriaExiste = rs.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return categoriaExiste;
    }

    //----------------------------Métodos Insertar----------------------------

    /**
     * Método para introducir un nuevo registro a la tabla Producto
     *
     * @param codigoProducto conjunto de valores único para identificar un producto
     * @param nombreProducto nombre del producto
     * @param categoria categoría a la que pertenece el producto
     * @param precioUnitProducto precio unitario del producto
     * @param cantidadProducto cantidad
     * @param unidadMedidaProducto unidad de medida de la cantidad de un producto
     * @param tipoProducto tipo de producto
     * @param usosProducto uso que tiene un producto
     */
    public void insertarProducto(String codigoProducto, String nombreProducto, String categoria,
                                 Double precioUnitProducto, Double cantidadProducto,
                                 String unidadMedidaProducto, String tipoProducto, String usosProducto) {

        //Instanciamos un objeto que almacenar la consulta SQL
        PreparedStatement sentencia = null;
        try {
            //Guardamos la consulta en un String
            String consulta = "INSERT INTO producto (codigoproducto, nombreproducto, idcategoria, preciounitproducto, " +
                    "cantidadproducto, unidadmedidaproducto, tipoproducto, usosproducto) VALUES (?,?,?,?,?,?,?,?)";

            int id_Categoria = Integer.valueOf(categoria.split(" ")[0]);

            sentencia = conexion.prepareStatement(consulta);
            sentencia.setString(1, codigoProducto);
            sentencia.setString(2, nombreProducto);
            sentencia.setInt(3, id_Categoria);
            sentencia.setDouble(4, precioUnitProducto);
            sentencia.setDouble(5, cantidadProducto);
            sentencia.setString(6, unidadMedidaProducto);
            sentencia.setString(7, tipoProducto);
            sentencia.setString(8, usosProducto);
            sentencia.executeUpdate();

        } catch (SQLException a) {
            a.printStackTrace();
        } finally {
            if (sentencia != null) {
                try {
                    sentencia.close();
                } catch (SQLException a) {
                    a.printStackTrace();
                }
            }
        }
    }

    /**
     * Método para introducir un nuevo registro a la tabla Categoria
     *
     * @param codigoCategoria conjunto de valores único para identificar una Categoría
     * @param nombreCategoria nombre de la categoría
     * @param descripcionCategoria breve descripción de una categoría
     */
    public void insertarCategoria(String codigoCategoria, String nombreCategoria, String descripcionCategoria) {

        //Instanciamos un objeto que almacenar la consulta SQL
        PreparedStatement sentencia = null;
        try {
            //Guardamos la consulta en un String
            String consulta = "INSERT INTO categoria(codigoCategoria, nombreCategoria, descripcionCategoria)" +
                    "VALUES (?, ?, ?)";

            sentencia = conexion.prepareStatement(consulta);
            sentencia.setString(1, codigoCategoria);
            sentencia.setString(2, nombreCategoria);
            sentencia.setString(3, descripcionCategoria);
            sentencia.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e);
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
        }
    }

    /**
     * Método para introducir un nuevo registro a la tabla Producto
     *
     * @param nombreUsuario nombre del usuario
     * @param apellidoUsuario apellidos del usuario
     * @param tipoUsuario tipo de usuario
     * @param cumpleUsuario fecha de cumpleaños del usuario
     * @param dniUsuario conjunto de valores único para identificar un usuario
     * @param psswdUsuario contraseña propia para cada usuario
     * @param telefono número de telefono de un usuario
     * @param altaUsuario fecha de alta de un usuario en la aplicación
     */
    public void insertarUsuario(String nombreUsuario, String apellidoUsuario, String tipoUsuario, Date cumpleUsuario,
                                String dniUsuario, String psswdUsuario, String telefono, Date altaUsuario) {
        //Instanciamos un objeto para almacenar la consulta SQL
        PreparedStatement sentencia = null;

        try {
            //Guardamos la consulta en un String
            String consulta = "INSERT INTO usuario (nombreUsuario, apellidoUsuario, tipoUsuario, fechaCumple, dniUsuario," +
                    " psswdUsuario, telefono, fechaAltaUsuario) VALUES (?,?,?,?,?,?,?,?)";

            sentencia = conexion.prepareStatement(consulta);
            sentencia.setString(1, nombreUsuario);
            sentencia.setString(2, apellidoUsuario);
            sentencia.setString(3, tipoUsuario);
            sentencia.setDate(4, cumpleUsuario);
            sentencia.setString(5, dniUsuario);
            sentencia.setString(6, psswdUsuario);
            sentencia.setString(7, telefono);
            sentencia.setDate(8, altaUsuario);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
        }
    }

    public void insertarInfo(String usuario, Date fechaPedido, String direccionPedido) {
        //Instanciamos un objeto para almacenar la consulta SQL
        PreparedStatement sentencia = null;

        try {
            //Guardamos la consulta en un String
            String consulta = "INSERT INTO pedido (idUsuario, fechaPedido, direccionPedido) VALUES (?,?,?)";

            int id_Usuario = Integer.valueOf(usuario.split(" ")[0]);

            sentencia = conexion.prepareStatement(consulta);
            sentencia.setInt(1, id_Usuario);
            sentencia.setDate(2, fechaPedido);
            sentencia.setString(3, direccionPedido);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
        }
    }

    public void insertarPedido(String producto, String info, int cantidad) {
        //Instanciamos un objeto para almacenar la consulta SQL
        PreparedStatement sentencia = null;

        try {
            //Guardamos la consulta en un String
            String consulta = "INSERT INTO detalle_pedido (idProducto, idPedido, cantidadDetalle) VALUES (?,?,?)";

            int id_Producto = Integer.valueOf(producto.split(" ")[0]);
            int id_Pedido = Integer.valueOf(info.split(" ")[0]);

            sentencia = conexion.prepareStatement(consulta);
            sentencia.setInt(1, id_Producto);
            sentencia.setInt(2, id_Pedido);
            sentencia.setInt(3, cantidad);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
        }
    }

    //----------------------------Métodos Eliminar----------------------------


    /**
     * Método para eliminar un registro de la tabla categoria.
     * Cogemos el idCategoria porque es un campo único para
     * todos los registros
     *
     * @param idCategoria valor autoincremental de la base de datos para una categoría
     */
    public void eliminarCategoria(int idCategoria) {

        //Instanciamos un objeto para almacenar la consulta SQL
        PreparedStatement sentencia = null;
        try {
            //Guardamos la consulta en un String
            String consulta = "DELETE FROM categoria where idCategoria = ?";

            sentencia = conexion.prepareStatement(consulta);
            sentencia.setInt(1, idCategoria);
            sentencia.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
        }
    }

    /**
     * Método para eliminar un registro de la tabla producto.
     * Cogemos el idProducto porque es un campo único para
     * todos los registros
     *
     * @param idProducto valor autoincremental de la base de datos para un producto
     */
    public void eliminarProducto(int idProducto) {

        //Instanciamos un objeto que almacenar la consulta SQL
        PreparedStatement sentencia = null;
        try {
            //Guardamos la consulta en un String
            String consulta = "DELETE FROM producto WHERE idProducto = ?";

            sentencia = conexion.prepareStatement(consulta);
            sentencia.setInt(1, idProducto);
            sentencia.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
        }
    }

    /**
     * Método para eliminar un registro de la tabla usuario.
     * Cogemos el idUsuario porque es un campo único para
     * todos los registros
     *
     * @param idUsuario valor autoincremental de la base de datos para un usuario
     */
    public void eliminarUsuario(int idUsuario) {

        //Instanciamos un objeto que almacenar la consulta SQL
        PreparedStatement sentencia = null;
        try {
            //Guardamos la consulta en un String
            String consulta = "DELETE FROM usuario WHERE idUsuario = ?";

            sentencia = conexion.prepareStatement(consulta);
            sentencia.setInt(1, idUsuario);
            sentencia.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
        }

    }

    /**
     * Método para eliminar un registro de la tabla pedido.
     * Cogemos el idDetalle porque es un campo único para
     * todos los registros
     *
     * @param idDetalle valor autoincremental de la base de datos para un usuario
     */
    public void eliminarProductoPedido(int idDetalle) {
        //Instanciamos un objeto que almacenar la consulta SQL
        PreparedStatement sentencia = null;
        try {
            //Guardamos la consulta en un String
            String consulta = "DELETE FROM detalle_pedido WHERE idDetalle = ?";

            sentencia = conexion.prepareStatement(consulta);
            sentencia.setInt(1, idDetalle);
            sentencia.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
        }

    }

    /**
     * Método para eliminar un registro de la tabla de información del pedido.
     * Cogemos el idDetalle porque es un campo único para
     * todos los registros
     *
     * @param idPedido valor autoincremental de la base de datos para un usuario
     */
    public void eliminarInfoPedido(int idPedido) {
        //Instanciamos un objeto que almacenar la consulta SQL
        PreparedStatement sentencia = null;
        try {
            //Guardamos la consulta en un String
            String consulta = "DELETE FROM pedido WHERE idpedido = ?";

            sentencia = conexion.prepareStatement(consulta);
            sentencia.setInt(1, idPedido);
            sentencia.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
        }

    }

    public void vaciarCarrito() {
        //Instanciamos un objeto que almacenar la consulta SQL
        PreparedStatement sentencia = null;
        try {
            //Guardamos la consulta en un String
            String consulta = "DELETE FROM detalle_pedido";

            sentencia = conexion.prepareStatement(consulta);
            sentencia.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
        }
    }


    //----------------------------Métodos Modificar----------------------------


    /**
     * Método para cambiar los valores de la tabla producto
     *
     * @param codigoProducto conjunto de valores único para identificar un producto
     * @param nombreProducto nombre del producto
     * @param idCategoria categoría a la que pertenece el producto
     * @param precioUnitProducto precio unitario del producto
     * @param cantidadProducto cantidad
     * @param unidadMedidaProducto unidad de medida de la cantidad de un producto
     * @param tipoProducto tipo de producto
     * @param usosProducto uso que tiene un producto
     * @param idProducto valor autoincremental de la base de datos para un producto
     */
    public void modificarProducto(String codigoProducto, String nombreProducto, String idCategoria,
                                  Double precioUnitProducto, Double cantidadProducto,
                                  String unidadMedidaProducto, String tipoProducto, String usosProducto, int idProducto) throws SQLException {

        //Instanciamos un objeto para almacenar la consulta SQL
        PreparedStatement sentencia = null;
        try {
            String consulta = "UPDATE producto SET codigoProducto = ?, nombreProducto = ?, " +
                    "idCategoria = ?, precioUnitProducto = ?, cantidadProducto = ?," +
                    "unidadMedidaProducto = ?, tipoProducto = ?, usosProducto = ? WHERE idProducto = ?";

            int id_Categoria = Integer.parseInt(idCategoria.split(" ")[0]);

            sentencia = conexion.prepareStatement(consulta);
            sentencia.setString(1, codigoProducto);
            sentencia.setString(2, nombreProducto);
            sentencia.setInt(3, id_Categoria);
            sentencia.setDouble(4, precioUnitProducto);
            sentencia.setDouble(5, cantidadProducto);
            sentencia.setString(6, unidadMedidaProducto);
            sentencia.setString(7, tipoProducto);
            sentencia.setString(8, usosProducto);
            sentencia.setInt(9, idProducto);
            sentencia.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
        }
    }

    /**
     * Método para cambiar valores de un registro de la tabla Categoria
     *
     * @param codigoCategoria conjunto de valores único para identificar una Categoría
     * @param nombreCategoria nombre de la categoría
     * @param descripcionCategoria breve descripción de una categoría
     * @param idCategoria valor autoincremental de la base de datos para una categoría
     * @throws SQLException
     */
    public void modificarCategoria(String codigoCategoria, String nombreCategoria,
                                   String descripcionCategoria, int idCategoria) throws SQLException {

        //Instanciamos un objeto para almacenar la consulta SQL
        PreparedStatement sentencia = null;
        try {
            String consulta = "UPDATE categoria SET codigoCategoria = ?, nombreCategoria = ?, " +
                    "descripcionCategoria = ? WHERE idCategoria = ?";

            sentencia = conexion.prepareStatement(consulta);
            sentencia.setString(1, codigoCategoria);
            sentencia.setString(2, nombreCategoria);
            sentencia.setString(3, descripcionCategoria);
            sentencia.setInt(4, idCategoria);
            sentencia.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
        }
    }


    /**
     * Método para cambiar valores de la tabla usuario
     *
     * @param nombreUsuario nombre del usuario
     * @param apellidoUsuario apellidos del usuario
     * @param tipoUsuario tipo de usuario
     * @param fechaCumple fecha de cumpleaños del usuario
     * @param dniUsuario conjunto de valores único para identificar un usuario
     * @param psswdUsuario contraseña propia para cada usuario
     * @param telefono número de telefono de un usuario
     * @param fechaAltaUsuario fecha de alta de un usuario en la aplicación
     * @param idUsuario valor autoincremental de la base de datos para un usuario
     * @throws SQLException
     */
    public void modificarUsuario(String nombreUsuario, String apellidoUsuario, String tipoUsuario,
                                 Date fechaCumple, String dniUsuario, String psswdUsuario,
                                 String telefono, Date fechaAltaUsuario, int idUsuario) throws SQLException {

        //Instanciamos un objeto para almacenar la consulta SQL
        PreparedStatement sentencia = null;
        try {
            //Guardamos la consulta en un String
            String consulta = "UPDATE usuario SET nombreUsuario = ?, apellidoUsuario = ?, tipoUsuario = ?," +
                    "fechaCumple = ?, dniUsuario = ?, psswdUsuario = ?, telefono = ?, fechaAltaUsuario = ?" +
                    "WHERE idUsuario = ?";

            sentencia = conexion.prepareStatement(consulta);
            sentencia.setString(1, nombreUsuario);
            sentencia.setString(2, apellidoUsuario);
            sentencia.setString(3, tipoUsuario);
            sentencia.setDate(4, fechaCumple);
            sentencia.setString(5, dniUsuario);
            sentencia.setString(6, psswdUsuario);
            sentencia.setString(7, telefono);
            sentencia.setDate(8, fechaAltaUsuario);
            sentencia.setInt(9, idUsuario);
            sentencia.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
        }
    }

    //-----------------------------Métodos Buscar-----------------------------

    /**
     * Este metodo comprueba si el nombre de un usuario ya existe en la base de datos y muestra
     * los datos del usuario con ese nombre
     * @param nombreUsuario nombre de un usuario
     * @return la información del usuario con el nombre introducido
     * @throws SQLException
     */
    public ResultSet buscarNombreUsuario(String nombreUsuario) throws SQLException {
        if( conexion == null)
            return null;

        if( conexion.isClosed())
            return null;
        //realizamos una consulta en la cual mostramos todos los datos cuando el nombre
        //del usuario sea igual al introducido
        String consulta = "SELECT * FROM usuario WHERE nombreUsuario = ?";
        PreparedStatement sentencia = null;

        sentencia = conexion.prepareStatement(consulta);

        sentencia.setString(1, String.valueOf(nombreUsuario));
        //ejecuta la sentecia y retorna un resultado
        ResultSet resultado = sentencia.executeQuery();

        return resultado;
    }

    /**
     * Este metodo comprueba si el dni de un usuario ya existe en la base de datos y muestra
     * los datos del usuario con ese dni en caso de existir
     * @param dniUsuario conjunto de valores único para identificar a un usuario
     * @return la información de la persona con el dni introducido
     * @throws SQLException
     */
    public ResultSet buscarDni(String dniUsuario) throws SQLException {
        if( conexion == null)
            return null;

        if( conexion.isClosed())
            return null;
        //realizamos una consulta en la cual cogemos mostramos todos los datos cuando el dni
        //del usuario sea igual al introducido
        String consulta = "SELECT * FROM usuario WHERE dniUsuario = ?";
        PreparedStatement sentencia = null;

        sentencia = conexion.prepareStatement(consulta);

        sentencia.setString(1, String.valueOf(dniUsuario));
        //ejecuta la sentecia y retorna un resultado
        ResultSet resultado = sentencia.executeQuery();

        if (resultado.wasNull()){
            String consulta1 = "SELECT * FROM usuario";
            PreparedStatement sentencia1 = null;

            sentencia1 = conexion.prepareStatement(consulta1);

            //ejecuta la sentecia y retorna un resultado
            ResultSet resultado1 = sentencia1.executeQuery();

            return resultado1;
        }
        return resultado;
    }

    /**
     * Este metodo comprueba si el nombre de un producto ya existe en la base de datos y muestra
     * la información del producto con dicho nombre.
     * @param nombreProducto nombre de un producto
     * @return la información del producto con el nombre introducido
     * @throws SQLException
     */
    public ResultSet buscarNombre(String nombreProducto) throws SQLException {
        PreparedStatement sentencia = null;

        //Guardamos la consulta en un String
        String consulta = "SELECT concat(p.`idProducto`) as 'ID', " +
                "concat(p.`codigoProducto`) as 'Código', concat(p.`nombreProducto`) as 'Nombre', " +
                "concat(c.`idCategoria`, ' - ', c.`nombreCategoria`) as 'Categoría', " +
                "concat(p.`precioUnitProducto`) as 'Precio unitario', " +
                "concat(p.`cantidadProducto`) as 'Cantidad', " +
                "concat(p.`unidadMedidaProducto`) as 'Unidad medida', " +
                "concat(p.`tipoProducto`) as 'Tipo', concat(p.`usosProducto`) as 'Usos' " +
                "FROM producto as p inner join categoria as c on p.idCategoria = c.idCategoria " +
                "WHERE p.nombreProducto=? ORDER BY p.nombreProducto DESC";

        sentencia = conexion.prepareStatement(consulta);
        sentencia.setString(1, nombreProducto);
        ResultSet resultado = sentencia.executeQuery();

        return resultado;
    }

    /**
     * Este metodo busca la información de todos los productos que pertenezcan a
     * la categoría introducida.
     * @param categoria nombre de la categoría
     * @return la información del producto con la categoría introducido
     * @throws SQLException
     */
    public ResultSet buscarCategoria(int categoria) throws SQLException {
        PreparedStatement sentencia = null;

        //Guardamos la consulta en un String
        String consulta = "SELECT concat(p.`idProducto`) as 'ID', " +
                "concat(p.`codigoProducto`) as 'Código', concat(p.`nombreProducto`) as 'Nombre', " +
                "concat(c.`idCategoria`, ' - ', c.`nombreCategoria`) as 'Categoría', " +
                "concat(p.`precioUnitProducto`) as 'Precio unitario', " +
                "concat(p.`cantidadProducto`) as 'Cantidad', " +
                "concat(p.`unidadMedidaProducto`) as 'Unidad medida', " +
                "concat(p.`tipoProducto`) as 'Tipo', concat(p.`usosProducto`) as 'Usos' " +
                "FROM producto as p inner join categoria as c on p.idCategoria = c.idCategoria " +
                "WHERE c.idCategoria=? ORDER BY p.nombreProducto DESC";

        sentencia = conexion.prepareStatement(consulta);
        sentencia.setInt(1, categoria);
        ResultSet resultado = sentencia.executeQuery();

        return resultado;
    }

    public ResultSet buscarCategoriaAdmin(int idCategoria) throws SQLException {
        PreparedStatement sentencia = null;

        //Guardamos la consulta en un String
        String consulta = "SELECT concat(c.`idCategoria`) as 'ID', " +
                "concat(c.`codigoCategoria`) as 'Código', concat(c.`nombreCategoria`) as 'Nombre', " +
                "concat(c.`descripcionCategoria`) as 'Descripción' " +
                "FROM categoria as c WHERE c.idCategoria=?";

        sentencia = conexion.prepareStatement(consulta);
        sentencia.setInt(1, idCategoria);
        ResultSet resultado = sentencia.executeQuery();

        return resultado;
    }

    /**
     * Este metodo busca los usos de un producto y muestra los datos
     * de los productos con ese uso.
     * @param usosProducto usos de un producto
     * @return la información del producto con el uso introducido
     * @throws SQLException
     */
    public ResultSet buscarUsos(String usosProducto) throws SQLException {
        PreparedStatement sentencia = null;

        //Guardamos la consulta en un String
        String consulta = "SELECT concat(p.`idProducto`) as 'ID', " +
                "concat(p.`codigoProducto`) as 'Código', concat(p.`nombreProducto`) as 'Nombre', " +
                "concat(c.`idCategoria`, ' - ', c.`nombreCategoria`) as 'Categoría', " +
                "concat(p.`precioUnitProducto`) as 'Precio unitario', " +
                "concat(p.`cantidadProducto`) as 'Cantidad', " +
                "concat(p.`unidadMedidaProducto`) as 'Unidad medida', " +
                "concat(p.`tipoProducto`) as 'Tipo', concat(p.`usosProducto`) as 'Usos' " +
                "FROM producto as p inner join categoria as c on p.idCategoria = c.idCategoria " +
                "WHERE p.usosProducto=? ORDER BY p.nombreProducto DESC";

        sentencia = conexion.prepareStatement(consulta);
        sentencia.setString(1, usosProducto);
        ResultSet resultado = sentencia.executeQuery();

        return resultado;
    }

    /**
     * Este metodo busca los tipos de un usuario y muestra los datos
     * de los usuarios con ese tipo.
     * @param tipoUsuario tipo de un usuario
     * @return la información del usuario con el tipo introducido
     * @throws SQLException
     */
    public ResultSet buscarTipoUsuario(String tipoUsuario) throws SQLException {
        PreparedStatement sentencia = null;

        String consulta = "SELECT concat(u.`idUsuario`) as 'ID', " +
                "concat(u.`nombreUsuario`) as 'Nombre', concat(u.`apellidoUsuario`) as 'Apellidos', " +
                "concat(u.`tipoUsuario`) as 'Tipo', concat(u.`fechaCumple`) as 'Cumpleaños', " +
                "concat(u.`dniUsuario`) as 'DNI', concat(u.`psswdUsuario`) as 'Contraseña', " +
                "concat(u.`telefono`) as 'Teléfono', concat(u.`fechaAltaUsuario`) as 'Fecha alta' " +
                "FROM usuario as u WHERE tipoUsuario=? ORDER BY nombreUsuario DESC";

        sentencia = conexion.prepareStatement(consulta);
        sentencia.setString(1, tipoUsuario);
        ResultSet resultado = sentencia.executeQuery();

        return  resultado;
    }

    /**
     * Este metodo busca los tipos de un producto y muestra los datos
     * de los productos con ese tipo.
     * @param tipoProducto tipo de un producto
     * @return la información del producto con el tipo introducido
     * @throws SQLException
     */
    public ResultSet buscarTipoProducto(String tipoProducto) throws SQLException {
        PreparedStatement sentencia = null;

        String consulta = "SELECT concat(p.`idProducto`) as 'ID', " +
                "concat(p.`codigoProducto`) as 'Código', concat(p.`nombreProducto`) as 'Nombre', " +
                "concat(c.`idCategoria`, ' - ', c.`nombreCategoria`) as 'Categoría', " +
                "concat(p.`precioUnitProducto`) as 'Precio unitario', " +
                "concat(p.`cantidadProducto`) as 'Cantidad', " +
                "concat(p.`unidadMedidaProducto`) as 'Unidad medida', " +
                "concat(p.`tipoProducto`) as 'Tipo', concat(p.`usosProducto`) as 'Usos' " +
                "FROM producto as p inner join categoria as c on p.idCategoria = c.idCategoria " +
                "WHERE p.tipoProducto = ? ORDER BY p.nombreProducto DESC";

        sentencia = conexion.prepareStatement(consulta);
        sentencia.setString(1, tipoProducto);
        ResultSet resultado = sentencia.executeQuery();

        return  resultado;
    }

    /**
     * Este metodo comprueba si el nombre de una categoría ya existe en la base de datos y muestra
     * la información de la categoría con dicho nombre.
     * @param nombreCategoria nombre de una categoría
     * @return la información de una categoría con el nombre introducido
     * @throws SQLException
     */
    public ResultSet buscarNombreCategoria(String nombreCategoria) throws SQLException {
        PreparedStatement sentencia = null;

        //Guardamos la consulta en un String
        String consulta = "SELECT concat(c.`idCategoria`) as 'ID', " +
                "concat(c.`codigoCategoria`) as 'Código', " +
                "concat(c.`nombreCategoria`) as 'Nombre', " +
                "concat(c.`descripcionCategoria`) as 'Descripción' " +
                "FROM categoria as c WHERE nombreCategoria=? ORDER BY c.nombreCategoria DESC";

        sentencia = conexion.prepareStatement(consulta);
        sentencia.setString(1, nombreCategoria);
        ResultSet resultado = sentencia.executeQuery();

        return resultado;
    }

    /**
     * Este metodo comprueba si el código de una categoría ya existe en la base de datos y muestra
     * la información de la categoría con dicho código.
     * @param codigoCategoria conjunto de valores único para identificar una categoría
     * @return la información de una categoría con el código introducido
     * @throws SQLException
     */
    public ResultSet buscarCodigoCategoria(String codigoCategoria) throws SQLException {
        PreparedStatement sentencia = null;

        //Guardamos la consulta en un String
        String consulta = "SELECT concat(c.`idCategoria`) as 'ID', " +
                "concat(c.`codigoCategoria`) as 'Código', " +
                "concat(c.`nombreCategoria`) as 'Nombre', " +
                "concat(c.`descripcionCategoria`) as 'Descripción' " +
                "FROM categoria as c WHERE codigoCategoria=?";

        sentencia = conexion.prepareStatement(consulta);
        sentencia.setString(1, codigoCategoria);
        ResultSet resultado = sentencia.executeQuery();

        return resultado;
    }

    //----------------------------Métodos Consultar----------------------------

    /**
     * Método para consultar todos los registros de la tabla de información de pedidos
     *
     * @return todos los registros de la tabla de información de pedidos
     * @throws SQLException
     */
    public ResultSet consultarInfoPedido() throws SQLException {

        //Instanciamos un objeto para almacenar la consulta SQL
        PreparedStatement sentencia = null;

        //Guardamos la consulta en un String
        String consulta = "SELECT concat(p.`idPedido`) as 'ID', " +
                "concat(u.`dniUsuario`) as 'Usuario', " +
                "concat(p.`fechaPedido`) as 'Fecha de pedido', " +
                "concat(p.`direccionPedido`) as 'Dirección'" +
                "FROM pedido as p inner join usuario as u on p.idUsuario = u.idUsuario";

        sentencia = conexion.prepareStatement(consulta);
        ResultSet resultado = sentencia.executeQuery();

        return resultado;
    }

    /**
     * Método para consultar todos los registros de la tabla producto
     *
     * @return todos los registros de la tabla de productos
     * @throws SQLException
     */
    public ResultSet consultarProductos() throws SQLException {

        //Instanciamos un objeto para almacenar la consulta SQL
        PreparedStatement sentencia = null;

        //Guardamos la consulta en un String
        String consulta = "SELECT concat(p.`idProducto`) as 'ID', " +
                "concat(p.`codigoProducto`) as 'Código', concat(p.`nombreProducto`) as 'Nombre', " +
                "concat(c.`idCategoria`, ' - ', c.`nombreCategoria`) as 'Categoría', " +
                "concat(p.`precioUnitProducto`) as 'Precio unitario', " +
                "concat(p.`cantidadProducto`) as 'Cantidad', " +
                "concat(p.`unidadMedidaProducto`) as 'Unidad medida', " +
                "concat(p.`tipoProducto`) as 'Tipo', concat(p.`usosProducto`) as 'Usos' " +
                "FROM producto as p inner join categoria as c on p.idCategoria = c.idCategoria";

        sentencia = conexion.prepareStatement(consulta);
        ResultSet resultado = sentencia.executeQuery();

        return resultado;
    }

    /**
     * Método para consultar todos los registros de la tabla categoria
     *
     * @return todos los registros de la tabla de categorías
     * @throws SQLException
     */
    public ResultSet consultarCategorias() throws SQLException {

        //Instanciamos un objeto para almacenar la consulta SQL
        PreparedStatement sentencia = null;

        //Guardamos la consulta en un String
        String consulta = "SELECT concat(c.`idCategoria`) as 'ID', " +
                "concat(c.`codigoCategoria`) as 'Código', " +
                "concat(c.`nombreCategoria`) as 'Nombre', " +
                "concat(c.`descripcionCategoria`) as 'Descripción' FROM categoria as c";

        sentencia = conexion.prepareStatement(consulta);
        ResultSet resultado = sentencia.executeQuery();

        return resultado;
    }

    /**
     * Método para consultar todos los registros de la tabla usuario
     *
     * @return todos los registros de la tabla de usuarios
     * @throws SQLException
     */
    public ResultSet consultarUsuarios() throws SQLException {

        //Instanciamos un objeto para almacenar la consulta SQL
        PreparedStatement sentencia = null;

        //Guardamos la consulta en un String
        String consulta = "SELECT concat(u.`idUsuario`) as 'ID', " +
                "concat(u.`nombreUsuario`) as 'Nombre', concat(u.`apellidoUsuario`) as 'Apellidos', " +
                "concat(u.`tipoUsuario`) as 'Tipo', concat(u.`fechaCumple`) as 'Cumpleaños', " +
                "concat(u.`dniUsuario`) as 'DNI', concat(u.`psswdUsuario`) as 'Contraseña', " +
                "concat(u.`telefono`) as 'Teléfono', concat(u.`fechaAltaUsuario`) as 'Fecha alta' " +
                "FROM usuario as u";

        sentencia = conexion.prepareStatement(consulta);
        ResultSet resultado = sentencia.executeQuery();

        return resultado;
    }

    /**
     * Método para consultar todos los registros de la tabla de información de pedidos
     *
     * @return todos los registros de la tabla de información de pedidos
     * @throws SQLException
     */
    public ResultSet consultarPedido() throws SQLException {

        //Instanciamos un objeto para almacenar la consulta SQL
        PreparedStatement sentencia = null;

        //Guardamos la consulta en un String
        String consulta = "SELECT concat(dp.`idDetalle`) as 'ID', " +
                "concat(u.`apellidoUsuario`,', ', u.`nombreUsuario` ,' - ', p.`fechaPedido`) as 'Pedido', " +
                "concat(pr.`nombreProducto`) as 'Producto', " +
                "concat(dp.`cantidadDetalle`) as 'Cantidad'" +
                "FROM pedido as p, detalle_pedido as dp, producto as pr, usuario as u " +
                "WHERE p.idpedido = dp.idPedido AND dp.idProducto = pr.idProducto " +
                "AND u.idUsuario = p.idUsuario";

        sentencia = conexion.prepareStatement(consulta);
        ResultSet resultado = sentencia.executeQuery();

        return resultado;
    }
    //---------------------Filtros---------------------

    /**
     * Método para ordenar los productos según su precio de menor a mayor.
     * @return los productos ordenados de menor a mayor
     * @throws SQLException
     */
    public ResultSet ordenarPrecioAsc() throws SQLException {
        PreparedStatement sentencia = null;

        //Guardamos la consulta en un String
        String consulta = "SELECT concat(p.`idProducto`) as 'ID', " +
                "concat(p.`codigoProducto`) as 'Código', concat(p.`nombreProducto`) as 'Nombre', " +
                "concat(c.`idCategoria`, ' - ', c.`nombreCategoria`) as 'Categoría', " +
                "concat(p.`precioUnitProducto`) as 'Precio unitario', " +
                "concat(p.`cantidadProducto`) as 'Cantidad', " +
                "concat(p.`unidadMedidaProducto`) as 'Unidad medida', " +
                "concat(p.`tipoProducto`) as 'Tipo', concat(p.`usosProducto`) as 'Usos' " +
                "FROM producto as p inner join categoria as c on p.idCategoria = c.idCategoria" +
                " ORDER BY precioUnitProducto ASC";

        sentencia = conexion.prepareStatement(consulta);
        ResultSet resultado = sentencia.executeQuery();

        return resultado;
    }

    /**
     * Método para ordenar los productos según su precio de mayor a menor.
     * @return los productos ordenados de mayor a menor
     * @throws SQLException
     */
    public ResultSet ordenarPrecioDesc() throws SQLException {
        PreparedStatement sentencia = null;

        //Guardamos la consulta en un String
        String consulta = "SELECT concat(p.`idProducto`) as 'ID', " +
                "concat(p.`codigoProducto`) as 'Código', concat(p.`nombreProducto`) as 'Nombre', " +
                "concat(c.`idCategoria`, ' - ', c.`nombreCategoria`) as 'Categoría', " +
                "concat(p.`precioUnitProducto`) as 'Precio unitario', " +
                "concat(p.`cantidadProducto`) as 'Cantidad', " +
                "concat(p.`unidadMedidaProducto`) as 'Unidad medida', " +
                "concat(p.`tipoProducto`) as 'Tipo', concat(p.`usosProducto`) as 'Usos' " +
                "FROM producto as p inner join categoria as c on p.idCategoria = c.idCategoria" +
                " ORDER BY precioUnitProducto DESC";

        sentencia = conexion.prepareStatement(consulta);
        ResultSet resultado = sentencia.executeQuery();

        return resultado;
    }

    /**
     * Método para ordenar los productos alfabéticamente de la A a la Z.
     * @return los productos ordenados alfabéticamente de la A a la Z
     * @throws SQLException
     */
    public ResultSet ordenarProductoAsc() throws SQLException {
        PreparedStatement sentencia = null;

        //Guardamos la consulta en un String
        String consulta = "SELECT concat(p.`idProducto`) as 'ID', " +
                "concat(p.`codigoProducto`) as 'Código', concat(p.`nombreProducto`) as 'Nombre', " +
                "concat(c.`idCategoria`, ' - ', c.`nombreCategoria`) as 'Categoría', " +
                "concat(p.`precioUnitProducto`) as 'Precio unitario', " +
                "concat(p.`cantidadProducto`) as 'Cantidad', " +
                "concat(p.`unidadMedidaProducto`) as 'Unidad medida', " +
                "concat(p.`tipoProducto`) as 'Tipo', concat(p.`usosProducto`) as 'Usos' " +
                "FROM producto as p inner join categoria as c on p.idCategoria = c.idCategoria " +
                "ORDER BY nombreProducto DESC";

        sentencia = conexion.prepareStatement(consulta);
        ResultSet resultado = sentencia.executeQuery();

        return resultado;
    }

    /**
     * Método para ordenar los productos alfabéticamente de la Z a la A.
     * @return los productos ordenados alfabéticamente de la Z a la A
     * @throws SQLException
     */
    public ResultSet ordenarProductoDesc() throws SQLException {
        PreparedStatement sentencia = null;

        //Guardamos la consulta en un String
        String consulta = "SELECT concat(p.`idProducto`) as 'ID', " +
                "concat(p.`codigoProducto`) as 'Código', concat(p.`nombreProducto`) as 'Nombre', " +
                "concat(c.`idCategoria`, ' - ', c.`nombreCategoria`) as 'Categoría', " +
                "concat(p.`precioUnitProducto`) as 'Precio unitario', " +
                "concat(p.`cantidadProducto`) as 'Cantidad', " +
                "concat(p.`unidadMedidaProducto`) as 'Unidad medida', " +
                "concat(p.`tipoProducto`) as 'Tipo', concat(p.`usosProducto`) as 'Usos' " +
                "FROM producto as p inner join categoria as c on p.idCategoria = c.idCategoria " +
                "ORDER BY nombreProducto ASC";

        sentencia = conexion.prepareStatement(consulta);
        ResultSet resultado = sentencia.executeQuery();

        return resultado;
    }

    /**
     * Método para ordenar los usuarios por su fecha de alta de más reciente a más antiguo
     * @return los usuarios ordenados por su fecha de alta
     * @throws SQLException
     */
    public ResultSet ordenarFechaAltaAsc() throws SQLException {
        PreparedStatement sentencia = null;

        //Guardamos la consulta en un String
        String consulta = "SELECT concat(u.`idUsuario`) as 'ID', " +
                "concat(u.`nombreUsuario`) as 'Nombre', concat(u.`apellidoUsuario`) as 'Apellido', " +
                "concat(u.`tipoUsuario`)as 'Tipo', " +
                "concat(u.`fechaCumple`) as 'Cumpleaños', " +
                "concat(u.`dniUsuario`) as 'DNI', " +
                "concat(u.`psswdUsuario`) as 'Contraseña', " +
                "concat(u.`telefono`) as 'Teléfono', concat(u.`fechaAltaUsuario`) as 'Fecha Alta' " +
                "FROM usuario as u ORDER BY fechaAltaUsuario ASC";

        sentencia = conexion.prepareStatement(consulta);
        ResultSet resultado = sentencia.executeQuery();

        return resultado;
    }

    /**
     * Método para ordenar los usuarios por su fecha de alta de más antiguo a más reciente
     * @return los usuarios ordenados por su fecha de alta
     * @throws SQLException
     */
    public ResultSet ordenarFechaAltaDesc() throws SQLException {
        PreparedStatement sentencia = null;

        //Guardamos la consulta en un String
        String consulta = "SELECT concat(u.`idUsuario`) as 'ID', " +
                "concat(u.`nombreUsuario`) as 'Nombre', concat(u.`apellidoUsuario`) as 'Apellido', " +
                "concat(u.`tipoUsuario`)as 'Tipo', " +
                "concat(u.`fechaCumple`) as 'Cumpleaños', " +
                "concat(u.`dniUsuario`) as 'DNI', " +
                "concat(u.`psswdUsuario`) as 'Contraseña', " +
                "concat(u.`telefono`) as 'Teléfono', concat(u.`fechaAltaUsuario`) as 'Fecha Alta' " +
                "FROM usuario as u ORDER BY fechaAltaUsuario DESC";

        sentencia = conexion.prepareStatement(consulta);
        ResultSet resultado = sentencia.executeQuery();

        return resultado;
    }

    /**
     * Método para ordenar los usuarios alfabéticamente de la A a la Z.
     * @return los usuarios ordenados alfabéticamente de la A a la Z
     * @throws SQLException
     */
    public ResultSet ordenarUsuarioAsc() throws SQLException {
        PreparedStatement sentencia = null;

        //Guardamos la consulta en un String
        String consulta = "SELECT concat(u.`idUsuario`) as 'ID', " +
                "concat(u.`nombreUsuario`) as 'Nombre', concat(u.`apellidoUsuario`) as 'Apellido', " +
                "concat(u.`tipoUsuario`)as 'Tipo', " +
                "concat(u.`fechaCumple`) as 'Cumpleaños', " +
                "concat(u.`dniUsuario`) as 'DNI', " +
                "concat(u.`psswdUsuario`) as 'Contraseña', " +
                "concat(u.`telefono`) as 'Teléfono', concat(u.`fechaAltaUsuario`) as 'Fecha Alta' " +
                "FROM usuario as u ORDER BY nombreUsuario ASC";

        sentencia = conexion.prepareStatement(consulta);
        ResultSet resultado = sentencia.executeQuery();

        return resultado;
    }

    /**
     * Método para ordenar los usuarios alfabéticamente de la Z a la A.
     * @return los usuarios ordenados alfabéticamente de la Z a la A
     * @throws SQLException
     */
    public ResultSet ordenarUsuarioDesc() throws SQLException {
        PreparedStatement sentencia = null;

        //Guardamos la consulta en un String
        String consulta = "SELECT concat(u.`idUsuario`) as 'ID', " +
                "concat(u.`nombreUsuario`) as 'Nombre', concat(u.`apellidoUsuario`) as 'Apellido', " +
                "concat(u.`tipoUsuario`)as 'Tipo', " +
                "concat(u.`fechaCumple`) as 'Cumpleaños', " +
                "concat(u.`dniUsuario`) as 'DNI', " +
                "concat(u.`psswdUsuario`) as 'Contraseña', " +
                "concat(u.`telefono`) as 'Teléfono', concat(u.`fechaAltaUsuario`) as 'Fecha Alta' " +
                "FROM usuario as u ORDER BY nombreUsuario DESC";

        sentencia = conexion.prepareStatement(consulta);
        ResultSet resultado = sentencia.executeQuery();

        return resultado;
    }

    /**
     * Método para ordenar las categorías alfabéticamente de la A a la Z.
     * @return las categorías ordenados alfabéticamente de la A a la Z
     * @throws SQLException
     */
    public ResultSet ordenarCategoriaAsc() throws SQLException {
        PreparedStatement sentencia = null;

        //Guardamos la consulta en un String
        String consulta = "SELECT concat(c.`idCategoria`) as 'ID', " +
                "concat(c.`codigoCategoria`) as 'Código', " +
                "concat(c.`nombreCategoria`) as 'Nombre', " +
                "concat(c.`descripcionCategoria`) as 'Descripción' " +
                "FROM categoria as c ORDER BY c.nombreCategoria ASC";

        sentencia = conexion.prepareStatement(consulta);
        ResultSet resultado = sentencia.executeQuery();

        return resultado;
    }

    /**
     * Método para ordenar las categorías alfabéticamente de la Z a la A.
     * @return las categorías ordenadas alfabéticamente de la Z a la A
     * @throws SQLException
     */
    public ResultSet ordenarCategoriaDesc() throws SQLException {
        PreparedStatement sentencia = null;

        //Guardamos la consulta en un String
        String consulta = "SELECT concat(c.`idCategoria`) as 'ID', " +
                "concat(c.`codigoCategoria`) as 'Código', " +
                "concat(c.`nombreCategoria`) as 'Nombre', " +
                "concat(c.`descripcionCategoria`) as 'Descripción' " +
                "FROM categoria as c ORDER BY c.nombreCategoria DESC";

        sentencia = conexion.prepareStatement(consulta);
        ResultSet resultado = sentencia.executeQuery();

        return resultado;
    }
    
}