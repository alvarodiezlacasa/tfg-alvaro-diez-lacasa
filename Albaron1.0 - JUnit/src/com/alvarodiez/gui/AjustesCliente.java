package com.alvarodiez.gui;

import javax.swing.*;
import java.awt.*;

public class AjustesCliente extends JFrame{
    JPanel panel1;
    JRadioButton btnFuenteUno;
    JRadioButton btnFuenteDos;
    JRadioButton btnFuenteTres;
    JRadioButton btnFuenteCuatro;
    JToggleButton btnModoOscuro;
    JLabel lblModoOscuro;
    JLabel lblEjemploFuente;
    JLabel lblCambiarFuente;

    //JMenuItem
    JMenuItem itemAjustes;
    JMenuItem itemVolver;
    JMenuItem itemAcercaDe;

    public AjustesCliente() {
        startJFrame();
    }

    private void startJFrame() {
        setTitle("Albaron");
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.pack();
        this.setVisible(false);
        this.setEnabled(true);
        this.setSize(new Dimension(this.getWidth(), this.getHeight()));
        this.setLocationRelativeTo(null);
        Image ico=Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("icono.png"));
        this.setIconImage(ico);

        setMenu();
    }

    /**
     * Setea una barra de menus
     */
    private void setMenu(){
        JMenuBar barra = new JMenuBar();
        JMenu menu = new JMenu("Opciones");
        itemAjustes = new JMenuItem("Ajustes");
        itemAjustes.setActionCommand("AjustesCliente");
        itemAcercaDe = new JMenuItem("Acerca de");
        itemAcercaDe.setActionCommand("Acerca de");
        itemVolver = new JMenuItem("Volver");
        itemVolver.setActionCommand("VolverAjustesCliente");
        menu.add(itemAjustes);
        menu.add(itemVolver);
        menu.add(itemAcercaDe);
        barra.add(menu);
        barra.add(Box.createHorizontalGlue());
        this.setJMenuBar(barra);
    }

}
