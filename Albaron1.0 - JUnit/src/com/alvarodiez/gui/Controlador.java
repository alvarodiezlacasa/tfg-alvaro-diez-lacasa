package com.alvarodiez.gui;

import com.alvarodiez.generarAlbaranes.GenerarAlbaran;
import com.alvarodiez.util.Util;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;
import java.time.LocalDate;

public class Controlador implements ActionListener, ListSelectionListener {

    private GenerarAlbaran generarAlbaran;
    private Ajustes ajustes;
    private AjustesCliente ajustesCliente;
    private Modelo modelo;
    private VistaAdmin vistaAdmin;
    private VistaConsultor vistaConsultor;
    private VistaCliente vistaCliente;

    private LoginDialog loginDialog;
    private RegisterDialog registerDialog;
    private CategoryDialog categoryDialog;
    private ProductDialog productDialog;
    private ModifProductDialog modifProductDialog;
    private ModifUserDialog modifUserDialog;
    private ModifCategoryDialog modifCategoryDialog;
    private AboutDialog aboutDialog;

    boolean refrescar;

    public Controlador(Modelo modelo, LoginDialog loginDialog) throws SQLException  {
        this.loginDialog=loginDialog;
        this.modelo=modelo;
        registerDialog=new RegisterDialog();
        vistaAdmin=new VistaAdmin();
        vistaConsultor=new VistaConsultor();
        vistaCliente=new VistaCliente();
        categoryDialog=new CategoryDialog();
        productDialog=new ProductDialog();
        modifProductDialog=new ModifProductDialog();
        modifUserDialog =new ModifUserDialog();
        modifCategoryDialog= new ModifCategoryDialog();
        aboutDialog=new AboutDialog();
        ajustes=new Ajustes();
        ajustesCliente= new AjustesCliente();
        generarAlbaran=new GenerarAlbaran();

        modelo.conectar();
        refrescarTodo();
        addActionListeners(this);
        addItemListeners(this);
    }

    public void refrescarTodo() {
        ajustes.refrescarCategoria(vistaAdmin,vistaConsultor,productDialog,modifProductDialog,modelo);
        ajustes.refrescarProducto(vistaAdmin,vistaConsultor, vistaCliente, modelo);
        ajustes.refrescarUsuario(vistaAdmin,vistaConsultor, vistaCliente, modelo);
        ajustes.refrescarLista(vistaAdmin,modelo);
        ajustes.refrescarInfo(vistaCliente, modelo);
        ajustes.refrescarPedido(vistaCliente, modelo);
        refrescar = false;
    }

    private void addActionListeners(ActionListener listener) {
        //Administrador
        vistaAdmin.btnInsertarCategoriaAdmin.addActionListener(listener);
        vistaAdmin.btnInsertarProductoAdmin.addActionListener(listener);
        vistaAdmin.btnEliminarCategoriaAdmin.addActionListener(listener);
        vistaAdmin.btnEliminarProductoAdmin.addActionListener(listener);
        vistaAdmin.btnEliminarUserAdmin.addActionListener(listener);
        vistaAdmin.btnModificarCategoriaAdmin.addActionListener(listener);
        vistaAdmin.btnModificarProductosAdmin.addActionListener(listener);
        vistaAdmin.btnModificarUserAdmin.addActionListener(listener);
        vistaAdmin.btnBuscarCategoriaAdmin.addActionListener(listener);
        vistaAdmin.btnBuscarDniAdmin.addActionListener(listener);
        vistaAdmin.btnBuscarProductosAdmin.addActionListener(listener);
        vistaAdmin.btnAOrdenarListaAZ.addActionListener(listener);
        vistaAdmin.btnAOrdenarListaZA.addActionListener(listener);
        vistaAdmin.btnPrecioAltoListaAdmin.addActionListener(listener);
        vistaAdmin.btnPrecioBajoListaAdmin.addActionListener(listener);
        vistaAdmin.btnCategoriaListaAdmin.addActionListener(listener);
        vistaAdmin.btnUsosListaAdmin.addActionListener(listener);
        vistaAdmin.btnTipoListaAdmin.addActionListener(listener);
        vistaAdmin.btnNombreListaAdmin.addActionListener(listener);
        vistaAdmin.btnMostrarSinFiltro.addActionListener(listener);
        vistaAdmin.itemAcercaDe.addActionListener(listener);
        vistaAdmin.itemVolver.addActionListener(listener);
        vistaAdmin.btnIcono1.addActionListener(listener);
        vistaAdmin.btnIcono2.addActionListener(listener);
        vistaAdmin.btnIcono3.addActionListener(listener);
        vistaAdmin.btnTipoTabla.addActionListener(listener);
        vistaAdmin.btnModoOscuro.addActionListener(listener);
        vistaAdmin.btnMostrarTodoCat.addActionListener(listener);
        vistaAdmin.btnMostrarTodoProd.addActionListener(listener);
        vistaAdmin.btnMostrarTodoUser.addActionListener(listener);

        registerDialog.btnAtrasRegister.addActionListener(listener);
        categoryDialog.btnCrearCategoria.addActionListener(listener);
        categoryDialog.btnAtrasCategoria.addActionListener(listener);

        productDialog.btnCrearProducto.addActionListener(listener);
        productDialog.btnAtrasProducto.addActionListener(listener);

        modifProductDialog.btnAtrasProdModif.addActionListener(listener);
        modifProductDialog.btnModifProd.addActionListener(listener);

        modifUserDialog.btnAtrasModifUser.addActionListener(listener);
        modifUserDialog.btnModificarUser.addActionListener(listener);
        modifUserDialog.btnHoyUser.addActionListener(listener);

        modifCategoryDialog.btnModifCategoria.addActionListener(listener);
        modifCategoryDialog.btnAtrasCategoria.addActionListener(listener);

        //Login
        loginDialog.btnRegistrarLogin.addActionListener(listener);
        loginDialog.btnVaciarLogin.addActionListener(listener);
        loginDialog.btnValidarLogin.addActionListener(listener);
        loginDialog.itemRegistrar.addActionListener(listener);
        loginDialog.itemAcercaDe.addActionListener(listener);
        loginDialog.itemSalir.addActionListener(listener);
        registerDialog.btnAltaRegister.addActionListener(listener);
        registerDialog.btnCrearUsuario.addActionListener(listener);

        //Consultor
        vistaConsultor.btnNombreProducto.addActionListener(listener);
        vistaConsultor.btnUsosProducto.addActionListener(listener);
        vistaConsultor.btnCategoriaProducto.addActionListener(listener);
        vistaConsultor.btnPrecioProducto.addActionListener(listener);
        vistaConsultor.btnOrdenarProducto.addActionListener(listener);
        vistaConsultor.btnNombreUsuario.addActionListener(listener);
        vistaConsultor.btnDniUsuario.addActionListener(listener);
        vistaConsultor.btnTipoUsuario.addActionListener(listener);
        vistaConsultor.btnAltaUsuario.addActionListener(listener);
        vistaConsultor.btnOrdenarUsuario.addActionListener(listener);
        vistaConsultor.btnNombreCategoria.addActionListener(listener);
        vistaConsultor.btnCodigoCategoria.addActionListener(listener);
        vistaConsultor.btnOrdenarCategoria.addActionListener(listener);
        vistaConsultor.itemAcercaDe.addActionListener(listener);
        vistaConsultor.itemVolver.addActionListener(listener);
        vistaConsultor.btnTipoTabla.addActionListener(listener);
        vistaConsultor.btnIcono1.addActionListener(listener);
        vistaConsultor.btnIcono2.addActionListener(listener);
        vistaConsultor.btnIcono3.addActionListener(listener);
        vistaConsultor.btnResetProducto.addActionListener(listener);
        vistaConsultor.btnResetUsuario.addActionListener(listener);
        vistaConsultor.btnResetCategoria.addActionListener(listener);

        //Cliente
        vistaCliente.itemAcercaDe.addActionListener(listener);
        vistaCliente.itemVolver.addActionListener(listener);
        vistaCliente.itemAjustes.addActionListener(listener);
        vistaCliente.btnInsertarProducto.addActionListener(listener);
        vistaCliente.btnEliminarProducto.addActionListener(listener);
        vistaCliente.btnCrearInfo.addActionListener(listener);
        vistaCliente.btnEliminarInfo.addActionListener(listener);
        vistaCliente.btnGenerarAlbaran.addActionListener(listener);
        vistaCliente.btnVaciarPedido.addActionListener(listener);
        ajustesCliente.btnModoOscuro.addActionListener(listener);
        ajustesCliente.btnFuenteUno.addActionListener(listener);
        ajustesCliente.btnFuenteDos.addActionListener(listener);
        ajustesCliente.btnFuenteTres.addActionListener(listener);
        ajustesCliente.btnFuenteCuatro.addActionListener(listener);
    }

    /**
     * Método mediante el cuál asignamos funciones a los objetos de la vista.
     * @param e comando de la función
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand();
        switch (command) {

            case "Salir": {
                if (JOptionPane.showConfirmDialog(null, "Va a salir. ¿Estás seguro?",
                        "Aviso", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
                    System.exit(0);
                }
            }
                break;

            case "Acerca de": {
                aboutDialog.setVisible(true);
            }
                break;

            case "VolverLogin": {
                vistaAdmin.setVisible(false);
                loginDialog.setVisible(true);
                vistaAdmin.dispose();
            }

            case "VolverLoginConsultor": {
                vistaConsultor.setVisible(false);
                loginDialog.setVisible(true);
                vistaConsultor.dispose();
            }
                break;

            case "VolverLoginCliente": {
                vistaCliente.setVisible(false);
                loginDialog.setVisible(true);
                vistaCliente.dispose();
            }
                break;

            case "ModoOscuroAjustes": {
                if (vistaAdmin.btnModoOscuro.isSelected()) {
                    ajustes.modoOscuro(vistaAdmin, productDialog, modifProductDialog,
                            categoryDialog, modifUserDialog, modifCategoryDialog);
                    vistaAdmin.btnModoOscuro.setText("Activado");
                } else {
                    ajustes.modoClaro(vistaAdmin, productDialog, modifProductDialog,
                            categoryDialog, modifUserDialog, modifCategoryDialog);
                    vistaAdmin.btnModoOscuro.setText("Apagado");

                }
            }
                break;

            case "ModoOscuroCliente": {
                if (ajustesCliente.btnModoOscuro.isSelected()) {
                    ajustes.modoOscuroCliente(ajustesCliente, vistaCliente);
                    ajustesCliente.btnModoOscuro.setText("Activado");
                } else {
                    ajustes.modoClaroCliente(ajustesCliente, vistaCliente);
                    ajustesCliente.btnModoOscuro.setText("Desactivado");
                }
            }
                break;

            case "Fuente 1": {
                ajustes.tablaAjustesCliente1(vistaCliente, ajustesCliente);
            }
                break;

            case "Fuente 2": {
                ajustes.tablaAjustesCliente2(vistaCliente, ajustesCliente);
            }
                break;

            case "Fuente 3": {
                ajustes.tablaAjustesCliente3(vistaCliente, ajustesCliente);
            }
                break;

            case "Fuente 4": {
                ajustes.tablaAjustesCliente4(vistaCliente, ajustesCliente);
            }
                break;

            case "IconoUno": {
                if (vistaAdmin.btnIcono1.isSelected())
                    ajustes.imagenIcono1Admin(vistaAdmin);

                if (vistaConsultor.btnIcono1.isSelected())
                    ajustes.imagenIcono1Consultor(vistaConsultor);
            }
                break;

            case "IconoDos": {
                if (vistaAdmin.btnIcono2.isSelected())
                    ajustes.imagenIcono2Admin(vistaAdmin);

                if (vistaConsultor.btnIcono2.isSelected())
                    ajustes.imagenIcono2Consultor(vistaConsultor);
            }
                break;

            case "IconoTres": {
                if (vistaAdmin.btnIcono3.isSelected())
                    ajustes.imagenIcono3Admin(vistaAdmin);

                if (vistaConsultor.btnIcono3.isSelected())
                    ajustes.imagenIcono3Consultor(vistaConsultor);
            }
                break;

            case "TablaAjustesAdmin": {
                if (vistaAdmin.btnTipoTabla.isSelected()) {
                    ajustes.tablaAjustesAdmin1(vistaAdmin);
                } else {
                    ajustes.tablaAjustesAdmin2(vistaAdmin);
                }
            }
                break;

            case "TablaAjustesConsultor": {
                if (vistaConsultor.btnTipoTabla.isSelected()) {
                    ajustes.tablaAjustesConsultor1(vistaConsultor);
                } else {
                    ajustes.tablaAjustesConsultor2(vistaConsultor);
                }
            }
                break;

//----------------------Listeners del Login y Registro---------------------

            case "Registrarme": {
                if (registerDialog.isActive()) {
                    loginDialog.setEnabled(false);
                    registerDialog.setVisible(true);
                } else {
                    loginDialog.setEnabled(true);
                    registerDialog.setVisible(true);
                }
            }
                break;

            case "fechaHoyRegister": {
                LocalDate miFecha = LocalDate.now();
                registerDialog.txtAltaRegister.setText(String.valueOf(miFecha));
            }
                break;

            case "fechaHoyModif": {
                LocalDate miFecha = LocalDate.now();
                modifUserDialog.txtAltaUser.setText(String.valueOf(miFecha));
            }

            case "VaciarLogin": {
                ajustes.vaciarLogin(loginDialog);
            }
                break;

            case "ValidarLogin": {
                String dni = loginDialog.txtDNILogin.getText();
                String psswd = loginDialog.txtPsswdLogin.getText();
                String tipo = (String) loginDialog.cbTipoUsuario.getSelectedItem();
                boolean comprobacion = modelo.existeUsuario(dni, psswd, tipo);
                if (comprobacion) {
                    switch (tipo) {
                        case "Administrador":
                            loginDialog.setVisible(false);
                            vistaAdmin.setVisible(true);
                            loginDialog.dispose();
                            break;

                        case "Consultor":
                            loginDialog.setVisible(false);
                            vistaConsultor.setVisible(true);
                            loginDialog.dispose();
                            break;

                        case "Cliente":
                            loginDialog.setVisible(false);
                            vistaCliente.setVisible(true);
                            loginDialog.dispose();
                            break;
                    }
                    loginDialog.txtDNILogin.setText(null);
                    loginDialog.txtPsswdLogin.setText(null);
                    loginDialog.cbTipoUsuario.setSelectedItem(null);

                } else {
                    Util.showErrorAlert("Credenciales incorrectas. Regístrate si todavia no tienes cuenta");
                }
            }
                break;

        //-----------------------Listeners de busquedas y filtros-------------------------

            //~~Cliente~~

            case "GenerarAlbaran": {
                try {
                    generarAlbaran.generarAlbaranObra(modelo);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
                break;

            //~~Consultor~~
            case "ResetProducto": {
                try {
                    vistaConsultor.dtmListaProductos.setRowCount(0);
                    ResultSet rs = modelo.consultarProductos();
                    ajustes.cargarFilasProductsConsultor(rs, vistaConsultor);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
                break;

            case "NombreProductoConsultor": {
                try {
                    String nombre = vistaConsultor.txtNombreProducto.getText();
                    if (nombre.isEmpty()) {
                        Util.showErrorAlert("Introduce el nombre del producto");
                    } else {
                        vistaConsultor.dtmListaProductos.setRowCount(0);
                        ResultSet rs = modelo.buscarNombre(nombre);
                        ajustes.cargarFilasProductsConsultor(rs, vistaConsultor);
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
                break;

            case "UsosProductoConsultor": {
                try {
                    if (vistaConsultor.cbUsosProducto.getSelectedIndex() == -1) {
                        Util.showErrorAlert("Selecciona un uso");
                    } else {
                        String usos = String.valueOf(vistaConsultor.cbUsosProducto.getSelectedItem());
                        vistaConsultor.dtmListaProductos.setRowCount(0);
                        ResultSet rs = modelo.buscarUsos(usos);
                        ajustes.cargarFilasProductsConsultor(rs, vistaConsultor);
                        vistaConsultor.cbUsosProducto.setSelectedIndex(-1);
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
                break;

            case "CategoriaProductoConsultor": {
                try {
                    if (vistaConsultor.cbCategoriaProducto.getSelectedIndex() == -1){
                        Util.showErrorAlert("Selecciona una categoría para poder filtrar");
                    } else {
                        String nombreCategoria = String.valueOf(vistaConsultor.cbCategoriaProducto.getSelectedItem());
                        vistaConsultor.dtmListaProductos.setRowCount(0);
                        int idCategoria = Integer.parseInt(nombreCategoria.substring(0, 1));
                        ResultSet rs = modelo.buscarCategoria(idCategoria);
                        ajustes.cargarFilasProductsConsultor(rs, vistaConsultor);
                        vistaConsultor.cbCategoriaProducto.setSelectedIndex(-1);
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
                break;

            case "PrecioProductoConsultor": {
                try {
                    if (vistaConsultor.btnPrecioProducto.isSelected()) {
                        vistaConsultor.btnPrecioProducto.setText("< a >");
                        vistaConsultor.dtmListaProductos.setRowCount(0);
                        ResultSet rs = modelo.ordenarPrecioDesc();
                        ajustes.cargarFilasProductsConsultor(rs, vistaConsultor);
                    } else {
                        vistaConsultor.btnPrecioProducto.setText("> a <");
                        vistaConsultor.dtmListaProductos.setRowCount(0);
                        ResultSet rs = modelo.ordenarPrecioAsc();
                        ajustes.cargarFilasProductsConsultor(rs, vistaConsultor);
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
                break;

            case "alfabetProductoConsultor": {
                try {
                    if (vistaConsultor.btnOrdenarProducto.isSelected()) {
                        vistaConsultor.btnOrdenarProducto.setText("A - Z");
                        vistaConsultor.dtmListaProductos.setRowCount(0);
                        ResultSet rs = modelo.ordenarProductoAsc();
                        ajustes.cargarFilasProductsConsultor(rs, vistaConsultor);
                    } else {
                        vistaConsultor.btnOrdenarProducto.setText("Z - A");
                        vistaConsultor.dtmListaProductos.setRowCount(0);
                        ResultSet rs = modelo.ordenarProductoDesc();
                        ajustes.cargarFilasProductsConsultor(rs, vistaConsultor);

                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
                break;

            case "ResetUsuario": {
                try {
                    vistaConsultor.dtmListaUsuarios.setRowCount(0);
                    ResultSet rs = modelo.consultarUsuarios();
                    ajustes.cargarFilasUsuariosConsultor(rs, vistaConsultor);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
                break;

            case "NombreUsuarioConsultor": {
                try {
                    String nombre = vistaConsultor.txtNombreUsuario.getText();
                    if (nombre.isEmpty()) {
                        Util.showErrorAlert("Introduce el nombre del usuario");
                    } else {
                        vistaConsultor.dtmListaUsuarios.setRowCount(0);
                        ResultSet rs = modelo.buscarNombreUsuario(nombre);
                        ajustes.cargarFilasUsuariosConsultor(rs, vistaConsultor);
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
                break;

            case "DNIUsuarioConsultor": {
                try {
                    //Creamos un string de dni y llamamos al txt de buscar
                    // para coger el texto que hemos escrito
                    String dniUsuario = vistaConsultor.txtDniUsuario.getText();
                    //Comprueba si el txt está vacío
                    if (dniUsuario.isEmpty()) {
                        Util.showErrorAlert("Introduce un número de DNI");
                    } else {
                        //Limpia los registros de la tabla
                        vistaConsultor.dtmListaUsuarios.setRowCount(0);
                        //Ejecutamos la consulta y guardamos el resultado en un ResulSet
                        ResultSet rs = modelo.buscarDni(dniUsuario);
                        //Cargamos los registros de la consulta
                        ajustes.cargarFilasUsuariosConsultor(rs, vistaConsultor);
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
                break;

            case "TipoUsuarioConsultor": {
                try {
                    if (vistaConsultor.cbTipoUsuario.getSelectedIndex()==-1){
                        Util.showErrorAlert("Selecciona una tipo para poder filtrar");
                    } else {
                        String tipo = (String) vistaConsultor.cbTipoUsuario.getSelectedItem();
                        vistaConsultor.dtmListaUsuarios.setRowCount(0);
                        ResultSet rs = modelo.buscarTipoUsuario(tipo);
                        ajustes.cargarFilasUsuariosConsultor(rs, vistaConsultor);
                        vistaConsultor.cbTipoUsuario.setSelectedIndex(-1);
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
                break;

            case "fechaUsuarioConsultor": {
                try {
                    if (vistaConsultor.btnAltaUsuario.isSelected()) {
                        vistaConsultor.btnAltaUsuario.setText("Más antiguo");
                        vistaConsultor.dtmListaUsuarios.setRowCount(0);
                        ResultSet rs = modelo.ordenarFechaAltaAsc();
                        ajustes.cargarFilasUsuariosConsultor(rs, vistaConsultor);
                    } else {
                        vistaConsultor.btnAltaUsuario.setText("Más nuevo");
                        vistaConsultor.dtmListaUsuarios.setRowCount(0);
                        ResultSet rs = modelo.ordenarFechaAltaDesc();
                        ajustes.cargarFilasUsuariosConsultor(rs, vistaConsultor);
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
                break;

            case "ordenarUsuarioConsultor": {
                try {
                    if (vistaConsultor.btnOrdenarUsuario.isSelected()) {
                        vistaConsultor.btnOrdenarUsuario.setText("A - Z");
                        vistaConsultor.dtmListaUsuarios.setRowCount(0);
                        ResultSet rs = modelo.ordenarUsuarioDesc();
                        ajustes.cargarFilasUsuariosConsultor(rs, vistaConsultor);

                    } else {
                        vistaConsultor.btnOrdenarUsuario.setText("Z - A");
                        vistaConsultor.dtmListaUsuarios.setRowCount(0);
                        ResultSet rs = modelo.ordenarUsuarioAsc();
                        ajustes.cargarFilasUsuariosConsultor(rs, vistaConsultor);
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
                break;

            case "ResetCategoria": {
                try {
                    vistaConsultor.dtmListaCategorias.setRowCount(0);
                    ResultSet rs = modelo.consultarCategorias();
                    ajustes.cargarFilasCategoriaConsultor(rs, vistaConsultor);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
                break;

            case "NombreCategoriaConsultor": {
                try {
                    //Creamos un string de dni y llamamos al txt de buscar
                    // para coger el texto que hemos escrito
                    String nombreCategoria = vistaConsultor.txtNombreCategoria.getText();
                    //Comprueba si el txt está vacío
                    if (nombreCategoria.isEmpty()) {
                        Util.showErrorAlert("Introduce un nombre");
                    } else {
                        //Limpia los registros de la tabla
                        vistaConsultor.dtmListaCategorias.setRowCount(0);
                        //Ejecutamos la consulta y guardamos el resultado en un ResulSet
                        ResultSet rs = modelo.buscarNombreCategoria(nombreCategoria);
                        //Cargamos los registros de la consulta
                        ajustes.cargarFilasCategoriaConsultor(rs, vistaConsultor);
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
                break;

            case "CodigoCategoriaConsultor": {
                try {
                    //Creamos un string de dni y llamamos al txt de buscar
                    // para coger el texto que hemos escrito
                    String codigoCategoria = vistaConsultor.txtCodigoCategoria.getText();
                    //Comprueba si el txt está vacío
                    if (codigoCategoria.isEmpty()) {
                        Util.showErrorAlert("Introduce un código");
                    } else {
                        //Limpia los registros de la tabla
                        vistaConsultor.dtmListaCategorias.setRowCount(0);
                        //Ejecutamos la consulta y guardamos el resultado en un ResulSet
                        ResultSet rs = modelo.buscarCodigoCategoria(codigoCategoria);
                        //Cargamos los registros de la consulta
                        ajustes.cargarFilasCategoriaConsultor(rs, vistaConsultor);
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
            break;

            case "OrdenarCategoriaConsultor": {
                try {
                    if (vistaConsultor.btnOrdenarCategoria.isSelected()) {
                        vistaConsultor.btnOrdenarCategoria.setText("A - Z");
                        vistaConsultor.dtmListaCategorias.setRowCount(0);
                        ResultSet rs = modelo.ordenarCategoriaDesc();
                        ajustes.cargarFilasCategoriaConsultor(rs, vistaConsultor);
                    } else {
                        vistaConsultor.btnOrdenarCategoria.setText("Z - A");
                        vistaConsultor.dtmListaCategorias.setRowCount(0);
                        ResultSet rs = modelo.ordenarCategoriaAsc();
                        ajustes.cargarFilasCategoriaConsultor(rs, vistaConsultor);
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
                break;

            //~~Admin~~

            case "BuscarUsuarioAdmin": {
                try {
                    //Creamos un string de dni y llamamos al txt de buscar
                    // para coger el texto que hemos escrito
                    String dniUsuario = vistaAdmin.txtBuscarUserAdmin.getText();
                    //Comprueba si el txt está vacío
                    if (dniUsuario.isEmpty()) {
                        Util.showErrorAlert("Introduce un número de DNI");
                    } else {
                        //Limpia los registros de la tabla
                        vistaAdmin.dtmUsuarioAdmin.setRowCount(0);
                        //Ejecutamos la consulta y guardamos el resultado en un ResulSet
                        ResultSet rs = modelo.buscarDni(dniUsuario);
                        //Cargamos los registros de la consulta
                        ajustes.cargarFilasUsuariosAdmin(rs, vistaAdmin);
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
                break;

            case "BuscarCategoriaAdmin": {
                try {
                    if (vistaAdmin.cbBuscarCategoria.getSelectedIndex()==-1){
                        Util.showErrorAlert("Selecciona una categoría para poder filtrar");
                    } else {
                        String nombreCategoria = (String) vistaAdmin.cbBuscarCategoria.getSelectedItem();
                        vistaAdmin.dtmCategoriaAdmin.setRowCount(0);
                        int idCategoria = Integer.parseInt(nombreCategoria.substring(0, 1));
                        ResultSet rs = modelo.buscarCategoriaAdmin(idCategoria);
                        ajustes.cargarFilasCategoriaAdmin(rs, vistaAdmin);
                        vistaAdmin.cbBuscarCategoria.setSelectedIndex(-1);
                    }
                } catch (SQLException ex) {
                        ex.printStackTrace();
                }

            }
                break;

            case "BuscarProductoAdmin": {
                try {
                    if (vistaAdmin.txtBuscarProductosAdmin.getText()==""){
                        Util.showErrorAlert("Selecciona una categoría para poder filtrar");
                    } else {
                        String nombreProducto = (String) vistaAdmin.txtBuscarProductosAdmin.getText();
                        vistaAdmin.dtmProductoAdmin.setRowCount(0);
                        ResultSet rs = modelo.buscarNombre(nombreProducto);
                        ajustes.cargarFilasProductsAdmin(rs, vistaAdmin);
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }

            }
                break;

            case "MostrarTodoProducto": {
                try {
                    vistaAdmin.dtmProductoAdmin.setRowCount(0);
                    ResultSet rs = modelo.consultarProductos();
                    ajustes.cargarFilasProductsAdmin(rs, vistaAdmin);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
                break;

            case "MostrarTodoUsuario": {
                try {
                    vistaAdmin.dtmUsuarioAdmin.setRowCount(0);
                    ResultSet rs = modelo.consultarUsuarios();
                    ajustes.cargarFilasUsuariosAdmin(rs, vistaAdmin);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
                break;

            case "MostrarTodoCategoria": {
                try {
                    vistaAdmin.dtmCategoriaAdmin.setRowCount(0);
                    ResultSet rs = modelo.consultarCategorias();
                    ajustes.cargarFilasCategoriaAdmin(rs, vistaAdmin);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
                break;

            case "PrecioMasAlto": {
                try {
                    vistaAdmin.dtmListaAdmin.setRowCount(0);
                    ResultSet rs = modelo.ordenarPrecioDesc();
                    ajustes.cargarFilasListadoAdmin(rs, vistaAdmin);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
                break;

            case "PrecioMasBajo": {
                try {
                    vistaAdmin.dtmListaAdmin.setRowCount(0);
                    ResultSet rs = modelo.ordenarPrecioAsc();
                    ajustes.cargarFilasListadoAdmin(rs, vistaAdmin);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
                break;

            case "OrdenarAZ": {
                try {
                    vistaAdmin.dtmListaAdmin.setRowCount(0);
                    ResultSet rs = modelo.ordenarProductoDesc();
                    ajustes.cargarFilasListadoAdmin(rs, vistaAdmin);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
                break;

            case "OrdenarZA": {
                try {
                    vistaAdmin.dtmListaAdmin.setRowCount(0);
                    ResultSet rs = modelo.ordenarProductoAsc();
                    ajustes.cargarFilasListadoAdmin(rs, vistaAdmin);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
                break;

            case "TipoProductoListado": {
                try {
                    String tipo = vistaAdmin.txtTipoListaAdmin.getText().toLowerCase();
                    if (tipo.isEmpty()) {
                        Util.showErrorAlert("Introduce el tipo de un producto");
                    } else {
                        vistaAdmin.dtmListaAdmin.setRowCount(0);
                        ResultSet rs = modelo.buscarTipoProducto(tipo);
                        ajustes.cargarFilasListadoAdmin(rs, vistaAdmin);
                    }

                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
                break;

            case "NombreProductoListado": {
                try {
                    String nombre = vistaAdmin.txtNombreListaAdmin.getText().toLowerCase();
                    if (nombre.isEmpty()) {
                        Util.showErrorAlert("Introduce el nombre de un producto");
                    } else {
                        vistaAdmin.dtmListaAdmin.setRowCount(0);
                        ResultSet rs = modelo.buscarNombre(nombre);
                        ajustes.cargarFilasListadoAdmin(rs, vistaAdmin);
                    }

                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
                break;

            case "CategoriaProductoListado": {
                try {
                    if (vistaAdmin.cbCategoriaListaAdmin.getSelectedIndex()==-1){
                        Util.showErrorAlert("Selecciona una categoría para poder filtrar");
                    } else {
                        vistaAdmin.dtmListaAdmin.setRowCount(0);
                        String categoria = (String) vistaAdmin.cbCategoriaListaAdmin.getSelectedItem();
                        int idCategoria = Integer.parseInt(categoria.substring(0, 1));

                        ResultSet rs = modelo.buscarCategoria(idCategoria);
                        ajustes.cargarFilasListadoAdmin(rs, vistaAdmin);
                        vistaAdmin.cbCategoriaListaAdmin.setSelectedIndex(-1);
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
                break;

            case "UsosProductoListado": {
                try {
                    if (vistaAdmin.cbUsosListaAdmin.getSelectedIndex()==-1){
                        Util.showErrorAlert("Selecciona un uso para poder filtrar");
                    } else {
                        vistaAdmin.dtmListaAdmin.setRowCount(0);
                        String usos = (String) vistaAdmin.cbUsosListaAdmin.getSelectedItem();
                        ResultSet rs = modelo.buscarUsos(usos);
                        ajustes.cargarFilasListadoAdmin(rs, vistaAdmin);
                        vistaAdmin.cbUsosListaAdmin.setSelectedIndex(-1);
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
                break;

            case "MostrarSinFiltros": {
                try {
                    vistaAdmin.dtmListaAdmin.setRowCount(0);
                    ResultSet rs = modelo.consultarProductos();
                    ajustes.cargarFilasListadoAdmin(rs, vistaAdmin);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
                break;

        //-------------------------Listeners de eliminar-------------------------

            case "EliminarCategoriaAdmin": {
                try {
                    modelo.eliminarCategoria(Integer.parseInt((String) vistaAdmin.tableCategoriasAdmin.
                            getValueAt(vistaAdmin.tableCategoriasAdmin.getSelectedRow(), 0)));
                    ajustes.refrescarCategoria(vistaAdmin, vistaConsultor, productDialog, modifProductDialog, modelo);
                    ajustes.refrescarProducto(vistaAdmin, vistaConsultor, vistaCliente, modelo);
                    ajustes.refrescarLista(vistaAdmin, modelo);
                } catch (ArrayIndexOutOfBoundsException e1) {
                    Util.showWarningAlert("Debes seleccionar un registro de la tabla antes de borrar");
                }
            }
                break;

            case "EliminarProductoAdmin": {
                try {
                    modelo.eliminarProducto(Integer.parseInt((String) vistaAdmin.tableProductosAdmin.
                            getValueAt(vistaAdmin.tableProductosAdmin.getSelectedRow(), 0)));
                    ajustes.refrescarProducto(vistaAdmin, vistaConsultor, vistaCliente, modelo);
                } catch (ArrayIndexOutOfBoundsException e1) {
                    Util.showWarningAlert("Debes seleccionar un registro de la tabla antes de borrar");
                }
            }
                break;

            case "EliminarUsuarioAdmin": {
                try {
                    modelo.eliminarUsuario(Integer.parseInt((String) vistaAdmin.tableUsuarioAdmin.
                            getValueAt(vistaAdmin.tableUsuarioAdmin.getSelectedRow(), 0)));
                    ajustes.refrescarUsuario(vistaAdmin, vistaConsultor, vistaCliente, modelo);
                } catch (ArrayIndexOutOfBoundsException e1) {
                    Util.showWarningAlert("Debes seleccionar un registro de la tabla antes de borrar");
                }
            }
                break;

            case "EliminarProductoPedido": {
                try {
                    modelo.eliminarProductoPedido(Integer.parseInt((String) vistaCliente.tablaPedidos.
                            getValueAt(vistaCliente.tablaPedidos.getSelectedRow(), 0)));
                    ajustes.refrescarPedido(vistaCliente, modelo);
                } catch (ArrayIndexOutOfBoundsException e1) {
                    Util.showWarningAlert("Debes seleccionar un registro de la tabla antes de borrar");
                }
            }
                break;

            case "EliminarInfoPedido": {
                try{
                    modelo.eliminarInfoPedido(Integer.parseInt((String) vistaCliente.tablaInfo.
                            getValueAt(vistaCliente.tablaInfo.getSelectedRow(), 0)));
                    ajustes.refrescarInfo(vistaCliente, modelo);
                    ajustes.refrescarPedido(vistaCliente, modelo);
                } catch (ArrayIndexOutOfBoundsException e1) {
                        Util.showWarningAlert("Debes seleccionar un registro de la tabla antes de borrar");
                }
            }
                break;

            case "VaciarCarrito": {
                modelo.vaciarCarrito();
                ajustes.refrescarPedido(vistaCliente, modelo);
            }
                break;

        //-------------------------Listeners de modificar--------------------------

            case "ModificarProducto": {
                try {
                    if (ajustes.comprobarProductoModifVacio(modifProductDialog)) {
                        Util.showErrorAlert("Rellena todos los campos");
                    } else {
                        modelo.modificarProducto(
                                modifProductDialog.txtCodigoProductoModif.getText(),
                                modifProductDialog.txtNombreProductoModif.getText(),
                                String.valueOf(modifProductDialog.cbCatProductoModif.getSelectedItem()),
                                Double.parseDouble(modifProductDialog.txtPrecioProductoModif.getText()),
                                Double.parseDouble(modifProductDialog.txtCantidadProductoModif.getText()),
                                modifProductDialog.txtMedidaProductoModif.getText(),
                                modifProductDialog.txtTipoProductoModif.getText(),
                                String.valueOf(modifProductDialog.cbUsosProductoModif.getSelectedItem()),
                                Integer.parseInt((String) vistaAdmin.tableProductosAdmin.getValueAt(vistaAdmin.tableProductosAdmin.getSelectedRow(), 0)));
                        ajustes.refrescarProducto(vistaAdmin, vistaConsultor, vistaCliente, modelo);
                        ajustes.borrarCamposProductoModif(modifProductDialog);

                        modifProductDialog.setVisible(false);
                        modifProductDialog.dispose();
                        vistaAdmin.setEnabled(true);
                    }
                } catch (ArrayIndexOutOfBoundsException e1) {
                    Util.showWarningAlert("Debes seleccionar un registro de la tabla antes de modificar");
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vistaAdmin.tableProductosAdmin.clearSelection();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
                break;

            case "ModificarUsuario": {
                try {
                    if (ajustes.comprobarUsuarioModifVacio(modifUserDialog)) {
                        Util.showErrorAlert("Rellena todos los campos");
                    } else {
                        modelo.modificarUsuario(
                                modifUserDialog.txtNombreUser.getText(),
                                modifUserDialog.txtApellidoUser.getText(),
                                String.valueOf(modifUserDialog.cbTipoUser.getSelectedItem()),
                                Date.valueOf(String.valueOf(modifUserDialog.dpCumpleUser.getDate())),
                                modifUserDialog.txtDniUser.getText(),
                                modifUserDialog.txtPsswdUser.getText(),
                                modifUserDialog.txtTelefonoUser.getText(),
                                Date.valueOf(modifUserDialog.txtAltaUser.getText()),
                                Integer.parseInt(String.valueOf(vistaAdmin.tableUsuarioAdmin.getValueAt(vistaAdmin.tableUsuarioAdmin.getSelectedRow(), 0))));
                        ajustes.refrescarUsuario(vistaAdmin, vistaConsultor,vistaCliente, modelo);
                        ajustes.borrarCamposUsuarioModif(modifUserDialog);

                        modifUserDialog.setVisible(false);
                        modifUserDialog.dispose();
                        vistaAdmin.setEnabled(true);
                    }
                } catch (ArrayIndexOutOfBoundsException e1) {
                    Util.showWarningAlert("Debes seleccionar un registro de la tabla antes de borrar");
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vistaAdmin.tableUsuarioAdmin.clearSelection();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
                break;

            case "ModificarCategoria": {
                try {
                    if (ajustes.comprobarCategoriaModifVacia(modifCategoryDialog)) {
                        Util.showErrorAlert("Rellena todos los campos");
                    } else {
                        modelo.modificarCategoria(
                                modifCategoryDialog.txtCodigoModif.getText(),
                                modifCategoryDialog.txtNombreModif.getText(),
                                modifCategoryDialog.txtDescripcionModif.getText(),
                                Integer.parseInt(String.valueOf(vistaAdmin.tableCategoriasAdmin.getValueAt(vistaAdmin.tableCategoriasAdmin.getSelectedRow(), 0))));
                        ajustes.refrescarCategoria(vistaAdmin, vistaConsultor, productDialog, modifProductDialog, modelo);
                        ajustes.refrescarProducto(vistaAdmin, vistaConsultor, vistaCliente, modelo);
                        ajustes.borrarCamposCategoriaModif(modifCategoryDialog);

                        modifCategoryDialog.setVisible(false);
                        modifCategoryDialog.dispose();
                        vistaAdmin.setEnabled(true);
                    }
                } catch (ArrayIndexOutOfBoundsException e1) {
                    Util.showWarningAlert("Debes seleccionar un registro de la tabla antes de borrar");
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vistaAdmin.tableCategoriasAdmin.clearSelection();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
                break;

        //-----------------------Listeners para cambiar de vista--------------------------

            case "InsertarCategoriaAdmin": {
                if (categoryDialog.isVisible()) {
                    vistaAdmin.setEnabled(false);
                    categoryDialog.setVisible(true);
                } else {
                    vistaAdmin.setEnabled(true);
                    categoryDialog.setVisible(true);
                }

            }
                break;

            case "InsertarProductoAdmin": {
                if (productDialog.isActive()) {
                    vistaAdmin.setEnabled(false);
                    productDialog.setVisible(true);
                } else {
                    vistaAdmin.setEnabled(true);
                    productDialog.setVisible(true);
                }
            }
                break;

            case "ModificarProductoAdmin": {
                if (modifProductDialog.isActive()) {
                    vistaAdmin.setEnabled(false);
                    modifProductDialog.setVisible(true);
                } else {
                    vistaAdmin.setEnabled(true);
                    modifProductDialog.setVisible(true);
                }
            }
                break;

            case "ModificarUsuarioAdmin": {
                if (modifUserDialog.isActive()) {
                    vistaAdmin.setEnabled(false);
                    modifUserDialog.setVisible(true);
                } else {
                    vistaAdmin.setEnabled(true);
                    modifUserDialog.setVisible(true);
                }
            }
                break;

            case "ModificarCategoriaAdmin": {
                if (modifCategoryDialog.isActive()) {
                    vistaAdmin.setEnabled(false);
                    modifCategoryDialog.setVisible(true);
                } else {
                    vistaAdmin.setEnabled(true);
                    modifCategoryDialog.setVisible(true);
                }
            }
                break;

            case "AjustesCliente": {
                ajustesCliente.setVisible(true);
            }
                break;

            //----------------------Listeners para insertar datos----------------------

            case "CrearProducto": {
                if (ajustes.comprobarProductoVacio(productDialog)) {
                    Util.showErrorAlert("Rellena todos los campos");
                } else if (modelo.productoYaExiste(productDialog.txtCodigoProducto.getText())) {
                    Util.showErrorAlert("Esa categoría ya pertenece a otra categoría");
                } else {
                    modelo.insertarProducto(
                            productDialog.txtCodigoProducto.getText(),
                            productDialog.txtNombreProducto.getText(),
                            String.valueOf(productDialog.cbCategoriaProducto.getSelectedItem()),
                            Double.valueOf(productDialog.txtPrecioProducto.getText()),
                            Double.valueOf(productDialog.txtCantidadProducto.getText()),
                            productDialog.txtMedidaProducto.getText(),
                            productDialog.txtTipoProducto.getText(),
                            String.valueOf(productDialog.cbUsosProducto.getSelectedItem()));
                    ajustes.borrarCamposProducto(productDialog);
                    productDialog.setVisible(false);
                    productDialog.dispose();
                    vistaAdmin.setEnabled(true);
                }
                ajustes.refrescarProducto(vistaAdmin,vistaConsultor, vistaCliente, modelo);
                ajustes.refrescarLista(vistaAdmin,modelo);
            }
                break;

            case "CrearCategoria": {
                if (ajustes.comprobarCategoriaVacia(categoryDialog)) {
                    Util.showErrorAlert("Rellena todos los campos");
                } else if (modelo.categoriaYaExiste(categoryDialog.txtCodigoCategoria.getText())) {
                    Util.showErrorAlert("Esa categoría ya pertenece a otra categoría");
                } else {
                    modelo.insertarCategoria(
                            categoryDialog.txtCodigoCategoria.getText(),
                            categoryDialog.txtNombreCategoria.getText(),
                            categoryDialog.txtDescripcionCategoria.getText());
                    ajustes.borrarCamposCategoria(categoryDialog);
                    categoryDialog.setVisible(false);
                    categoryDialog.dispose();
                    vistaAdmin.setEnabled(true);
                }
                ajustes.refrescarCategoria(vistaAdmin,vistaConsultor,productDialog,modifProductDialog,modelo);
            }
                break;

            case "CrearUsuario": {
                try {
                    if (ajustes.comprobarUsuarioVacio(registerDialog)) {
                        Util.showErrorAlert("Rellena todos los campos");
                    } else if (modelo.dniYaExiste(registerDialog.txtDniRegister.getText())) {
                        Util.showErrorAlert("Ese dni ya existe.\nIntroduce un dni diferente");
                    } else {
                        if (registerDialog.txtPsswdRegister.getText().equals(registerDialog.txtConfirmarRegister.getText())) {
                            modelo.insertarUsuario(
                                    registerDialog.txtNombreRegister.getText(),
                                    registerDialog.txtApellidoRegister.getText(),
                                    String.valueOf(registerDialog.cbTipoRegister.getSelectedItem()),
                                    Date.valueOf(registerDialog.dpCumpleRegister.getDate()),
                                    registerDialog.txtDniRegister.getText(),
                                    registerDialog.txtPsswdRegister.getText(),
                                    registerDialog.txtTelefonoRegister.getText(),
                                    Date.valueOf(registerDialog.txtAltaRegister.getText()));
                            ajustes.borrarCamposUsuario(registerDialog);
                            registerDialog.setVisible(false);
                            loginDialog.setVisible(true);
                            registerDialog.dispose();
                        } else {
                            Util.showErrorAlert("Las contraseñas no coinciden");
                        }
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");

                }
                ajustes.refrescarUsuario(vistaAdmin,vistaConsultor,vistaCliente,modelo);
            }
                break;

            case "CrearInfo": {
                try {
                    if (ajustes.comprobarInfoVacia(vistaCliente)) {
                        Util.showErrorAlert("Rellena todos los campos");
                    } else {
                        String direccion = vistaCliente.txtCalleCliente.getText()+", "+ vistaCliente.txtPortalCliente.getText()
                                +" "+ vistaCliente.txtCiudadCliente.getText()+", "+vistaCliente.txtCPCliente.getText();
                        modelo.insertarInfo(
                                String.valueOf(vistaCliente.cbUsuarioCliente.getSelectedItem()),
                                Date.valueOf(vistaCliente.fechaCliente.getDate()),
                                direccion);
                        ajustes.borrarCamposInfo(vistaCliente);
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");

                }
                ajustes.refrescarInfo(vistaCliente,modelo);
            }
            break;

            case "CrearPedido": {
                try {
                    if (ajustes.comprobarPedidoVacio(vistaCliente)) {
                        Util.showErrorAlert("Rellena todos los campos");
                    } else {
                       modelo.insertarPedido(
                               String.valueOf(vistaCliente.cbProductoCliente.getSelectedItem()),
                               String.valueOf(vistaCliente.cbInfoCliente.getSelectedItem()),
                               Integer.parseInt(vistaCliente.txtCantidad.getText()));
                       ajustes.borrarCamposPedido(vistaCliente);
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");

                }
                ajustes.refrescarPedido(vistaCliente,modelo);
            }
            break;

//---------------------Listeners para volver a la vista-------------------------

            case "AtrasRegistro": {
                registerDialog.setVisible(false);
                loginDialog.setVisible(true);
                registerDialog.dispose();
            }
                break;

            case "AtrasCategoria": {
                categoryDialog.setVisible(false);
                categoryDialog.dispose();
                vistaAdmin.setEnabled(true);
                vistaAdmin.setVisible(true);
            }
                break;

            case "AtrasProducto": {
                productDialog.setVisible(false);
                productDialog.dispose();
                vistaAdmin.setEnabled(true);
            }
                break;

            case "AtrasModificarProducto": {
                modifProductDialog.setVisible(false);
                modifProductDialog.dispose();
                vistaAdmin.setEnabled(true);
                }
                break;

            case "AtrasModificarUser": {
                modifUserDialog.setVisible(false);
                modifUserDialog.dispose();
                vistaAdmin.setEnabled(true);
            }
                break;

            case "AtrasModificarCategoria": {
                modifCategoryDialog.setVisible(false);
                modifCategoryDialog.dispose();
                vistaAdmin.setEnabled(true);
            }
                break;


            }
        }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting()
                && !((ListSelectionModel) e.getSource()).isSelectionEmpty()) {
            if (e.getSource().equals(vistaAdmin.tableProductosAdmin.getSelectionModel())) {
                int row = vistaAdmin.tableProductosAdmin.getSelectedRow();
                modifProductDialog.txtCodigoProductoModif.setText(String.valueOf(vistaAdmin.tableProductosAdmin.getValueAt(row, 1)));
                modifProductDialog.txtNombreProductoModif.setText(String.valueOf(vistaAdmin.tableProductosAdmin.getValueAt(row, 2)));
                modifProductDialog.cbCatProductoModif.setSelectedItem(String.valueOf(vistaAdmin.tableProductosAdmin.getValueAt(row, 3)));
                modifProductDialog.txtPrecioProductoModif.setText(String.valueOf(vistaAdmin.tableProductosAdmin.getValueAt(row, 4)));
                modifProductDialog.txtCantidadProductoModif.setText(String.valueOf(vistaAdmin.tableProductosAdmin.getValueAt(row, 5)));
                modifProductDialog.txtMedidaProductoModif.setText(String.valueOf(vistaAdmin.tableProductosAdmin.getValueAt(row, 6)));
                modifProductDialog.txtTipoProductoModif.setText(String.valueOf(vistaAdmin.tableProductosAdmin.getValueAt(row, 7)));
                modifProductDialog.cbUsosProductoModif.setSelectedItem(String.valueOf(vistaAdmin.tableProductosAdmin.getValueAt(row, 8)));

            } else if (e.getSource().equals(vistaAdmin.tableUsuarioAdmin.getSelectionModel())) {
                int row = vistaAdmin.tableUsuarioAdmin.getSelectedRow();
                modifUserDialog.txtNombreUser.setText(String.valueOf(vistaAdmin.tableUsuarioAdmin.getValueAt(row, 1)));
                modifUserDialog.txtApellidoUser.setText(String.valueOf(vistaAdmin.tableUsuarioAdmin.getValueAt(row, 2)));
                modifUserDialog.cbTipoUser.setSelectedItem(String.valueOf(vistaAdmin.tableUsuarioAdmin.getValueAt(row, 3)));
                modifUserDialog.dpCumpleUser.setDate((Date.valueOf(String.valueOf(vistaAdmin.tableUsuarioAdmin.getValueAt(row, 4)))).toLocalDate());
                modifUserDialog.txtDniUser.setText(String.valueOf(vistaAdmin.tableUsuarioAdmin.getValueAt(row, 5)));
                modifUserDialog.txtPsswdUser.setText(String.valueOf(vistaAdmin.tableUsuarioAdmin.getValueAt(row, 6)));
                modifUserDialog.txtTelefonoUser.setText(String.valueOf(vistaAdmin.tableUsuarioAdmin.getValueAt(row, 7)));
                modifUserDialog.txtAltaUser.setText(String.valueOf(vistaAdmin.tableUsuarioAdmin.getValueAt(row, 8)));

            } else if (e.getSource().equals(vistaAdmin.tableCategoriasAdmin.getSelectionModel())) {
                int row = vistaAdmin.tableCategoriasAdmin.getSelectedRow();
                modifCategoryDialog.txtNombreModif.setText(String.valueOf(vistaAdmin.tableCategoriasAdmin.getValueAt(row, 1)));
                modifCategoryDialog.txtCodigoModif.setText(String.valueOf(vistaAdmin.tableCategoriasAdmin.getValueAt(row, 2)));
                modifCategoryDialog.txtDescripcionModif.setText(String.valueOf(vistaAdmin.tableCategoriasAdmin.getValueAt(row, 3)));

            } else if (e.getValueIsAdjusting() && ((ListSelectionModel) e.getSource()).isSelectionEmpty() && !refrescar) {
                if (e.getSource().equals(vistaAdmin.tableProductosAdmin.getSelectionModel())) {
                    ajustes.borrarCamposProductoModif(modifProductDialog);

                } else if (e.getSource().equals(vistaAdmin.tableUsuarioAdmin.getSelectionModel())) {
                    ajustes.borrarCamposUsuarioModif(modifUserDialog);

                } else if (e.getSource().equals(vistaAdmin.tableCategoriasAdmin.getSelectionModel())) {
                    ajustes.borrarCamposCategoriaModif(modifCategoryDialog);
                }
            }
        }
    }

    private void addItemListeners(Controlador controlador) {
    }
}
