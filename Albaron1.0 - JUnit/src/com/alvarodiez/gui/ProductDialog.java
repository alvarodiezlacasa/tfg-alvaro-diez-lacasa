package com.alvarodiez.gui;

import com.alvarodiez.enums.UsosProducto;

import javax.swing.*;
import java.awt.*;

/**
 * Clase de la vista para añadir un producto nuevo a la base de datos
 */
public class ProductDialog extends JFrame{
    JPanel panel1;

    //JComboBox
    JComboBox cbUsosProducto;
    JComboBox cbCategoriaProducto;

    //JTextField
    JTextField txtCodigoProducto;
    JTextField txtNombreProducto;
    JTextField txtPrecioProducto;
    JTextField txtCantidadProducto;
    JTextField txtMedidaProducto;
    JTextField txtTipoProducto;

    //JButton
    JButton btnAtrasProducto;
    JButton btnCrearProducto;

    /**
     * Constructor de la clase ProductDialog dónde
     * llamamos al método startJFrame() para configurar la ventana
     */
    public ProductDialog() {
        startJFrame();
    }

    /**
     * Método en el que establecemos la configuración de la ventana
     * como por ejemplo el tamaño, la posición, el icono...
     * Además, llamamos a los métodos para configurar los combobox.
     */
    private void startJFrame(){
        this.setTitle("Albaron");
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.pack();
        this.setVisible(false);
        this.setSize(new Dimension(this.getWidth(), this.getHeight()));
        this.setLocationRelativeTo(null);
        Image ico=Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("icono.png"));
        this.setIconImage(ico);

        setEnumComboBox();
    }

    /**
     * Setea el combobox en el que cargamos los objetos de una clase enumerada
     */
    private void setEnumComboBox() {
        for (UsosProducto constant : UsosProducto.values()) {
            cbUsosProducto.addItem(constant.getValor());
        }
        cbUsosProducto.setSelectedIndex(-1);
    }
}
