package com.alvarodiez.gui;

import com.alvarodiez.enums.TipoUsuario;
import com.alvarodiez.enums.UsosProducto;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

/**
 * Clase de la vista del consultor
 */
public class VistaConsultor extends JFrame{
    JPanel panel1;
    JTabbedPane tabbedPane1;
    JPanel panelAjustes;

    //Usuarios
    JTextField txtNombreUsuario;
    JTextField txtDniUsuario;
    JComboBox cbTipoUsuario;
    JToggleButton btnAltaUsuario;
    JToggleButton btnOrdenarUsuario;
    JButton btnTipoUsuario;
    JButton btnDniUsuario;
    JButton btnNombreUsuario;
    JButton btnResetUsuario;
    JTable tablaUsuarios;
    JLabel lblIconoUsuario;
    JLabel lblAccionUsuarioC;

    //Productos
    JButton btnNombreProducto;
    JButton btnUsosProducto;
    JButton btnCategoriaProducto;
    JButton btnResetProducto;
    JComboBox cbUsosProducto;
    JComboBox cbCategoriaProducto;
    JTextField txtNombreProducto;
    JToggleButton btnPrecioProducto;
    JToggleButton btnOrdenarProducto;
    JLabel lblIconoProducto;
    JLabel lblAccionProductoC;
    JTable tablaProductos;

    //Categorías
    JTextField txtNombreCategoria;
    JTextField txtCodigoCategoria;
    JButton btnNombreCategoria;
    JButton btnCodigoCategoria;
    JButton btnResetCategoria;
    JToggleButton btnOrdenarCategoria;
    JTable tablaCategorias;
    JLabel lblIconoCategoria;
    JLabel lblAccionCategoriaC;

    //Ajustes
    JLabel lblIconoAjustes;
    JLabel lblAjustes;
    JLabel lblFuenteAjustes;
    JToggleButton btnTipoTabla;
    JRadioButton btnIcono1;
    JRadioButton btnIcono3;
    JRadioButton btnIcono2;

    //JMenuBar
    JMenuItem itemAcercaDe;
    JMenuItem itemVolver;

    //DTM's
    DefaultTableModel dtmListaProductos;
    DefaultTableModel dtmListaCategorias;
    DefaultTableModel dtmListaUsuarios;

    /**
     * Constructor de la clase VistaConsultor dónde
     * llamamos al método startJFrame() para configurar la ventana
     */
    public VistaConsultor() {
        startJFrame();
    }

    /**
     * Método en el que establecemos la configuración de la ventana
     * como por ejemplo el tamaño, la posición, el icono...
     * Además, llamamos a los métodos para configurar los combobox,
     * las tablas y la barra de menú
     */
    private void startJFrame() {
        setTitle("Albaron");
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(false);
        this.setEnabled(true);
        this.setSize(new Dimension(this.getWidth(), this.getHeight()+20));
        this.setLocationRelativeTo(null);
        Image ico=Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("icono.png"));
        this.setIconImage(ico);

        setDTM();
        setMenu();
        setEnumComboBox();
    }

    /**
     * Método para aplicar un formato a las tablas
     */
    private void setDTM() {
        this.dtmListaCategorias = new DefaultTableModel();
        this.tablaCategorias.setModel(dtmListaCategorias);

        this.dtmListaProductos = new DefaultTableModel();
        this.tablaProductos.setModel(dtmListaProductos);

        this.dtmListaUsuarios = new DefaultTableModel();
        this.tablaUsuarios.setModel(dtmListaUsuarios);
    }

    /**
     * Setea una barra de menus
     */
    private void setMenu(){
        JMenuBar barra = new JMenuBar();
        JMenu menu = new JMenu("Opciones");
        itemAcercaDe = new JMenuItem("Acerca de");
        itemAcercaDe.setActionCommand("Acerca de");
        itemVolver = new JMenuItem("Volver");
        itemVolver.setActionCommand("VolverLoginConsultor");
        menu.add(itemVolver);
        menu.add(itemAcercaDe);
        barra.add(menu);
        barra.add(Box.createHorizontalGlue());
        this.setJMenuBar(barra);
    }

    /**
     * Setea los combobox en los que cargamos los objetos de una clase enumerada
     */
    private void setEnumComboBox() {
        for (UsosProducto constant : UsosProducto.values()) {
            cbUsosProducto.addItem(constant.getValor());
        }
        cbUsosProducto.setSelectedIndex(-1);

        for (TipoUsuario constant : TipoUsuario.values()) {
            cbTipoUsuario.addItem(constant.getValor());
        }
        cbTipoUsuario.setSelectedIndex(-1);
    }

}
