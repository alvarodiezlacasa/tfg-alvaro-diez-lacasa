package com.alvarodiez.gui;

import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class VistaCliente extends JFrame{
    JPanel panel1;

    JTextField txtCantidad;
    JButton btnGenerarAlbaran;
    JButton btnInsertarProducto;
    JButton btnEliminarProducto;
    JButton btnVaciarPedido;
    JComboBox cbUsuarioCliente;
    JComboBox cbProductoCliente;
    DatePicker fechaCliente;

    //Info
    JTextField txtCiudadCliente;
    JTextField txtCPCliente;
    JTextField txtCalleCliente;
    JTextField txtPortalCliente;
    JButton btnCrearInfo;
    JComboBox cbInfoCliente;
    JTable tablaPedidos;
    JTable tablaInfo;
    JButton btnEliminarInfo;
    JLabel lblCalleInfo;
    JLabel lblCiudadInfo;
    JLabel lblCPInfo;
    JLabel lblFechaInfo;
    JLabel lblPortalInfo;
    JLabel lblUsuarioInfo;
    JLabel lblProductoDetalle;
    JLabel lblCantidadDetalle;
    JLabel lblInfoDetalle;
    JLabel lblDetalle;
    JLabel lblPedido;

    //JMenuBar
    JMenuItem itemAjustes;
    JMenuItem itemAcercaDe;
    JMenuItem itemVolver;

    //DTM's
    DefaultTableModel dtmPedidoCliente;
    DefaultTableModel dtmInfoCliente;

    public VistaCliente() {
        startJFrame();

    }

    private void startJFrame() {
        setTitle("Albaron");
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(false);
        this.setEnabled(true);
        this.setSize(new Dimension(this.getWidth(), this.getHeight()+20));
        this.setLocationRelativeTo(null);
        Image ico=Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("icono.png"));
        this.setIconImage(ico);

        setDTM();
        setMenu();
        limitTF();
    }

    private void setDTM() {
        this.dtmPedidoCliente = new DefaultTableModel();
        this.tablaPedidos.setModel(dtmPedidoCliente);

        this.dtmInfoCliente = new DefaultTableModel();
        this.tablaInfo.setModel(dtmInfoCliente);
    }

    /**
     * Setea una barra de menus
     */
    private void setMenu(){
        JMenuBar barra = new JMenuBar();
        JMenu menu = new JMenu("Opciones");
        itemAjustes = new JMenuItem("Ajustes");
        itemAjustes.setActionCommand("AjustesCliente");
        itemAcercaDe = new JMenuItem("Acerca de");
        itemAcercaDe.setActionCommand("Acerca de");
        itemVolver = new JMenuItem("Volver");
        itemVolver.setActionCommand("VolverLoginCliente");
        menu.add(itemAjustes);
        menu.add(itemVolver);
        menu.add(itemAcercaDe);
        barra.add(menu);
        barra.add(Box.createHorizontalGlue());
        this.setJMenuBar(barra);
    }

    /**
     * Este método recoge los listeners para limitar el número
     * de carácteres de los JTextFields
     */
    private void limitTF() {
        txtCiudadCliente.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                if (txtCiudadCliente.getText().length()>=10){
                    e.consume();
                    Toolkit.getDefaultToolkit().beep();
                }
            }
        });
        txtCalleCliente.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                if (txtCalleCliente.getText().length()>=10){
                    e.consume();
                    Toolkit.getDefaultToolkit().beep();
                }
            }
        });
    }

}
