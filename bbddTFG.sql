-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema bbddtfg
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema bbddtfg
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `bbddTFG` DEFAULT CHARACTER SET utf8 ;
USE `bbddTFG` ;

-- -----------------------------------------------------
-- Table `bbddTFG`.`categoria`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bbddTFG`.`categoria` (
  `idCategoria` INT NOT NULL AUTO_INCREMENT,
  `codigoCategoria` VARCHAR(40) NOT NULL UNIQUE,
  `nombreCategoria` VARCHAR(30) NOT NULL,
  `descripcionCategoria` VARCHAR(60) NULL,
  PRIMARY KEY (`idCategoria`),
  UNIQUE INDEX `nombreCategoria_UNIQUE` (`nombreCategoria` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bbddTFG`.`producto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bbddTFG`.`producto` (
  `idProducto` INT NOT NULL AUTO_INCREMENT,
  `codigoProducto` VARCHAR(45) NOT NULL UNIQUE,
  `nombreProducto` VARCHAR(45) NOT NULL,
  `idCategoria` INT NOT NULL,
  `precioUnitProducto` DOUBLE NOT NULL,
  `cantidadProducto` DOUBLE NULL,
  `tipoProducto` VARCHAR(45) NULL,
  `usosProducto` VARCHAR(45) NULL,
  PRIMARY KEY (`idProducto`, `idCategoria`),
  INDEX `fk_Producto_Categoria_idx` (`idCategoria` ASC),
  CONSTRAINT `fk_Producto_Categoria`
    FOREIGN KEY (`idCategoria`)
    REFERENCES `bbddTFG`.`categoria` (`idCategoria`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

ALTER TABLE `bbddtfg`.`producto` ADD COLUMN unidadMedidaProducto VARCHAR(45) AFTER `cantidadProducto`;
-- -----------------------------------------------------producto
-- Table `bbddTFG`.`usuario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bbddTFG`.`usuario` (
  `idUsuario` INT NOT NULL AUTO_INCREMENT,
  `nombreUsuario` VARCHAR(45) NULL,
  `apellidoUsuario` VARCHAR(45) NULL,
  `tipoUsuario` VARCHAR(15) NOT NULL,
  `fechaCumple` DATE NULL,
  `dniUsuario` VARCHAR(10) NOT NULL UNIQUE,
  `psswdUsuario` VARCHAR(45) NULL,
  `telefono` VARCHAR(20) NULL,
  `fechaAltaUsuario` DATE NULL,
  PRIMARY KEY (`idUsuario`))
ENGINE = InnoDB;

insert into `bbddTFG`.`usuario` (`nombreUsuario`, `apellidoUsuario`, `tipoUsuario`, `fechaCumple`, `dniUsuario`,  `psswdUsuario`, `telefono`, `fechaAltaUsuario`)
values ("Alvaro","Diez","Administrador","1999-10-21","123","123","626708665",now());
insert into `bbddTFG`.`usuario` (`nombreUsuario`, `apellidoUsuario`, `tipoUsuario`, `fechaCumple`, `dniUsuario`,  `psswdUsuario`, `telefono`, `fechaAltaUsuario`)
values ("Alvaro","Diez","Cliente","1999-10-21","789","789","626708665",now());
insert into `bbddTFG`.`usuario` (`nombreUsuario`, `apellidoUsuario`, `tipoUsuario`, `fechaCumple`, `dniUsuario`,  `psswdUsuario`, `telefono`, `fechaAltaUsuario`)
values ("Alvaro","Diez","Consultor","1999-10-21","456","456","626708665",now());

-- -----------------------------------------------------
-- Table `bbddTFG`.`pedido`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bbddTFG`.`pedido` (
  `idpedido` INT NOT NULL AUTO_INCREMENT,
  `idUsuario` INT NOT NULL,
  `fechaPedido` DATE NULL,
  `direccionPedido` varchar(700) NULL,
  PRIMARY KEY (`idpedido`, `idUsuario`),
  INDEX `fk_pedido_usuario1_idx` (`idUsuario` ASC),
  CONSTRAINT `fk_pedido_usuario1`
    FOREIGN KEY (`idUsuario`)
    REFERENCES `bbddTFG`.`usuario` (`idUsuario`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bbddTFG`.`detalle_pedido`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bbddTFG`.`detalle_pedido` (
  `idDetalle` INT NOT NULL AUTO_INCREMENT,
  `idProducto` INT NOT NULL,
  `idPedido` INT NOT NULL,
  `cantidadDetalle` INT NULL,
  PRIMARY KEY (`idDetalle`, `idProducto`, `idPedido`),
  INDEX `fk_Detalle_Pedido_Producto1_idx` (`idProducto` ASC),
  INDEX `fk_detalle_pedido_pedido1_idx` (`idPedido` ASC),
  CONSTRAINT `fk_Detalle_Pedido_Producto1`
    FOREIGN KEY (`idProducto`)
    REFERENCES `bbddTFG`.`producto` (`idProducto`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_detalle_pedido_pedido1`
    FOREIGN KEY (`idPedido`)
    REFERENCES `bbddTFG`.`pedido` (`idpedido`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

--

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

--

DELIMITER $$
create function existeDni(f_dni varchar(40)) 
returns bit
READS SQL DATA
DETERMINISTIC
begin
	declare i int;
	set i=0;
	while (i<(select max(idUsuario) from usuario)) do
	if ((select dniUsuario from usuario 
		 where idUsuario=(i+1)) like f_dni) 
	then return 1;
	end if;
	set i=i+1;
	end while;
	return 0;
    
end$$
DELIMITER 

--

DELIMITER $$
create function existeProducto(f_code varchar(40)) 
returns bit
READS SQL DATA
DETERMINISTIC
begin
	declare i int;
	set i=0;
	while (i<(select max(idProducto) from producto)) do
	if ((select codigoProducto from producto 
		 where idProducto=(i+1)) like f_code) 
	then return 1;
	end if;
	set i=i+1;
	end while;
	return 0;
    
end$$
DELIMITER 

--

DELIMITER $$
create function existeCategoria(f_categoria varchar(40)) 
returns bit
READS SQL DATA
DETERMINISTIC
begin
	declare i int;
	set i=0;
	while (i<(select max(idCategoria) from categoria)) do
	if ((select codigoCategoria from categoria 
		 where idCategoria=(i+1)) like f_categoria) 
	then return 1;
	end if;
	set i=i+1;
	end while;
	return 0;
    
end$$
DELIMITER 
